import sc2, random
from sc2.data import ActionResult
from sc2.constants import *
from sc2.position import Point2, Point3
from offense import attacker


class MacroZ:
    #Only call after allin fails, to try and recover
    #should already be dead, but whatevs
    def __init__(self, bot):
        self.bot = bot
        self.max_workers = 72
        self.OVspots = []
        self.OVspots += self.bot.expansion_locations
        self.OVspots.remove(self.bot.townhalls.first.position)
        #self.injectors = [] #tags only
        self.max_injectors = 4
        self.creepers = [] #tags only
        self.allin = True #DONT TOUCH
        self.upg = [AbilityId.RESEARCH_ZERGMISSILEWEAPONSLEVEL1, AbilityId.RESEARCH_ZERGGROUNDARMORLEVEL1,
                    AbilityId.RESEARCH_ZERGMISSILEWEAPONSLEVEL2, AbilityId.RESEARCH_ZERGGROUNDARMORLEVEL2,
                    AbilityId.RESEARCH_ZERGMISSILEWEAPONSLEVEL3, AbilityId.RESEARCH_ZERGGROUNDARMORLEVEL3]
        self.army = []
        self.mainarmy = None
        self.lair = None
        self.roachwarrenloc = None
        self.townhalls = None

    async def step(self, iter):
        #do shit once
        self.townhalls = self.bot.townhalls


        await self.macroBuildings()
        await self.macroUpgrades()
        await self.macroLarva()
        await self.macroQueenProduction()
        #await self.bot.inject(self.injectors)
        #await self.creep()
        # if len(self.army) == 0:
        #     if len(self.bot.units(UnitTypeId.ROACH)) > 30:
        #         self.army += [x.tag for x in self.bot.units(UnitTypeId.ROACH)[:30]]
        #         self.army += [x.tag for x in self.bot.units(UnitTypeId.RAVAGER)]
        #         for x in self.army:
        #             loc = self.bot.known_enemy_structures.random.position.towards(self.bot.game_info.map_center, 3)
        #             if not loc:
        #                 loc = self.bot.enemy_start_locations[0]
        #             u = self.bot.units.find_by_tag(x)
        #             if u:
        #                 await self.bot.do(u.attack(loc))
        # if len(self.army) > 0:
        #     for u in self.bot.units.tags_in(self.army):
        #         if u.is_idle:
        #             loc = self.bot.known_enemy_structures.random.position.towards(self.bot.game_info.map_center, 3)
        #             if not loc:
        #                 loc = self.bot.enemy_start_locations[0]
        #             await self.bot.do(u.attack(loc))
        if not self.mainarmy:
            if self.bot.units(UnitTypeId.ROACH).amount > 30:
                toadd = self.bot.units(UnitTypeId.ROACH).take(30) + [u for u in self.bot.units(UnitTypeId.RAVAGER)]
                self.mainarmy = attacker(toadd)
                self.bot.atkmanager.platoons.append(self.mainarmy)
                self.mainarmy.atk(goal=self.bot.enemy_start_locations[0].towards(self.bot.game_info.map_center, 3), retreat=self.townhalls.center, should_kite=False)
                print('Main Army told to atk')




    async def macroBuildings(self):
        #pool first
        if not self.bot.units(UnitTypeId.SPAWNINGPOOL).exists and self.bot.can_afford(UnitTypeId.SPAWNINGPOOL):
            if len(self.bot.workers) > 12:
                w = self.bot.selectMineralDrone(self.townhalls.first)
                #poolloc TODO: when geysers are next to each other
                th = self.townhalls.first.position
                gas = self.bot.state.vespene_geyser.closer_than(10, th).closest_to(await self.bot.get_next_expansion()).position
                poolloc = None
                if th.x <= gas.x and th.y > gas.y: #gas southwest
                    poolloc = Point2((gas.x-3, gas.y-1))
                elif th.x > gas.x and th.y <= gas.y: #gas northeast
                    poolloc = Point2((gas.x+3, gas.y+1))
                if poolloc:
                    await self.bot.do(w.build(UnitTypeId.SPAWNINGPOOL, poolloc))
        #gas after pool
        if self.bot.units(UnitTypeId.SPAWNINGPOOL).exists and len(self.bot.units(UnitTypeId.EXTRACTOR))+self.bot.already_pending(UnitTypeId.EXTRACTOR) < 1:
            if self.bot.can_afford(UnitTypeId.EXTRACTOR) and len(self.bot.workers) > 12:
                print('MacroBuildings: Gas condition true')
                g = self.bot.state.vespene_geyser.closest_to(self.bot.units(UnitTypeId.SPAWNINGPOOL).first)
                w = self.bot.selectMineralDrone(self.townhalls.first)
                await self.bot.do(w.build(UnitTypeId.EXTRACTOR, g))
        #natural
        if self.bot.units(UnitTypeId.SPAWNINGPOOL).exists and len(self.townhalls) < 2:
            if self.bot.can_afford(UnitTypeId.HATCHERY) and len(self.bot.workers) >= 14:
                w = self.bot.selectMineralDrone(self.townhalls.first)
                await self.bot.do(w.build(UnitTypeId.HATCHERY, await self.bot.get_next_expansion()))
        #3rd
        if len(self.townhalls) < 3 and len(self.bot.workers) >= 27 and self.bot.can_afford(UnitTypeId.HATCHERY):
            print('3rd hatch condition TRUE')
            loc = await self.bot.get_next_expansion()
            w = self.bot.selectMineralDrone(self.townhalls.random)
            await self.bot.do(w.build(UnitTypeId.HATCHERY, loc))
        if self.bot.units(UnitTypeId.SPAWNINGPOOL).exists and len(self.bot.workers) > 36 and not self.bot.units.of_type({LAIR, HIVE}):
            if self.bot.can_afford(LAIR) and self.townhalls.ready.noqueue and self.lair == None:
                self.lair = self.townhalls.ready.noqueue.first
                print('Macroz: Got lair,', self.lair)
                await self.bot.do(self.lair(AbilityId.UPGRADETOLAIR_LAIR))
        if not (self.bot.units(ROACHWARREN) or self.bot.already_pending(ROACHWARREN)) and len(self.bot.workers) > 34:
            if self.bot.can_afford(ROACHWARREN):
                th = self.townhalls.ready.closest_to(self.bot.game_info.map_center)
                mins = self.bot.state.mineral_field.closer_than(10, th).center
                self.bot._client.debug_sphere_out(Point3((mins.x, mins.y, 12)), 1, Point3((100,255,200)))
                loc = th.position.towards(mins, -8)
                self.bot._client.debug_sphere_out(Point3((loc.x, loc.y, 12)), 1, Point3((100, 200, 255)))
                #TODO: Fix spam
                w =  self.bot.selectMineralDrone(self.townhalls.closest_to(self.bot.game_info.map_center))
                loc = await self.bot.find_placement(ROACHWARREN, loc)
                if loc:
                    print('found loc for RoachWarren')
                    await self.bot.do(w.build(ROACHWARREN, loc))
        if len(self.bot.units(EVOLUTIONCHAMBER))+self.bot.already_pending(EVOLUTIONCHAMBER) < 2 and len(self.bot.workers) > 34 and self.bot.minerals > 75:
            th = self.townhalls.ready.closest_to(self.bot.game_info.map_center)
            mins = self.bot.state.mineral_field.closer_than(10, th).center
            loc = th.position.towards(mins, -8)
            # TODO: Fix spam
            w = self.bot.selectMineralDrone(self.townhalls.closest_to(self.bot.game_info.map_center))
            loc = await self.bot.find_placement(UnitTypeId.EVOLUTIONCHAMBER, loc)
            if loc:
                print('found loc for Evo')
                await self.bot.do(w.build(UnitTypeId.EVOLUTIONCHAMBER, loc))
        if self.bot.units(UnitTypeId.ROACHWARREN) and len(self.bot.units(UnitTypeId.EXTRACTOR))+self.bot.already_pending(EXTRACTOR) < 3 and len(self.bot.workers) > 35:
            await self.bot.takeGas()
        if len(self.bot.workers) >= 51 and len(self.bot.units(UnitTypeId.EXTRACTOR)) + self.bot.already_pending(EXTRACTOR) < 6:
            await self.bot.takeGas()
        if self.bot.workers.amount >= 60 and len(self.townhalls) + self.bot.already_pending(UnitTypeId.HATCHERY) < 4:
            if self.bot.can_afford(UnitTypeId.HATCHERY):
                loc = await self.bot.get_next_expansion()
                w = self.bot.selectMineralDrone(self.townhalls.closest_to(loc))
                await self.bot.do(w.build(UnitTypeId.HATCHERY, loc)) #TODO: generalise expand_now and implement
        if self.townhalls.ready.amount >= 4 and len(self.bot.units(EXTRACTOR)) + self.bot.already_pending(EXTRACTOR) < 8:
            await self.bot.takeGas() #TODO: auto expo (5th base onwards)




    async def macroLarva(self):
        if self.bot.units(UnitTypeId.SPAWNINGPOOL).ready:
            poolready = True
        else:
            poolready = False
        if self.bot.units(UnitTypeId.ROACHWARREN).ready:
            roachready = True
        else:
            roachready = False
        lingcount = self.bot.units(UnitTypeId.ZERGLING).amount + 2*self.bot.already_pending(UnitTypeId.ZERGLING)
        roachcount = self.bot.units(UnitTypeId.ROACH).amount + self.bot.already_pending(UnitTypeId.ROACH)

        if roachcount >= 8 and self.bot.units(RAVAGER).amount * 8 < roachcount and self.bot.units(RAVAGER).amount < 8:
            if self.bot.can_afford(RAVAGER) and self.bot.units(UnitTypeId.ROACH):
                if self.bot.known_enemy_units:
                    r = self.bot.units(ROACH).furthest_to(
                        self.bot.known_enemy_units.random)
                else:
                    r = self.bot.units(UnitTypeId.ROACH).furthest_to(self.bot.enemy_start_locations[0])
                await self.bot.do(r(MORPHTORAVAGER_RAVAGER))

        for l in self.bot.units(LARVA):
            #priority 1: OVs
            if (self.bot.supply_used > 12 and self.bot.supply_cap == 14) or (self.bot.supply_used > 18 and self.bot.supply_cap == 22) or (self.bot.supply_used >= 23 and self.bot.supply_left < 5):
                if self.bot.can_afford(OVERLORD) and self.bot.already_pending(OVERLORD) < min(3, len(self.townhalls)):
                    await self.bot.do(l.train(OVERLORD))
                    continue
                else:
                    if self.bot.supply_left <= 0:
                        break
            #priority 2: Safety lings
            if poolready and not roachready and lingcount < 8:
                if self.bot.can_afford(ZERGLING):
                    print('Safety Ling')
                    await self.bot.do(l.train(ZERGLING))
                    lingcount += 2
                    continue
                else:
                    break
            #priority 3: Safety Roaches
            if roachready and roachcount < 8:
                if self.bot.can_afford(ROACH):
                    await self.bot.do(l.train(ROACH))
                    roachcount += 1
                    continue
                else:
                    break
            #priority 4: drone
            if self.bot.state.common.food_workers < min(self.max_workers, 16*len(self.townhalls)+3*len(self.bot.units(EXTRACTOR))):#cap 72 drones, dont outdo hatcheries
                    if self.bot.can_afford(DRONE):
                        #print('droned, min condition:', min(self.max_workers, 16*len(self.townhalls)+3*len(self.bot.units(EXTRACTOR))))
                        await self.bot.do(l.train(DRONE))
                        continue
                    else:
                        break
            #make army
            if poolready and not roachready and lingcount < 14:
                if self.bot.can_afford(DRONE):
                    await self.bot.do(l.train(ZERGLING))
                    lingcount += 2
            else:
                #ratio of 1:8 for roach-ravager; cap at 8 ravagers:
                if self.bot.can_afford(ROACH):
                    await self.bot.do(l.train(ROACH))

    async def macroUpgrades(self):
        upglist = await self.bot.get_available_abilities(self.bot.units.structure)
        if self.bot.units(UnitTypeId.SPAWNINGPOOL).ready.noqueue.exists:
            p = self.bot.units(UnitTypeId.SPAWNINGPOOL).ready.noqueue.first
            if any(AbilityId.RESEARCH_ZERGLINGMETABOLICBOOST in x for x in upglist):
                print('MacroZ: lingspeed in upglist')
                if self.bot.can_afford(AbilityId.RESEARCH_ZERGLINGMETABOLICBOOST):
                    await self.bot.do(p(AbilityId.RESEARCH_ZERGLINGMETABOLICBOOST))
            if AbilityId.RESEARCH_ZERGLINGADRENALGLANDS in upglist:
                await self.bot.doAbility(p, AbilityId.RESEARCH_ZERGLINGADRENALGLANDS, skipcheck = True)
        if self.bot.units(UnitTypeId.ROACHWARREN).ready.noqueue.exists:
            w = self.bot.units(UnitTypeId.ROACHWARREN).ready.noqueue.first
            if any(AbilityId.RESEARCH_GLIALREGENERATION in x for x in upglist):
                if self.bot.can_afford(AbilityId.RESEARCH_GLIALREGENERATION):
                    await self.bot.do(w(AbilityId.RESEARCH_GLIALREGENERATION))
        if self.bot.units(UnitTypeId.EVOLUTIONCHAMBER).ready.noqueue:
            if any(RESEARCH_ZERGMELEEWEAPONS in x for x in upglist):
                print('RESEARCH_ZERGMELEEWEAPONS in upglist')
            evos = self.bot.units(UnitTypeId.EVOLUTIONCHAMBER).ready.noqueue
            for e in evos:
                for u in self.upg:
                    if any(u in x for x in upglist):
                        if self.bot.can_afford(u) and e.is_idle:
                            await self.bot.do(e(u))
                            break

    async def macroQueenProduction(self):
        if len(self.bot.units(QUEEN)) + self.bot.already_pending(QUEEN) < 5 and not (len(self.bot.workers) > 28 and not self.bot.units.of_type({LAIR, HIVE})):
            for h  in self.townhalls:
                if h.is_idle and self.bot.can_afford(QUEEN):
                    await self.bot.do(h.train(QUEEN))
                    break

    async def creep(self):
        for u in self.creepers:
            q = self.bot.units.find_by_tag(u)
            if not q:
                continue
            if q.energy >= 25 and BUILD_CREEPTUMOR_QUEEN not in q.orders and len(self.bot.units(CREEPTUMOR) | self.bot.units(CREEPTUMORBURROWED) | self.bot.units(CREEPTUMORQUEEN)) < 4:
                sources = self.townhalls | self.bot.units(CREEPTUMORBURROWED) | self.bot.units(CREEPTUMORQUEEN)
                if not sources:
                    continue
                s = sources.closest_to(q)
                u = await self.bot.get_next_expansion()
                u2 = u.towards(self.bot.game_info.map_center, 6)
                loc = s.position.towards(u2, 10).rounded
                await self.bot.do(q(BUILD_CREEPTUMOR_QUEEN, loc))
        for c in self.bot.units(CREEPTUMOR) | self.bot.units(CREEPTUMORBURROWED) | self.bot.units(CREEPTUMORQUEEN):
            a = await self.bot.get_available_abilities(c)
            if AbilityId.BUILD_CREEPTUMOR_TUMOR in a:
                target = self.bot.known_enemy_structures.closest_to(c)
                if not target:
                    target = self.bot.enemy_start_locations[0]
                else:
                    target = target.position

                locs = c.position.towards_with_random_angle(target.position, 10).rounded
                self.bot._client.debug_line_out(c, locs)
                expo = c.closest(self.bot.expansion_locations.keys())
                if locs.distance_squared(expo) > 20:
                    l = await self.bot.find_placement(CREEPTUMOR, locs, int(locs.distance_to_point2(expo))+1)
                    if l:
                        await self.bot.do(c(BUILD_CREEPTUMOR_TUMOR, l))


        
    async def buildingConstructionComplete(self, unit):
        if unit.type_id == EXTRACTOR: #TODO: Fix
            ths = self.townhalls.filter(lambda x: x.assigned_harvesters != x.ideal_harvesters)
            if ths:
                th = ths.closest_to(unit)
            else:
                th = self.townhalls.closest_to(unit)
            ww = [u.tag for u in self.bot.workers.idle.closer_than(8, unit)]
            if len(ww) > 3:
                ww = ww[:3]
                print('too many drones, picking first 3')
            elif len(ww) < 3:
                while len(ww) < 3:
                    w = self.bot.selectMineralDrone(th, notin=ww)
                    if not w:
                        print('NOT w')
                    if len(ww) < 3: #should be redundant
                        ww.append(w.tag)
                    print('appending to ww:', w)
            if len(ww) == 3:
                for x in ww:
                    await self.bot.do(self.bot.units.find_by_tag(x)(SMART, unit))
        #TODO: Extractors, tech buildigs maybe, new hatches
    async def unitCreated(self, unit):
        if unit.type_id == OVERLORD:
            print('Overlord made')
            locs = unit.position.sort_by_distance(self.bot.cartographer.OVspots)
            #print(locs)
            if len(locs) > 0:
                await self.bot.do(unit(SMART, locs[0]))
                self.bot.cartographer.OVspots.remove(locs[0])
                self.bot.cartographer.OVspotsOccupied.append(locs[0])
        if unit.type_id == QUEEN:
            currentH = self.townhalls.closest_to(unit)
            if (len(self.bot.creep.injectors) < self.max_injectors) and (len(self.bot.creep.injectors) < len(self.townhalls.ready)):
                if len(self.bot.creep.injectors) == 0:
                    print('Queen Ready, first injector')
                    self.bot.creep.injectors.append(unit.tag)
                else:
                    if currentH.position._distance_squared(self.bot.units(QUEEN).tags_in(set(self.bot.creep.injectors)).closest_to(currentH).position) > 36: #an injector not -in 6 range
                        self.bot.creep.injectors.append(unit.tag)
                    else:
                        for h in self.townhalls.prefer_close_to(unit):
                            if h.position._distance_squared(self.bot.units(QUEEN).tags_in(set(self.bot.creep.injectors)).closest_to(h).position) < 36:
                                continue
                            else:
                                await self.bot.do(unit.move(h.position.towards(unit, 5)))
                                self.bot.creep.injectors.append(unit.tag)
                                print('MacroZ: trying to move queen to different th')
                                break
            else:
                #either creeper, or gonna become injector eventually #TODO fix this
                h = await self.bot.get_next_expansion()

                if not self.bot.units.closer_than(12, currentH).of_type({CREEPTUMOR, CREEPTUMORQUEEN}):
                    t = unit.position.closest(self.bot.creep.goals)
                    if unit.distance_to(t) >= 10:
                        loc = currentH.position.towards(h, 10).rounded
                        await self.bot.do(unit(BUILD_CREEPTUMOR_QUEEN, loc))
                    else:
                        err = await self.bot.do(unit(BUILD_CREEPTUMOR_QUEEN, t))
                        if not err:
                            print('queen took goal,', t)
                            if t in self.bot.creep.goals:
                                self.bot.creep.goals.remove(t)
                            else:
                                print('queen tried to take goal, wtf goal not in goals', t, err) #should now be fixed? (removed .rounded)
                else:
                    await self.bot.do(unit.move(self.bot.units.of_type({CREEPTUMOR, CREEPTUMORQUEEN}).closest_to(self.bot.game_info.map_center)))
                    self.bot.creep.creepers.append(unit.tag)
                    print('MacroZ: new creeper, creep tumor already exists')
                if len(self.bot.units(QUEEN).tags_not_in(self.bot.creep.creepers)) < self.max_injectors: #gonna become injector eventually
                    print('new queen abilities:', await self.bot.get_available_abilities(unit))
                    loc = h.towards(unit, 3)
                    if not loc:
                        loc = unit.position.towards(h, 10)
                    await self.bot.do(unit(SCAN_MOVE, h.towards(unit, 3), queue=True))
                    print('new queen, gonna be injector soon')
                    self.bot.creep.injectors.append(unit.tag)
                else:
                    print('MacroZ: queen added to creeper')
                    self.bot.creep.creepers.append(unit.tag)
        #if unit.type_id == EGG:
            #print(unit, unit.orders[0])
    async def unitDestroyed(self, tag):
        pass
