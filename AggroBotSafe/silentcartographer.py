import sc2, random
from sc2.data import ActionResult
from sc2.constants import *
from sc2.position import Point2, Point3

class SilentCartographer:
    '''everything map-related and map awareness
    Responsibilities:
        [ ] track expansions better than default
        [ ] track enemy expansions
        [ ] do build order location json`s
        [ ] get building placements if no build order locations
        [X] deal with OV pillars
        [ ] deal with OVs? '''

    def __init__(self, bot, jason=None, handle_OVs=True):
        self.bot = bot
        self.ready = False
        self.bo = jason
        self.expofree = [] #Point2
        self.expomine = [] #Point2
        self.expoenem = [] #Point2
        self.heightmap = None
        self.shouldHandleOVs = handle_OVs
        self.OVs = [] #tags
        self.OVPillars = [] #Point2
        self.OVspots = [] #Point2
        self.OVspotsOccupied = [] #Point2
        #TODO: more

    async def step(self, iter):
        if self.ready == False:
            #setup own bases
            freebase = list(self.bot.expansion_locations.keys())
            ##sort expofree
            self.expofree = [self.bot.game_info.player_start_location]
            while freebase:
                viableExpos = {x.closest(freebase): x.distance_to_closest(freebase) for x in self.expofree}
                chosen = min(viableExpos, key=viableExpos.get)
                if chosen not in self.expofree:
                    self.expofree.append(chosen)
                    freebase.remove(chosen)
                else:
                    freebase.remove(chosen)
            ##seperate owned bases
            if len(self.bot.enemy_start_locations) == 1:
                loc = self.bot.enemy_start_locations[0]
                print('Cartographer: confirm len(enemy_start_locations) = 1')
            else:
                loc = self.bot.game_info.map_center
            for th in self.bot.townhalls.sorted_by_distance_to(loc, reverse=True):
                expo = th.position.closest(self.expofree)
                print('Cartographer: mine expo,', expo)
                self.expomine.append(expo)
                self.expofree.remove(expo)



            #setup enemy bases
            enems = self.bot.known_enemy_structures.of_type(
                {UnitTypeId.HATCHERY, UnitTypeId.LAIR, UnitTypeId.HIVE, UnitTypeId.COMMANDCENTER,
                 UnitTypeId.ORBITALCOMMAND, UnitTypeId.PLANETARYFORTRESS, UnitTypeId.NEXUS})
            if enems:
                for th in enems:
                    expo = th.position.closest(self.expofree)
                    self.expoenem.append(expo)
                    self.expofree.remove(expo)
            else:
                if len(self.bot.enemy_start_locations) == 1:
                    loc2 = self.bot.enemy_start_locations[0].closest(self.expofree)
                    self.expoenem.append(loc2)
                    self.expofree.remove(loc2)
                else:
                    print('Cartographer: AARGH len not 1, len:', len(self.bot.enemy_start_locations))

            #OV stuff
            print('Cartographer, right before OV stuff, mine:', self.expomine)
            if self.shouldHandleOVs:
                #setup OVs
                self.OVs = [x.tag for x in self.bot.units(UnitTypeId.OVERLORD)]
                #setup OV Pillars
                self.heightmap = self.bot.game_info.terrain_height
                pillarPoint2 = {Point2((x,y)): self.heightmap[(x,y)] >= 143
                               for x in range(self.heightmap.width)
                               for y in range(self.heightmap.height)}
                pillarPoint2 = self.bot.game_info._find_groups({p for p in pillarPoint2 if pillarPoint2[p]}, 2)
                for group in pillarPoint2:
                    pos = Point2.center(group)
                    self.OVPillars.append(Point2((int(pos.x)+1, int(pos.y))))
                #setup OV spots, remove nearby pillars
                if len(self.expomine) > 1:
                    loc = self.expomine[1]
                elif self.expomine and len(self.expofree) > 1:
                    loc = self.expofree[1-len(self.expomine)]
                else:
                    loc = self.bot.game_info.player_start_location
                print('Cartographer, loc:', loc)
                self.OVspots = self.expofree + [i for i in self.OVPillars if i._distance_squared(loc) > 900]



            self.ready = True
        else:
            pass


    async def buildingConstructionComplete(self, unit):
        pass
    async def unitCreated(self, unit):
        if not self.ready:
            return
        if unit in self.bot.townhalls:
            loc = unit.position.closest(self.expofree)
            if unit.position._distance_squared(loc) <= 225:
                self.expomine.append(loc)
                self.expofree.remove(loc)
        elif unit.type_id == UnitTypeId.OVERLORD and self.shouldHandleOVs:
            locs = unit.position.sort_by_distance(self.OVspots)
            # print(locs)
            if len(locs) > 0:
                await self.bot.do(unit(SMART, locs[0]))
                self.OVspots.remove(locs[0])
                self.OVspotsOccupied.append(locs[0])



    async def unitDestroyed(self, tag):
        pass
