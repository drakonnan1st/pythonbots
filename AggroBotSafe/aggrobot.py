import sc2, random
from sc2.data import ActionResult
from sc2 import run_game, maps, Race, Difficulty
from sc2.player import Bot, Computer, Human
from sc2.constants import *
from sc2.position import Point2, Point3

from basebot import BaseBot
from aggroza import AggroZA
from aggrozb import  AggroZB
from macroz import MacroZ
from workermgmt import WorkerMgmt
from creep import Creep
from offense import OffenseManager
from silentcartographer import SilentCartographer

class AggroBot(BaseBot):
    def __init__(self):
        super().__init__()
        self.ais = []
        self.debug = True

    def on_start(self):
        self.ai = AggroZA(self)
        self.ais.append(self.ai)
        self.workermanager = WorkerMgmt(self)
        self.ais.append(self.workermanager)
        self.creep = Creep(self)
        self.ais.append(self.creep)
        self.atkmanager = OffenseManager(self)
        self.ais.append(self.atkmanager)
        self.cartographer = SilentCartographer(self)
        self.ais.append(self.cartographer)

    async def on_step(self, iteration):
        if iteration == 0:
            await self.workersplit(self.townhalls.random)
            await self.chat_send('(glhf)')
        for ai in self.ais:
            await ai.step(iteration)
##        if iteration == 20:
##            self.ai = MacroZ(self)
##            print('hijacked to MacroZ')

        if self.ai.allin == False:
            self.ai = MacroZ(self)
            self.ais[0] = self.ai
        if self.debug == True:
            for p in self.cartographer.OVspots:
                self._client.debug_line_out(Point3((p.x, p.y, 4)), Point3((p.x, p.y, 15)), Point3((100,255,100)))
            for t in self.workermanager.townhalltags:
                u = self.units.find_by_tag(t)
                if u:
                    if t == self.workermanager.activeline:
                        self._client.debug_sphere_out(u.position3d, 3, Point3((20,255,20)))
                    if t in self.workermanager.lockedlines:
                        self._client.debug_text_world('locked', Point3((u.position.x, u.position.y, 14)))
                        self._client.debug_box_out(Point3((u.position.x-3, u.position.y-3, 8)), Point3((u.position.x+3, u.position.y+3, 15)), Point3((100,100,100)))
                    else:
                        self._client.debug_text_world('not locked', Point3((u.position.x, u.position.y, 14)))
                        self._client.debug_box_out(Point3((u.position.x - 3, u.position.y - 3, 8)),
                                                   Point3((u.position.x + 3, u.position.y + 3, 15)),
                                                   Point3((20, 255, 20)))
            for t in self.creep.goals:
                self._client.debug_sphere_out(Point3((t.x, t.y, 12)), 1, color = Point3((180,180,180)))
            h = await self.get_next_expansion()
            if h:
                self._client.debug_box_out(Point3((h.x-2.5, h.y-2.5, 10)), Point3((h.x+2.5,h.y+2.5,14)))
            a = self.units.find_by_tag(self.workermanager.activeline)
            if a:
                u = a.position.towards(self.state.mineral_field.closest_to(a), 3)
                self._client.debug_text_world('active', Point3((u.x, u.y, 14)))
            self._client.debug_text_screen('DRONE: {},\n already_pending: {}, \n dronecondition: {} \n creeper count: {}'.format(
                self.units(UnitTypeId.DRONE).amount,
                self.already_pending(UnitTypeId.DRONE),
                min(self.max_workers, 16 * len(self.townhalls) + 3 * len(self.units(UnitTypeId.EXTRACTOR))),
                len(self.creep.creepers)), pos=(0.1,0.1))
            await self._client.send_debug()

        #End of on_step()
        await self.dostuff()

    async def on_building_construction_complete(self, unit):
        for ai in self.ais:
            await ai.buildingConstructionComplete(unit)
        #if unit.type_id == EXTRACTOR:
            #await self.distribute_workers()
    async def on_unit_created(self, unit):
        for ai in self.ais:
            await ai.unitCreated(unit)
    async def on_unit_destroyed(self, unit_tag):
        for ai in self.ais:
            await ai.unitDestroyed(unit_tag)

#groundatk = {UnitTypeId.ZERGLING, UnitTypeId.BANELING, UnitTypeId.QUEEN, UnitTypeId.SPORECRAWLER, UnitTypeId.ROACH, UnitTypeId.HYDRALISK, }


run_game(maps.get("BlueshiftLE"), [
    Bot(Race.Zerg, AggroBot()),
    Computer(Race.Zerg, Difficulty.Easy)],
    #Human(Race.Zerg),
    #Bot(Race.Zerg, AggroBot())],
    realtime=False)
