import sc2, random
from sc2.data import ActionResult
from sc2 import run_game, maps, Race, Difficulty
from sc2.player import Bot, Computer, Human
from sc2.constants import *
from sc2.position import Point2, Point3

from macroz import MacroZ

class AggroZB:
    '''
    12 pool
    14 OV stop droning
    @pool 3 sets of lings + send worker to nat
    17 hatch
    9 more sets of lings
    drone+queen
    '''
    def __init__(self, bot):
        self.phase = 0
        self.bot = bot
        self.hatch = None
        self.army = []
        self.max_workers = 66 #idk why
        self.allin = True
        self.pool = None
        self.poolloc = None
        self.natdrone = None
        self.atktarget = None
        self.enemynattag = None
        self.attacking = False

    async def step(self, iter):
        if self.hatch == None:
            self.hatch = self.bot.townhalls.first
        if self.atktarget == None:
            self.atktarget = self.bot.enemy_start_locations[0]


        if self.attacking:
            await self.lingmicro()
            await self.macroBuildings()
            await self.macroLarva()
        else:
            await self.buildorder()



    async def buildorder(self):
        if self.bot.supply_used == 12 and self.bot.minerals > 150:
            if self.pool == None:
                self.pool = self.bot.selectMineralDrone(self.hatch)
                gas = self.bot.state.vespene_geyser.closer_than(10, self.hatch).closest_to(
                    await self.bot.get_next_expansion()).position
                if self.hatch.position.x <= gas.x and self.hatch.position.y > gas.y:  # gas southwest
                    self.poolloc = Point2((gas.x - 3, gas.y - 1))
                elif self.hatch.position.x > gas.x and self.hatch.position.y <= gas.y:  # gas northeast
                    self.poolloc = Point2((gas.x + 3, gas.y + 1))
                if self.poolloc:
                    await self.bot.do(self.pool.move(self.poolloc))
        if not self.bot.units(UnitTypeId.SPAWNINGPOOL) and self.bot.minerals >= 200:
            if self.pool and self.poolloc:
                await self.bot.do(self.pool.build(UnitTypeId.SPAWNINGPOOL, self.poolloc))
            else:
                print('AggroZB: no drone or pool location')
        if self.bot.units(UnitTypeId.SPAWNINGPOOL) and self.bot.supply_cap <= 14:
            if self.bot.supply_used < 14:
                await self.bot.drone()
            if self.bot.supply_left <= 0 and not self.bot.already_pending(UnitTypeId.OVERLORD):
                for l in self.bot.units(UnitTypeId.LARVA):
                    if self.bot.can_afford(UnitTypeId.OVERLORD):
                        await self.bot.do(l.train(UnitTypeId.OVERLORD))
        if 14 < self.bot.supply_cap < 23:
            if self.bot.supply_used < 17 and self.bot.units(UnitTypeId.SPAWNINGPOOL).ready:
                for l in self.bot.units(LARVA):
                    if self.bot.can_afford(ZERGLING):
                        await self.bot.do(l.train(UnitTypeId.ZERGLING))
            if self.bot.supply_used > 15 and self.natdrone == None:
                self.natdrone = self.bot.selectMineralDrone(self.hatch)
                nat = await self.bot.get_next_expansion()
                await self.bot.do(self.natdrone.move(nat))
            if self.bot.supply_used >= 17 and self.bot.townhalls.amount < 2 and self.natdrone:
                if self.bot.minerals >= 300:
                    loc = await self.bot.get_next_expansion()
                    await self.bot.do(self.natdrone.build(UnitTypeId.HATCHERY, loc))
            if self.bot.units(UnitTypeId.ZERGLING).amount + 2*self.bot.already_pending(UnitTypeId.ZERGLING) < 12 and self.bot.townhalls.amount > 1:
                for l in self.bot.units(UnitTypeId.LARVA):
                    if self.bot.can_afford(UnitTypeId.ZERGLING):
                        await self.bot.do(l.train(UnitTypeId.ZERGLING))
            if self.bot.units(UnitTypeId.ZERGLING).amount + 2*self.bot.already_pending(UnitTypeId.ZERGLING) >= 12:
                if not self.attacking:
                    self.attacking = True
                if self.hatch.noqueue and self.bot.minerals >= 150:
                    await self.bot.do(self.hatch.train(UnitTypeId.QUEEN))
                if self.bot.units(UnitTypeId.QUEEN).amount + self.bot.already_pending(UnitTypeId.QUEEN) > 0:
                    if self.bot.already_pending(UnitTypeId.OVERLORD) == 0:
                        if self.bot.units(UnitTypeId.LARVA) and self.bot.minerals >= 100:
                            l = self.bot.units(UnitTypeId.LARVA).first
                            await self.bot.do(l.train(UnitTypeId.OVERLORD))
                    else:
                        await self.bot.drone()
        if self.bot.supply_cap > 23:
            await self.bot.drone()


    async def lingmicro(self):
        if self.attacking == True:
            if self.bot.units(UnitTypeId.ZERGLING).amount < 1:
                self.attacking = False
                self.allin = False
            else:
                for z in self.bot.units(UnitTypeId.ZERGLING):
                    col = Point3((255,255,255))

                    if z.position._distance_squared(self.atktarget) > 2500:
                        col = Point3((100,100,255))
                        if z.is_idle:
                            await self.bot.do(z.move(self.atktarget))
                        if self.bot.known_enemy_units.exclude_type({UnitTypeId.LARVA}).closer_than(12, z):
                            await self.bot.do(z.attack(self.atktarget))
                            col =Point3((255,0,0))
                    else:
                        col = Point3((255,100,255))
                        if self.bot.known_enemy_units.of_type({UnitTypeId.ZERGLING}).closer_than(4, z) or self.bot.known_enemy_units.of_type({UnitTypeId.QUEEN, UnitTypeId.SPINECRAWLER, UnitTypeId.ROACH}).closer_than(7, z):
                            closest = self.bot.known_enemy_units.of_type({UnitTypeId.ZERGLING, UnitTypeId.DRONE, UnitTypeId.QUEEN, UnitTypeId.SPINECRAWLER, UnitTypeId.ROACH}).closest_to(z)
                            if not z.is_attacking:
                                await self.bot.do(z.attack(closest))
                                col = Point3((255,50,100))
                            else:
                                if z.orders[0].target != closest:
                                    await self.bot.do(z.attack(closest))
                                    col = Point3((200, 50, 50))
                        elif self.bot.known_enemy_units.of_type({UnitTypeId.DRONE}).closer_than(7, z):
                            closest = self.bot.known_enemy_units.of_type({UnitTypeId.DRONE}).closest_to(z)
                            if not z.is_attacking:
                                await self.bot.do(z.attack(closest))
                                col = Point3((200, 0, 50))
                            else:
                                if z.orders[0].target != closest:
                                    await self.bot.do(z.attack(closest))
                                    col = Point3((200, 50, 50))
                        elif self.bot.known_enemy_units.of_type({UnitTypeId.HATCHERY, UnitTypeId.LAIR}).closer_than(12, z):
                            closest = self.bot.known_enemy_units.of_type({UnitTypeId.HATCHERY, UnitTypeId.LAIR}).closest_to(z)
                            if self.enemynattag == None:
                                self.enemynattag = closest.tag
                                print('got your natural:', self.enemynattag)
                            if not z.is_attacking:
                                await self.bot.do(z.attack(closest))
                                col = Point3((200, 0, 50))
                            else:
                                if z.orders[0].target != closest:
                                    await self.bot.do(z.attack(closest))
                                    col = Point3((200, 50, 50))
                        elif self.bot.known_enemy_units.exclude_type({UnitTypeId.LARVA}):
                            closest = self.bot.known_enemy_units.exclude_type({UnitTypeId.LARVA}).closest_to(z)
                            if closest:
                                if not z.is_attacking:
                                    await self.bot.do(z.attack(closest))
                                    col = Point3((200, 0, 50))
                                elif z.orders[0].target != closest:
                                    await self.bot.do(z.attack(closest))
                                    col = Point3((200, 50, 50))
                        else:
                            if z.is_idle:
                                await self.bot.do(z.attack(self.atktarget))

                    if z.order_target:
                        tgt = None
                        if isinstance(z.order_target, int):
                            tgt1 = self.bot.units.find_by_tag(z.order_target)
                            if tgt1:
                                tgt = tgt1.position3d
                        elif isinstance(z.order_target, Point2):
                            tgt = Point3((z.order_target.x, z.order_target.y, z.position3d.z))
                        if tgt:
                            self.bot._client.debug_line_out(z, tgt, col)


    async def macroBuildings(self):
        #incase pool ded:
        if not self.bot.units(UnitTypeId.SPAWNINGPOOL):
            if self.bot.supply_workers > 6 and self.bot.minerals > 200 and not self.bot.already_pending(UnitTypeId.SPAWNINGPOOL):
                w = self.bot.selectMineralDrone(self.hatch)
                mins = self.bot.state.mineral_field.closer_than(10, self.hatch).center
                loc = await self.bot.find_placement(UnitTypeId.SPAWNINGPOOL, self.hatch.position.towards(mins, -8))
                if loc:
                    await self.bot.do(w.build(UnitTypeId.SPAWNINGPOOL, loc))
        #incase hatch ded
        if self.bot.townhalls.amount < 2 and self.bot.already_pending(UnitTypeId.HATCHERY) - self.bot.units(UnitTypeId.HATCHERY).not_ready.amount == 0:
            if self.bot.minerals >= 300:
                w = self.bot.selectMineralDrone(self.hatch)
                loc = await self.bot.get_next_expansion()
                await self.bot.do(w.build(UnitTypeId.HATCHERY, loc))
        #take gas 1:
        if not self.bot.units(UnitTypeId.EXTRACTOR):
            if self.bot.supply_workers > 16 and self.bot.minerals > 75 and self.bot.already_pending(UnitTypeId.EXTRACTOR) - self.bot.units(UnitTypeId.EXTRACTOR).not_ready.amount == 0:
                await self.bot.takeGas()
        #TODO: make more idk

    async def macroLarva(self):
        if self.bot.larva_count > 0:
            if self.bot.units(UnitTypeId.SPAWNINGPOOL).ready:
                poolready = True
            else:
                poolready = False
            if self.bot.units(UnitTypeId.ROACHWARREN).ready:
                roachready = True
            else:
                roachready = False
            lingcount = self.bot.units(UnitTypeId.ZERGLING).amount + 2 * self.bot.already_pending(UnitTypeId.ZERGLING)
            roachcount = self.bot.units(UnitTypeId.ROACH).amount + self.bot.already_pending(UnitTypeId.ROACH)

            for l in self.bot.units(LARVA):
                # priority 1: OVs
                if (self.bot.supply_used > 12 and self.bot.supply_cap == 14) or (
                        self.bot.supply_used > 18 and self.bot.supply_cap == 22) or (
                        self.bot.supply_used >= 23 and self.bot.supply_left < 5):
                    if self.bot.can_afford(OVERLORD) and self.bot.already_pending(OVERLORD) < min(3,
                                                                                                  len(self.bot.townhalls)):
                        await self.bot.do(l.train(OVERLORD))
                        continue
                    else:
                        if self.bot.supply_left <= 0:
                            break
                # priority 2: Safety lings
                # if poolready and not roachready and lingcount < 8:
                #     if self.bot.can_afford(ZERGLING):
                #         print('Safety Ling')
                #         await self.bot.do(l.train(ZERGLING))
                #         lingcount += 2
                #         continue
                #     else:
                #         break
                # priority 3: Safety Roaches
                if roachready and roachcount < 8:
                    if self.bot.can_afford(ROACH):
                        await self.bot.do(l.train(ROACH))
                        roachcount += 1
                        continue
                    else:
                        break
                # priority 4: drone
                if self.bot.state.common.food_workers < min(self.max_workers, 16 * len(self.bot.townhalls) + 3 * len(
                        self.bot.units(EXTRACTOR))):  # cap 72 drones, dont outdo hatcheries
                    if self.bot.can_afford(DRONE):
                        # print('droned, min condition:', min(self.max_workers, 16*len(self.townhalls)+3*len(self.bot.units(EXTRACTOR))))
                        await self.bot.do(l.train(DRONE))
                        continue
                    else:
                        break
                # make army
                if poolready and not roachready and lingcount < 14:
                    if self.bot.can_afford(DRONE):
                        await self.bot.do(l.train(ZERGLING))
                        lingcount += 2
                else:
                    # ratio of 1:8 for roach-ravager; cap at 8 ravagers:
                    if self.bot.can_afford(ROACH):
                        await self.bot.do(l.train(ROACH))





    async def buildingConstructionComplete(self, unit):
        pass

    async def unitCreated(self, unit):
        if unit.type_id == UnitTypeId.SPAWNINGPOOL:
            self.pool = unit

    async def unitDestroyed(self, tag):
        if self.enemynattag:
            if tag == self.enemynattag:
                if self.bot.townhalls:
                    hatch = self.bot.townhalls.closest_to(self.atktarget)
                    self.allin = False
                    for z in self.bot.units(UnitTypeId.ZERGLING):
                        await self.bot.do(z.move(hatch.position.towards(self.atktarget, 6)))