import sc2, random
from sc2.data import ActionResult
from sc2.constants import *
from sc2.position import Point2, Point3


class WorkerMgmt:
    #Does worker distribution, RALLY_WORKERS, RALLY_UNITS for zerg only
    def __init__(self, bot):
        self.bot = bot
        self.armyrally = None #zerg only; Point2
        self.workerrally = None #Point2
        self.townhalltags = [] #all townhall tags
        self.lockedlines = [] #townhalls that should stay at 16/16 and follow workerrally
        self.activeline = 0 #town hall tag to rally
        self.geysertags = [] #geysers raw
        self.extractors = []
        self.step1 = True
        if self.bot.debug == False:
            self.bot.debug = True

    async def step(self, iter):
        if self.step1 == True:
            for t in self.bot.townhalls.ready:
                self.townhalltags.append(t.tag)
                self.activeline = t.tag
                self.geysertags += [x.tag for x in self.bot.state.vespene_geyser.closer_than(10, t)]
                if self.bot.units.of_type({EXTRACTOR, REFINERY, ASSIMILATOR}):
                    self.extractors += [x.tag for x in self.bot.units.of_type({EXTRACTOR, REFINERY, ASSIMILATOR})]
            for x in self.extractors:
                self.geysertags.remove(self.bot.state.vespene_geyser.closest_to(self.bot.units.find_by_tag(x)).tag)

            print('WorkerMgmt: Initialised, townhalltags:', self.townhalltags)
            if len(self.townhalltags) > 0: #TODO: This will break if there are no townhalltags;
                #self.activeline = self.bot.units.tags_in(set(self.townhalltags)).sort(key=lambda x: x.assigned_harvesters).first.tag
                self.step1 = False
        else: #HERE: lock lines, []deal with oversaturation, []deal with empty bases

            for t in self.townhalltags: # dont think 'if not in self.lockedlines' is needed
                th = self.bot.units.find_by_tag(t)
                if not th:
                    self.townhalltags.remove(t)
                    print('WorkerMgmt: removing from townhalltags:', t)
                    if t == self.activeline:
                        await self.setactive() #if the activeline died, reset activeline
                    continue
                if th.ideal_harvesters == 0:
                    self.townhalltags.remove(t)
                    print('WorkerMgmt: empty base:', t)
                    continue
                if t not in self.lockedlines and th.assigned_harvesters >= th.ideal_harvesters:
                    if t == self.activeline:
                        ths = self.bot.townhalls.filter(lambda x: x.assigned_harvesters < x.ideal_harvesters and x.ideal_harvesters > 0)
                        if ths:
                            print('WorkerMgmt: locking active, setting new active')
                            self.lockedlines.append(t)
                            await self.setactive(th=ths.random, all=True)
                    else:
                        active = self.bot.units.find_by_tag(self.activeline)
                        if active:
                            await self.bot.do(th(RALLY_WORKERS, self.bot.state.mineral_field.closest_to(active)))
                            print('WorkerMgmt: changing worker rally of t')
                if th.assigned_harvesters > th.ideal_harvesters:
                        await self.bot.distribute_workers() #TODO: upgrade this


    async def setactive(self, th=None, all=False):
        if th == None:
            ths = self.bot.townhalls.filter(lambda x: x.assigned_harvesters < 16 and x.ideal_harvesters > 0)
            if not ths:
                ths = self.bot.townhalls.filter(lambda x: x.ideal_harvesters > 0)
                if not ths:
                    return None
            print('WorkerMgmt: tryna setactive, ths:', ths)
            th = ths.sorted(keyfn=lambda x: x.assigned_harvesters, reverse = True).first
        self.activeline = th.tag
        mins = self.bot.state.mineral_field.closer_than(10, th)
        if not mins: #shouldn't happen if ideal_harvesters > 0
            return  None
        self.workerrally = self.bot.state.mineral_field.closest_to(mins.center)
        if all == False:
            for th in self.bot.units.tags_in(self.lockedlines):
                await self.bot.do(th(RALLY_WORKERS, self.workerrally))
        else:
            for th in self.bot.townhalls:
                await self.bot.do(th(RALLY_WORKERS, self.workerrally))


    async def distdrone(self):
        expos = self.bot.expansion_locations #why needed?
        exposmine = self.bot.owned_expansions

        mineralTag = [x.tag for x in self.bot.state.mineral_field]
        geyserTag = self.extractors

        geyserUndersaturated = {}
        geyserOversaturated = {}

        poolOfTags = set()

        for g in self.bot.units.tags_in(self.extractors):
            deficit = g.ideal_harvesters - g.assigned_harvesters


    #requried
    async def buildingConstructionComplete(self, unit):
        if unit in self.bot.townhalls:
            self.townhalltags.append(unit.tag)
            print('WorkerMgmt: New Hatch in townhalltags at', unit.position)
            if unit.type_id == HATCHERY: #sets rally points
                loc = await self.bot.find_placement(CREEPTUMOR, self.bot.townhalls.center)
                if not loc:
                    loc = self.bot.townhalls.closest_to(self.bot.game_info.map_center).position.towards(self.bot.game_info.map_center, 6)
                for u in self.bot.townhalls:
                    await self.bot.do(u(RALLY_UNITS, loc))
        if unit.type_id in [EXTRACTOR, ASSIMILATOR, REFINERY]:
            self.extractors.append(unit.tag)
    async def unitCreated(self, unit):
        if unit in self.bot.townhalls:
            await self.bot.do(unit(RALLY_WORKERS, self.bot.state.mineral_field.closest_to(unit)))
            if unit.type_id == HATCHERY:
                await self.bot.do(unit(RALLY_UNITS, unit.position.towards(self.bot.game_info.map_center, 6)))
        elif unit.type_id in {UnitTypeId.EXTRACTOR, UnitTypeId.ASSIMILATOR, UnitTypeId.REFINERY}:
            self.extractors.append(unit.tag)
            g = self.bot.state.vespene_geyser.closest_to(self.bot.units.find_by_tag(unit.tag))
            if g:
                if g.tag in self.geysertags:
                    self.geysertags.remove(g.tag)
                else:
                    print('WorkerMgmt:UnitCreated: geyser tag not in geysertags')


    async def unitDestroyed(self, tag): #TODO: check if this even works on state.mineral_field
        if tag in [x.tag for x in self.bot.state.units.mineral_field]:
            print('WorkerMgmt: mineral field died in unitDestroyed')
            await self.bot.distribute_workers()
        elif tag in self.extractors:
            self.extractors.remove(tag)
        elif tag in self.townhalltags:
            print('WorkerMgmt: townhall died, removing tag from townhalltags,', tag)
            self.townhalltags.remove(tag)