import sc2, random
from sc2.data import ActionResult
from sc2 import run_game, maps, Race, Difficulty
from sc2.player import Bot, Computer, Human
from sc2.constants import *
from sc2.position import Point2, Point3
import math

class Creep:
    '''
    Deals with all queen related stuff, 'cept for transfuses
    Responsibilities:
        [X] injects from specific injectors
        [ ] injectors go to a hatch that they can inject from
        [ ] queens defend attacks
        [X] noninjectors put a creep tumor
        [X] noninjectors keep putting tumors
        [ ] noninjectors place tumors more intelligently (dont spam towards already-spammed goal)
        [X] tumors spread themselves
        [X] Queens replace dead tumors
        [ ] Queens replace dead tumors intelligently
    '''
    #do injects, do everything for creep
    def __init__(self, bot):
        self.bot = bot
        self.creepers = []  # tags only
        self.injectors = []  # tags only
        self.max_injectors = 4
        self.goals = []
        self.queenpriority = [] #Point2; places for queens to prioritise tumors
        self.occupied = []
        self.activetumors = {} #tag:Point2
        self.inactivetumors = {} #tag:Point2
        self.firststep = False

    async def step(self, iter):
        if self.firststep == False:
            self.expos = self.bot.expansion_locations.keys()
            self.goals += [x.towards(self.bot.game_info.map_center, 6) for x in self.expos]
            #done: add all ramps into goals, so that it *tries* to spread through the attack lanes
            #TODO: Fix the ramp spots
            self.goals += [Point2.center(x.points).rounded for x in self.bot.game_info.map_ramps]

            self.firststep = True
        else:
            activetumors = self.bot.units.tags_in(self.activetumors)
            thready = self.bot.townhalls.ready
            self.bot._client.debug_text_screen('Creep: ActiveTurmos: {}'.format(len(activetumors)), pos=(0.1, 0.2))
            queenspots = []
            for source in activetumors | thready:
                a = source.position.towards(source.position.closest(self.goals), 8)
                queenspots.append(a)
                self.bot._client.debug_box_out(Point3((a.x-0.4, a.y-0.4, 5)), Point3((a.x+0.4, a.y+0.4, 14)), Point3((20,255,255)))
            queenspots = await self.bot.can_place_list(self.bot._game_data.abilities[ZERGBUILD_CREEPTUMOR.value], queenspots)

            #inject
            await self.bot.inject(self.injectors)
            #creep by queens
            for t in self.creepers:
                q = self.bot.units.find_by_tag(t)
                if not q:
                    continue
                self.bot._client.debug_sphere_out(q.position3d, 1, color=Point3((255,100,255)))
                if q.energy >= 25 and len(activetumors) < 10 and q.is_idle: #no more than 10 active for now
                    if len(self.queenpriority) > 0:
                        tgt = q.position.sort_by_distance(self.queenpriority)
                        for t in tgt:
                            print('Creep: Available queen, priority > 0, found target', t)
                            err = await self.bot.do(q(AbilityId.BUILD_CREEPTUMOR_QUEEN, t))
                            if not err:
                                self.queenpriority.remove(t)
                                print('Creep: queen shouldve dealt with dead tumor')
                                continue
                    if len(queenspots) > 0:
                        s = self.bot.start_location.furthest(queenspots).rounded
                        err = await self.bot.do(q(AbilityId.BUILD_CREEPTUMOR_QUEEN, s))
                        if err:
                            print('Creep: failed queen build', err)

            #creep by tumors
            for c in activetumors:
                if c.type_id == UnitTypeId.CREEPTUMOR:
                    self.bot._client.debug_sphere_out(c.position3d, 1, color=Point3((50,50,200)))
                if c.type_id == UnitTypeId.CREEPTUMORBURROWED:
                    self.bot._client.debug_sphere_out(c.position3d, 1, color=Point3((50, 200, 50)))
                if c.type_id == UnitTypeId.CREEPTUMORQUEEN:
                    self.bot._client.debug_sphere_out(c.position3d, 1, color=Point3((200, 50, 50)))
                if AbilityId.BUILD_CREEPTUMOR_TUMOR in await self.bot.get_available_abilities(c):
                    if c.is_idle:
                        target = c.position.closest(self.goals)
                        self.bot._client.debug_line_out(Point3((c.position.x, c.position.y, c.position3d.z+0.5)),  Point3((target.x, target.y, c.position3d.z+0.5)))
                        if c.distance_to(target) < 10:
                            loc = await self.bot.find_placement(AbilityId.BUILD_CREEPTUMOR_TUMOR, target)
                            err = await self.bot.do(c(AbilityId.BUILD_CREEPTUMOR_TUMOR, loc))
                            if not err:
                                self.goals.remove(target)
                                self.occupied.append(target)
                                print('Creep: Got target', target)
                                self.activetumors[c.tag] = None
                                self.inactivetumors[c.tag] = c.position
                                continue
                        else:
                            ab = self.bot._game_data.abilities[ZERGBUILD_CREEPTUMOR.value]
                            locs = target.sort_by_distance([Point2((c.position.x + d*math.cos(math.pi*0.0675*a), c.position.y+d*math.sin(math.pi*0.0675*a))).rounded for a in range(32) for d in {6, 7, 8, 9, 10}])
                            locsmask = await self.bot._client.query_building_placement(ab, locs) #TODO:replace with the new self.bot.can_place_list
                            locs = [l for i, l in enumerate(locs) if locsmask[i] == ActionResult.Success]
                            expo = c.position.closest(self.expos)
                            i = 0
                            for a in locs:
                                i += 1
                                self.bot._client.debug_box_out(Point3((a.x-0.45, a.y-0.45, c.position3d.z+0.5)), Point3((a.x+0.45, a.y+0.45, c.position3d.z+0.5)), color=Point3((255-i*255/len(locs), 0, 0)))
                            #self.bot._client.debug_line_out(c, expo)
                            #ans = None
                            for l in locs:
                                if l._distance_squared(expo) > 16:
                                    err = await self.bot.do(c(AbilityId.BUILD_CREEPTUMOR_TUMOR, l))
                                    if err:
                                        print('creep: tumor error', c, err)
                                        continue
                                    if not err:
                                        print('creep: awaited tumor from ring, l:', l)
                                        print('Creep: Removing from active tumors,', c.tag)
                                        del self.activetumors[c.tag]
                                        self.inactivetumors[c.tag] = c.position
                                        break
                                else:
                                    continue
                #else:
                    #print('Creep: Removing from active tumors,', c.tag)
                    #del self.activetumors[c.tag]
                    #self.inactivetumors[c.tag] = c.position



    async def buildingConstructionComplete(self, unit):
        if unit.type_id in {UnitTypeId.CREEPTUMOR, UnitTypeId.CREEPTUMORQUEEN, UnitTypeId.CREEPTUMORBURROWED}:
            if unit.tag not in self.activetumors:
                self.activetumors[unit.tag] = unit.position
                print('Creep: added to activetumors:', self.activetumors)
            else:
                print('already in activetumors, type:', unit.type_id)
    async def unitCreated(self, unit):
        if unit.type_id == UnitTypeId.CREEPTUMOR or unit.type_id == UnitTypeId.CREEPTUMORQUEEN:
            target = unit.position.closest(self.goals)
            if unit.distance_to(target) < 10:
                #self.goals.remove(target)
                #self.occupied.append(target)
                print('Creep: removed target')

    async def unitDestroyed(self, tag): #TODO: check if this even works on state.mineral_field
        if tag in self.creepers:
            self.creepers.remove(tag)
        if tag in self.injectors:
            self.injectors.remove(tag)
        if tag in self.activetumors:
            self.queenpriority.append(self.activetumors[tag])
            print('Creep: Active tumor died, appending', self.activetumors[tag])
            del self.activetumors[tag]
            print('Creep: queen priority now', self.queenpriority)
        if tag in self.inactivetumors:
            self.queenpriority.append(self.inactivetumors[tag])
            print('Creep: Inactive tumor died, appending', self.inactivetumors[tag])
            del self.inactivetumors[tag]




