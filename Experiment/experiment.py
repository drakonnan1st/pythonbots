import sc2, random
from sc2.data import ActionResult
from sc2 import run_game, maps, Race, Difficulty
from sc2.player import Bot, Computer, Human
from sc2.constants import *
from sc2.position import Point2, Point3

from basebot import BaseBot


class Experiment(BaseBot):
    def __init__(self):
        super().__init__()
        self.ais = []


    async def on_step(self, iteration):
        if iteration == 0:
            await self.chat_send('bot started')




        await self.dostuff()



def main():
    sc2.run_game(maps.get("MicroTest01"), [
    Bot(Race.Protoss, Experiment())],
    realtime=False)

if __name__ == '__main__':
    main()
