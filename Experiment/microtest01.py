import sc2, random
from sc2.data import ActionResult
from sc2 import run_game, maps, Race, Difficulty
from sc2.player import Bot, Computer, Human
from sc2.constants import *
from sc2.position import Point2, Point3

from basebot import BaseBot
from Experiment.offense import attacker


class MicroTest01:
    def __init__(self, bot):
        self.bot = bot
        self.stalkers = None
        self.waypoint = Point2((100,100))
        self.retreat = Point2((25, 25))

    async def step(self, itera):
        if self.stalkers == None:
            self.stalkers = attacker(self.bot.units(UnitTypeId.ROACH))
            self.bot.warmonger.platoons.append(self.stalkers)
            self.stalkers.atk(self.waypoint, self.retreat)
            print('attacker created')



    async def buildingConstructionComplete(self, unit):
        pass
    async def unitCreated(self, unit):
        pass
    async def unitDestroyed(self, tag):
        pass
