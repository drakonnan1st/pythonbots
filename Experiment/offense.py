import sc2, random
from sc2.data import ActionResult
from sc2.unit import Unit
from sc2.units import Units
from sc2 import run_game, maps, Race, Difficulty
from sc2.player import Bot, Computer, Human
from sc2.constants import *
from sc2.position import Point2, Point3
from typing import Union, List





class attacker:
    def __init__(self, army = Union[int, "Unit", "Units", List[int]]):
        if isinstance(army, int):
            self.Single = True
            self.army = army
        elif isinstance(army, Unit):
            self.Single = True
            self.army = army.tag
        elif isinstance(army, Units):
            self.Single = False
            self.army = [x.tag for x in army]
        elif isinstance(army, List):
            self.Single = False
            self.army = army
        self.goal = None #Point2, will try to get there
        self.retreat = None #also Point2
        self.phase = 0 #0: idle, 1: in transit, 2: attacking, 3: retreating
        self.should_kite = True

    def atk(self, goal, retreat, should_kite=True):
        self.goal = goal
        self.retreat = retreat
        self.should_kite = should_kite




class OffenseManager:
    def __init__(self, bot):
        self.bot = bot
        self.platoons = [] #list of attackers


    async def step(self, itera):
        for platoon in self.platoons:
            if platoon.goal != None:
                if platoon.phase == 0:
                    if platoon.Single == True:
                        u = self.bot.units.find_by_tag(platoon.army)
                        if u:
                            await self.bot.do(u.move(platoon.goal))
                            print('platoon phase now 1')
                            platoon.phase = 1
                    else:
                        us = self.bot.units.tags_in(platoon.army)
                        for u in us:
                            await self.bot.do(u.move(platoon.goal))
                        platoon.phase = 1
                        print('platoon phase now 1')
                elif platoon.phase == 1:
                    u = None
                    if platoon.Single:
                        u = self.bot.units.find_by_tag(platoon.army)
                        if u.is_idle:
                            await self.bot.do(u.move(platoon.goal))
                    else:
                        us = self.bot.units.tags_in(platoon.army)
                        if us:
                            if us.idle:
                                for a in us:
                                    await self.bot.do(a.move(platoon.goal))
                            u = self.bot.units.tags_in(platoon.army).closest_to(platoon.goal)
                    if u:
                        if u.position._distance_squared(platoon.goal) <= 1600 or self.bot.known_enemy_units.exclude_type({UnitTypeId.LARVA}).closer_than(15, u):
                            if platoon.Single:
                                platoon.phase = 2
                                print('platoon phase now 2')
                                await self.bot.do(u.attack(platoon.goal))
                            else:
                                for u in self.bot.units.tags_in(platoon.army):
                                    await self.bot.do(u.attack(platoon.goal))
                                platoon.phase = 2
                                print('platoon phase now 2')
                elif platoon.phase == 2:
                    tags = []
                    if platoon.Single:
                        tags.append(platoon.army)
                    else:
                        tags = platoon.army
                    for u in self.bot.units.tags_in(tags):
                        #This is where the fun begins
                        if self.bot.known_enemy_units.exclude_type({UnitTypeId.LARVA}).closer_than(14, u):
                            target = self.bot.known_enemy_units.closest_to(u)
                            if platoon.should_kite == True:
                                if u.is_flying:
                                    tgtrnge = target.air_range
                                    rnge = u.air_range
                                else:
                                    tgtrnge = target.ground_range
                                    rnge = u.ground_range
                                if u.weapon_cooldown == 0:
                                    if not u.is_attacking:
                                        await self.bot.do(u.attack(target))
                                        continue
                                elif u.weapon_cooldown > 0:
                                    if rnge < tgtrnge:
                                        if not u.is_moving:
                                            print('u to move closer')
                                            await self.bot.do(u.move(target.position.towards(u, rnge/2)))
                                            self.bot._client.debug_line_out(u, target, Point3((0,255,255)))
                                            continue
                                    else:
                                        d = u.distance_to(target)
                                        self.bot._client.debug_line_out(u, target, Point3((255,100,100)))
                                        p = (u.position3d + target.position3d)/2
                                        p = Point3((p.x, p.y, (u.position3d.z + target.position3d.z)/2))
                                        self.bot._client.debug_text_world('dist: {:.2f}'.format(d), p)
                                        if d <= min(tgtrnge+2, rnge):
                                            print('u too close')
                                            if not u.is_moving:
                                                await self.bot.do(u.move(u.position.towards(target, -10)))
                                                continue
                                        elif d > rnge+3:
                                            print('u too far')
                                            if not u.is_attacking:
                                                await self.bot.do(u.attack(target))
                                                continue
                            else:
                                if not u.is_attacking:
                                    await self.bot.do(u.attack(target))
                                else:
                                    if u.order_target != target.tag:
                                        await self.bot.do(u.attack(target))
                        else:
                            if u.position._distance_squared(platoon.goal) > 1600:
                                if platoon.Single:
                                    await self.bot.do(u.move(platoon.goal))
                                    platoon.phase = 1
                                    print('platoon phase now 1')
                                else:
                                    for a in self.bot.units.tags_in(platoon.army):
                                        await self.bot.do(a.move(platoon.goal))
                                    platoon.phase = 1
                                    print('platoon phase now 1')
                                    break
                            else:
                                if not u.is_attacking:
                                    await self.bot.do(u.attack(platoon.goal))
                                else:
                                    if u.order_target != platoon.goal:
                                        await self.bot.do(u.attack(platoon.goal))


    async def buildingConstructionComplete(self, unit):
        pass
    async def unitCreated(self, unit):
        pass
    async def unitDestroyed(self, tag):
        for p in self.platoons:
            if p.Single:
                if p.army == tag:
                    self.platoons.remove(p)
                    del p
            else:
                if tag in p.army:
                    p.army.remove(tag)
