import sc2, random
from sc2.data import ActionResult
from sc2 import run_game, maps, Race, Difficulty
from sc2.player import Bot, Computer, Human
from sc2.constants import *
from sc2.position import Point2, Point3

class AggroZA:
    def __init__(self, bot):
        self.phase = 0
        self.bot = bot
        self.hatch = None
        self.army = []
        self.max_workers = 66 #idk why
        self.allin = True
        

    async def step(self, iter):
        if self.phase == 0: #get 16 drones
            if self.hatch == None:
                self.hatch = self.bot.units(HATCHERY).first
                self.enemmain = self.bot.enemy_start_locations[0]
            if self.bot.supply_used < 14:
                await self.bot.drone() #TODO make drone in basebot of all races
            else:
                if len(self.bot.units(EXTRACTOR)) < 2 and self.bot.supply_used <= 14:
                    if self.bot.minerals >= 75:
                        gs = self.bot.state.vespene_geyser.closer_than(10, self.hatch)
                        for g in gs:
                            if not self.bot.units(EXTRACTOR).closer_than(2,g).exists:
                                w = self.bot.units(DRONE).filter(lambda w: w.is_carrying_minerals == False).closest_to(g)
                                await self.bot.do(w.build(EXTRACTOR, g))
                                print('AggroZA::step::phase0: Built extractor', g)
                if len(self.bot.units(EXTRACTOR)) == 2 and self.bot.supply_used == 14:
                    for g in self.bot.units(EXTRACTOR):
                        await self.bot.do(g(CANCEL))
                if len(self.bot.workers) > 14:
                    for w in self.bot.workers:
                        await self.bot.do(w(SMART, self.hatch))
                        await self.bot.do(w(SMART, self.bot.state.mineral_field.closer_than(10,self.enemmain).furthest_to(self.enemmain), queue=True))
                    self.phase = 1
        if self.phase == 1: #in transit
            if len(self.bot.workers.closer_than(6, self.bot.state.mineral_field.closer_than(10,self.enemmain).furthest_to(self.enemmain))) > 12:
                for w in self.bot.workers.closer_than(20, self.enemmain):
                    targets = self.bot.known_enemy_units.not_structure
                    if targets:
                        await self.bot.do(w(ATTACK, targets.closest_to(w)))
                    else:
                        await self.bot.do(w(ATTACK, self.enemmain.towards(self.bot.state.mineral_field.closer_than(10, self.enemmain).furthest_to(self.enemmain).position, 3)))
                        print('warning, no drone targets')
                    self.army.append(w.tag)
                self.phase = 2
                self.groupA = []
                self.groupB = []
                print('phase now 2')
        elif self.phase == 2:
            await self.bot.drone()
            for t in self.army+self.groupA+self.groupB:
                if not self.bot.units.find_by_tag(t):
                    if t in self.army:
                        self.army.remove(t)
                    if t in self.groupA:
                        self.groupA.remove(t)
                    if t in self.groupB:
                        self.groupB.remove(t)
                    continue
                w = self.bot.units.by_tag(t)
                if w.is_attacking and w.orders[0].target in [x.tag for x in self.bot.known_enemy_structures]:
                    targets = self.bot.known_enemy_units.not_structure.exclude_type({LARVA})
                    if targets:
                        await self.bot.do(w(ATTACK, targets.closest_to(w)))
                if w.is_attacking and w.health < 20 and t in self.army:
                    await self.bot.do(w(SMART, self.bot.state.mineral_field.closest_to(self.hatch)))
                    self.army.remove(t)
                    self.groupA.append(t)
                    print('pulling back', w)
                if t in self.groupA and self.bot.known_enemy_units.not_structure:
                    if w.distance_to(self.bot.known_enemy_units.not_structure.closest_to(w)) < 1:
                        targets = self.bot.known_enemy_units.not_structure.exclude_type({LARVA})
                        if not targets:
                            targets = self.bot.known_enemy_units.exclude_type({LARVA})
                            if not targets:
                                await self.bot.do(w(SMART, self.bot.state.mineral_field.closest_to(self.hatch)))
                                self.groupA.remove(t)
                                continue
                        await self.bot.do(w.attack(targets.closest_to(w)))
                        print('redeployed', w)
                        self.groupA.remove(t)
                        self.groupB.append(t)
                elif t in self.groupA and not self.bot.known_enemy_units.not_structure:
                    await self.bot.do(w(SMART, self.bot.state.mineral_field.closest_to(self.hatch)))
                    self.groupA.remove(t)
                if w.health < 5 and t in self.groupB:
                    await self.bot.do(w(SMART, self.bot.state.mineral_field.closest_to(self.hatch)))
                    self.groupB.remove(t)
                    print('bringing back', w)
                if w.is_idle:
                    print('idle', w)
                    targets = self.bot.known_enemy_units.exclude_type({LARVA})
                    if targets:
                        await self.bot.do(w(ATTACK, targets.closest_to(w)))
                        continue
                    else:
                        print('lol cant find targets')
                        await self.bot.do(w(SMART, self.bot.state.mineral_field.closest_to(self.hatch)))
                        if t in self.army:
                            self.army.remove(t)
                        if t in self.groupA:
                            self.groupA.remove(t)
                        if t in self.groupB:
                            self.groupB.remove(t)
                        break
            if len(self.bot.workers.closer_than(20, self.enemmain)) == 0:
                print('lol attack failed')
                #self.phase = 4
                self.allin = False
        if self.phase == 4: #DISCONTINUED, MOVED TO MacroZ
            await self.bot.drone()
            if self.bot.supply_left <= 1 and not self.bot.already_pending(OVERLORD):
                for l in self.bot.units(LARVA):
                    if self.bot.can_afford(OVERLORD):
                        await self.bot.do(l.train(OVERLORD))
            if not self.bot.units(SPAWNINGPOOL).exists and self.bot.can_afford(SPAWNINGPOOL):
                d = self.bot.selectMineralDrone(th=self.hatch)
                await self.bot.do(d.build(SPAWNINGPOOL))
            if self.bot.units(SPAWNINGPOOL).exists and not self.bot.units(EXTRACTOR).exists:
                w = self.bot.selectMineralDrone(th=self.hatch)
                gs = self.bot.state.vespene_geyser.closer_than(10, self.hatch)
                for g in gs:
                    if not self.bot.units(EXTRACTOR).closer_than(2,g).exists:
                        w = self.bot.units(DRONE).filter(lambda w: w.is_carrying_minerals == False).closest_to(g)
                        await self.bot.do(w.build(EXTRACTOR, g))
                        break
            if self.bot.units(SPAWNINGPOOL).ready.exists:
                pass


    
    #needed for other bots
    async def buildingConstructionComplete(self, unit):
        pass
    async def unitCreated(self, unit):
        pass
    async def unitDestroyed(self, tag):
        pass
