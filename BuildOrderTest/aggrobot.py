import sc2, random
from sc2.data import ActionResult
from sc2 import run_game, maps, Race, Difficulty
from sc2.player import Bot, Computer, Human
from sc2.constants import *
from sc2.position import Point2, Point3

from basebot import BaseBot
from aggroza import AggroZA
from macroz import MacroZ
from workermgmt import WorkerMgmt
from creep import Creep

class AggroBot(BaseBot):
    def __init__(self):
        super().__init__()
        self.ais = []
        self.debug = True

    def on_start(self):
        self.ai = AggroZA(self)
        self.ais.append(self.ai)
        self.workermanager = WorkerMgmt(self)
        self.ais.append(self.workermanager)
        self.creep = Creep(self)
        self.ais.append(self.creep)

    async def on_step(self, iteration):
        if iteration == 0:
            await self.workersplit(self.townhalls.random)
            await self.chat_send('(glhf)')
        for ai in self.ais:
            await ai.step(iteration)
##        if iteration == 20:
##            self.ai = MacroZ(self)
##            print('hijacked to MacroZ')

        if self.ai.allin == False:
            self.ai = MacroZ(self)
            self.ais[0] = self.ai
        if self.debug == True:
            for t in self.workermanager.minlines:
                u = self.units.find_by_tag(t)
                if u:
                    if t in self.workermanager.lockedlines:
                        self._client.debug_text_world('locked', Point3((u.position.x, u.position.y, 14)))
                    else:
                        self._client.debug_text_world('not locked', Point3((u.position.x, u.position.y, 14)))
            for t in self.creep.goals:
                self._client.debug_sphere_out(Point3((t.x, t.y, 12)), 1, color = Point3((180,180,180)))
            h = await self.get_next_expansion()
            if h:
                self._client.debug_box_out(Point3((h.x-2.5, h.y-2.5, 10)), Point3((h.x+2.5,h.y+2.5,14)))
            a = self.units.find_by_tag(self.workermanager.activeline)
            if a:
                u = a.position.towards(self.state.mineral_field.closest_to(a), 3)
                self._client.debug_text_world('active', Point3((u.x, u.y, 14)))
            self._client.debug_text_screen('DRONE: {},\n already_pending: {}, \n dronecondition: {}'.format(
                self.units(UnitTypeId.DRONE).amount,
                self.already_pending(UnitTypeId.DRONE),
                min(self.max_workers, 16 * len(self.townhalls) + 3 * len(self.units(UnitTypeId.EXTRACTOR)))), pos=(0.1,0.1))
            await self._client.send_debug()

        #End of on_step()
        await self.dostuff()

    async def on_building_construction_complete(self, unit):
        for ai in self.ais:
            await ai.buildingConstructionComplete(unit)
        #if unit.type_id == EXTRACTOR:
            #await self.distribute_workers()
    async def on_unit_created(self, unit):
        for ai in self.ais:
            await ai.unitCreated(unit)
    async def on_unit_destroyed(self, unit_tag):
        for ai in self.ais:
            await ai.unitDestroyed(unit_tag)




run_game(maps.get("(2)LostandFoundLE"), [
    Bot(Race.Zerg, AggroBot()),
    Computer(Race.Zerg, Difficulty.Easy)],
    realtime=False)
