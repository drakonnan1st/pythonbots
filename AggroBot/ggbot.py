import sc2, random, time, asyncio
from sc2.data import ActionResult
from sc2 import maps, Race, Difficulty
from sc2.player import Bot, Computer, Human
from sc2.constants import *
from sc2.position import Point2, Point3

from basebot import BaseBot


class GGbot(BaseBot):
    '''GGs out instantly, but maybe try to start a worker earlier?'''
    def __init__(self):
        super().__init__()
        self.init = time.time()

    def on_start(self):
        self.start = time.time()
        print('time btwn init and start:', time.time() - self.init)
    async def on_step(self, iteration: int):
        if iteration == 0:
            print('time btwn start and step0:', time.time() - self.start)
            await self.chat_send('(gg)')
            await asyncio.sleep(1)
            await self._client.leave()

def main():
    sc2.run_game(sc2.maps.get("BlueshiftLE"), [
        Bot(Race.Terran, GGbot()),
        Computer(Race.Protoss, Difficulty.Easy)
    ], realtime=True)

if __name__ == '__main__':
    main()