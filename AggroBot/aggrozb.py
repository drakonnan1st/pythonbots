import sc2, random
from sc2.data import ActionResult
from sc2 import run_game, maps, Race, Difficulty
from sc2.player import Bot, Computer, Human
from sc2.constants import *
from sc2.position import Point2, Point3

from macroz import MacroZ

class AggroZB:
    '''
    12 pool
    14 OV stop droning
    @pool 3 sets of lings + send worker to nat
    17 hatch
    9 more sets of lings
    drone+queen
    '''
    def __init__(self, bot):
        self.phase = 0
        self.bot = bot
        self.hatch = None
        self.army = []
        self.max_workers = 66 #idk why
        self.allin = True
        self.pool = None
        self.poolloc = None
        self.natdrone = None
        self.atktarget = None
        self.enemynattag = None
        self.attacking = False
        self.army = []
        self.deadlings = []

    async def step(self, iter):
        if self.hatch == None:
            self.hatch = self.bot.townhalls.first
        if self.atktarget == None:
            self.atktarget = self.bot.enemy_start_locations[0]


        if self.attacking:
            await self.lingmicro()
            await self.macroBuildings()
            await self.macroLarva()
        else:
            await self.buildorder()



    async def buildorder(self):
        if self.bot.supply_used == 12 and self.bot.minerals > 150:
            if self.bot.units(UnitTypeId.OVERLORD).noqueue and self.bot.cartographer.ready:
                self.enemmain = self.bot.enemy_start_locations[0]
                enemnat = self.enemmain.closest(self.bot.cartographer.expofree)
                pillarlocs = enemnat.sort_by_distance(self.bot.cartographer.OVspots)
                if len(pillarlocs) > 1:
                    if self.enemmain._distance_squared(pillarlocs[0]) > self.enemmain._distance_squared(pillarlocs[1]):
                        loc = pillarlocs[0]
                    else:
                        loc = pillarlocs[1]
                    await self.bot.do(AbilityId.SMART, self.bot.units(UnitTypeId.OVERLORD).noqueue.first.tag, loc)
            if self.pool == None:
                if not self.bot.cartographer.expofree:
                    return
                self.pool = self.bot.selectMineralDrone(self.hatch)
                gas = self.bot.state.vespene_geyser.closer_than(10, self.hatch).closest_to(
                    self.bot.cartographer.expofree[0]).position
                if self.hatch.position.x <= gas.x and self.hatch.position.y > gas.y:  # gas southwest
                    self.poolloc = Point2((gas.x - 3, gas.y - 1))
                elif self.hatch.position.x > gas.x and self.hatch.position.y <= gas.y:  # gas northeast
                    self.poolloc = Point2((gas.x + 3, gas.y + 1))
                if self.poolloc:
                    await self.bot.do(AbilityId.MOVE, self.pool.tag, self.poolloc)
        if not self.bot.units(UnitTypeId.SPAWNINGPOOL) and self.bot.minerals >= 200:
            if self.pool and self.poolloc:
                await self.bot.do(UnitTypeId.SPAWNINGPOOL, self.pool.tag, self.poolloc)
                await self.bot.do(AbilityId.RALLY_UNITS, self.hatch.tag, self.enemmain.closest(self.bot.cartographer.expofree).towards(self.hatch, 5))
            else:
                print('AggroZB: no drone or pool location')
        if self.bot.units(UnitTypeId.SPAWNINGPOOL) and self.bot.supply_cap <= 14:
            if self.bot.supply_used < 14:
                await self.bot.drone()
            if self.bot.supply_left <= 0 and not self.bot.already_pending(UnitTypeId.OVERLORD):
                if self.bot.minerals >= 100:
                    l = self.bot.units(LARVA)
                    if l:
                        await self.bot.do(UnitTypeId.OVERLORD, l.first.tag)
        if 14 < self.bot.supply_cap < 23:
            if self.bot.supply_used < 17 and self.bot.units(UnitTypeId.SPAWNINGPOOL).ready:
                larvatags = [x.tag for x in self.bot.units(UnitTypeId.LARVA)]
                count = min(len(larvatags), int(self.bot.minerals/50), 17-self.bot.supply_used)
                if count:
                    tags = larvatags[:count]
                    await self.bot.do(UnitTypeId.ZERGLING, tags)
            if self.bot.supply_used > 15 and self.natdrone == None and self.bot.cartographer.expofree:
                self.natdrone = self.bot.selectMineralDrone(self.hatch)
                nat = self.bot.cartographer.expofree[0]
                await self.bot.do(AbilityId.MOVE, self.natdrone.tag, nat)
            if self.bot.supply_used >= 17 and self.bot.townhalls.amount < 2 and self.natdrone:
                if self.bot.minerals >= 300:
                    loc = self.bot.cartographer.expofree[0]
                    await self.bot.do(UnitTypeId.HATCHERY, self.natdrone.tag, loc)
            if self.bot.units(UnitTypeId.ZERGLING).amount + 2*self.bot.already_pending(UnitTypeId.ZERGLING) < 12 and self.bot.townhalls.amount > 1:
                larvatags = [x.tag for x in self.bot.units(UnitTypeId.LARVA)]
                count = min(len(larvatags),
                            int(self.bot.minerals/50),
                            12-self.bot.units(UnitTypeId.ZERGLING).amount+2*self.bot.already_pending(UnitTypeId.ZERGLING))
                if count > 0:
                    tags = larvatags[:count]
                    await self.bot.do(UnitTypeId.ZERGLING, tags)
            if self.bot.units(UnitTypeId.ZERGLING).amount + 2*self.bot.already_pending(UnitTypeId.ZERGLING) >= 12:
                if not self.attacking:
                    self.attacking = True
                if self.hatch.noqueue and self.bot.minerals >= 150:
                    await self.bot.do(UnitTypeId.QUEEN, self.hatch.tag)
                if self.bot.units(UnitTypeId.QUEEN).amount + self.bot.already_pending(UnitTypeId.QUEEN) > 0:
                    if self.bot.already_pending(UnitTypeId.OVERLORD) == 0:
                        if self.bot.units(UnitTypeId.LARVA) and self.bot.minerals >= 100:
                            l = self.bot.units(UnitTypeId.LARVA).first.tag
                            await self.bot.do(UnitTypeId.OVERLORD, l)
                    else:
                        await self.bot.drone()
        if self.bot.supply_cap > 23:
            await self.bot.drone()


    async def lingmicro(self):
        if self.attacking == True:
            if not self.army:
                self.attacking = False
                self.allin = False
            else:
                for z in self.bot.units(UnitTypeId.ZERGLING):
                    col = Point3((255,255,255))

                    if z.position._distance_squared(self.atktarget) > 2500:
                        col = Point3((100,100,255))
                        if z.is_idle:
                            await self.bot.do(MOVE, z.tag, self.atktarget)
                        if self.bot.known_enemy_units.exclude_type({UnitTypeId.LARVA}).closer_than(12, z):
                            if not z.is_attacking:
                                await self.bot.do(ATTACK, z.tag, self.atktarget)
                            col =Point3((255,0,0))
                    else:
                        col = Point3((255,100,255))
                        if self.bot.known_enemy_units.of_type({UnitTypeId.ZERGLING}).closer_than(4, z) or self.bot.known_enemy_units.of_type({UnitTypeId.QUEEN, UnitTypeId.SPINECRAWLER, UnitTypeId.ROACH}).closer_than(7, z):
                            closest = self.bot.known_enemy_units.of_type({UnitTypeId.ZERGLING, UnitTypeId.DRONE, UnitTypeId.QUEEN, UnitTypeId.SPINECRAWLER, UnitTypeId.ROACH}).closest_to(z)
                            if not z.is_attacking:
                                await self.bot.do(ATTACK, z.tag, closest)
                                col = Point3((255,50,100))
                            else:
                                if z.orders[0].target != closest:
                                    await self.bot.do(ATTACK, z.tag, closest)
                                    col = Point3((200, 50, 50))
                        elif self.bot.known_enemy_units.of_type({UnitTypeId.DRONE}).closer_than(7, z):
                            closest = self.bot.known_enemy_units.of_type({UnitTypeId.DRONE}).closest_to(z)
                            if not z.is_attacking:
                                await self.bot.do(ATTACK, z.tag, closest)
                                col = Point3((200, 0, 50))
                            else:
                                if z.orders[0].target != closest:
                                    await self.bot.do(ATTACK, z.tag, closest)
                                    col = Point3((200, 50, 50))
                        elif self.bot.known_enemy_units.of_type({UnitTypeId.HATCHERY, UnitTypeId.LAIR}).closer_than(12, z):
                            closest = self.bot.known_enemy_units.of_type({UnitTypeId.HATCHERY, UnitTypeId.LAIR}).closest_to(z)
                            if self.enemynattag == None:
                                self.enemynattag = closest.tag
                                print('got your natural:', self.enemynattag)
                            if not z.is_attacking:
                                await self.bot.do(ATTACK, z.tag, closest)
                                col = Point3((200, 0, 50))
                            else:
                                if z.orders[0].target != closest:
                                    await self.bot.do(ATTACK, z.tag, closest)
                                    col = Point3((200, 50, 50))
                        elif self.bot.known_enemy_units.exclude_type({UnitTypeId.LARVA}):
                            closest = self.bot.known_enemy_units.exclude_type({UnitTypeId.LARVA}).closest_to(z)
                            if closest:
                                if not z.is_attacking:
                                    await self.bot.do(ATTACK, z.tag, closest)
                                    col = Point3((200, 0, 50))
                                elif z.orders[0].target != closest:
                                    await self.bot.do(ATTACK, z.tag, closest)
                                    col = Point3((200, 50, 50))
                        else:
                            if z.is_idle:
                                await self.bot.do(ATTACK, z.tag, self.atktarget)

                    if z.order_target:
                        tgt = None
                        if isinstance(z.order_target, int):
                            tgt1 = self.bot.units.find_by_tag(z.order_target)
                            if tgt1:
                                tgt = tgt1.position3d
                        elif isinstance(z.order_target, Point2):
                            tgt = Point3((z.order_target.x, z.order_target.y, z.position3d.z))
                        if tgt:
                            self.bot._client.debug_line_out(z, tgt, col)


    async def macroBuildings(self):
        #incase pool ded:
        if not self.bot.units(UnitTypeId.SPAWNINGPOOL):
            if self.bot.supply_workers > 6 and self.bot.minerals > 200 and not self.bot.already_pending(UnitTypeId.SPAWNINGPOOL):
                w = self.bot.selectMineralDrone(self.hatch)
                mins = self.bot.state.mineral_field.closer_than(10, self.hatch).center
                loc = await self.bot.find_placement(UnitTypeId.SPAWNINGPOOL, self.hatch.position.towards(mins, -8))
                if loc:
                    await self.bot.do(UnitTypeId.SPAWNINGPOOL, w.tag, loc)
        #incase hatch ded
        if self.bot.townhalls.amount < 2 and self.bot.already_pending(UnitTypeId.HATCHERY) - self.bot.units(UnitTypeId.HATCHERY).not_ready.amount == 0:
            if self.bot.minerals >= 300:
                w = self.bot.selectMineralDrone(self.hatch)
                loc = self.bot.cartographer.expofree[0]
                await self.bot.do(UnitTypeId.HATCHERY, w.tag, loc)
        #take gas 1:
        if not self.bot.units(UnitTypeId.EXTRACTOR):
            if self.bot.supply_workers > 16 and self.bot.minerals > 75 and self.bot.already_pending(UnitTypeId.EXTRACTOR) - self.bot.units(UnitTypeId.EXTRACTOR).not_ready.amount == 0:
                await self.bot.takeGas()
        #TODO: make more idk

    async def macroLarva(self):
        if self.bot.units(UnitTypeId.SPAWNINGPOOL).ready:
            poolready = True
        else:
            poolready = False
        if self.bot.units(UnitTypeId.ROACHWARREN).ready:
            roachready = True
        else:
            roachready = False
        lingcount = self.bot.units(UnitTypeId.ZERGLING).amount + 2*self.bot.already_pending(UnitTypeId.ZERGLING)
        roachcount = self.bot.units(UnitTypeId.ROACH).amount + self.bot.already_pending(UnitTypeId.ROACH)
        dronecount = self.bot.supply_workers
        lingsneeded = 0 if (roachready or not poolready) or lingcount >= 16 else 16-lingcount
        roachesneeded = 0 if (not roachready) or roachcount >= 8 else 8-roachcount
        dronesneeded = max(0, min(self.max_workers, 16*self.bot.townhalls.amount + 3*self.bot.units(UnitTypeId.EXTRACTOR).amount) - dronecount) #TODO: use ai numbers
        pendingOVs = self.bot.already_pending(UnitTypeId.OVERLORD)
        lps = self.bot.townhalls.amount/11 + len(self.bot.creep.injectors)*3/30
        if (self.bot.supply_used > 12 and self.bot.supply_cap == 14) or (self.bot.supply_used > 18 and self.bot.supply_cap == 22):
            OVsneeded = 1
            print('Larva: need hardcoded 1 OV')
        elif self.bot.supply_cap + pendingOVs*8 >= 200:
            OVsneeded = 0
            print('maxed now')
        else:
            if self.bot.units(UnitTypeId.ROACHWARREN): #producing at 2 supply per larva
                OVsneeded = -(self.bot.supply_left - 36*lps)//8 - pendingOVs #weird negatives to avoid need for ceil()
            else:
                OVsneeded = -(self.bot.supply_left - 18*lps)//8 - pendingOVs
            OVsneeded = 0 if OVsneeded < 0 else int(OVsneeded) #need int

        if roachcount >= 8 and self.bot.units(RAVAGER).amount * 8 < roachcount and self.bot.units(RAVAGER).amount < 8:
            if self.bot.can_afford(RAVAGER) and self.bot.units(UnitTypeId.ROACH):
                if self.bot.known_enemy_units:
                    r = self.bot.units(ROACH).furthest_to(
                        self.bot.known_enemy_units.random)
                else:
                    r = self.bot.units(UnitTypeId.ROACH).furthest_to(self.bot.enemy_start_locations[0])
                await self.bot.do(AbilityId.MORPHTORAVAGER_RAVAGER, r.tag)

        larvatags = [x.tag for x in self.bot.units(UnitTypeId.LARVA)]
        #priority 1: OVs
        if OVsneeded:
            count = min(OVsneeded, len(larvatags), int(self.bot.minerals/100))
            print('OVs needed:', OVsneeded, '; supply:', self.bot.supply_used, '/', self.bot.supply_cap, '; lps:', lps)
            if count:
                await self.bot.do(UnitTypeId.OVERLORD, larvatags[:count])
                self.bot.minerals -= 100*count
                del larvatags[:count]
            else:
                return
        if lingsneeded and not self.attacking: #dont reinforce allin
            count = min(int(lingsneeded/2), len(larvatags), int(self.bot.minerals/50), self.bot.supply_left)
            if count:
                await self.bot.do(UnitTypeId.ZERGLING, larvatags[:count])
                self.bot.minerals -= 50*count
                self.bot.supply_left -= count
                del larvatags[:count]
            else:
                return
        if roachesneeded:
            count = min(roachesneeded, len(larvatags), int(self.bot.minerals/75), int(self.bot.vespene/25), int(self.bot.supply_left/2))
            if count:
                await self.bot.do(UnitTypeId.ROACH, larvatags[:count])
                self.bot.minerals -= 75 * count
                self.bot.vespene -= 25*count
                self.bot.supply_left -= 2*count
                del larvatags[:count]
            else:
                return
        if dronesneeded:
            count = min(dronesneeded, len(larvatags), int(self.bot.minerals/50), self.bot.supply_left)
            if count:
                await self.bot.do(UnitTypeId.DRONE, larvatags[:count])
                self.bot.minerals -= 50*count
                self.bot.supply_left -= count
                del larvatags[:count]
            else:
                return
        #roaches:
        if roachready:
            count = min(len(larvatags), int(self.bot.minerals/75), int(self.bot.vespene/25), int(self.bot.supply_left/2))
            if count:
                await self.bot.do(UnitTypeId.ROACH, larvatags[:count])
                self.bot.minerals -= 75 * count
                self.bot.vespene -= 25*count
                self.bot.supply_left -= 2*count
                del larvatags[:count]
            else:
                return
        else:
            count = min(len(larvatags), int(self.bot.minerals/50), self.bot.supply_left)
            if count:
                await self.bot.do(UnitTypeId.ZERGLING, larvatags[:count])
                self.bot.minerals -= 50*count
                self.bot.supply_left -= count
                del larvatags[:count]





    async def buildingConstructionComplete(self, unit):
        pass

    async def unitCreated(self, unit):
        if unit.type_id == UnitTypeId.SPAWNINGPOOL:
            self.pool = unit
        if unit.type_id == UnitTypeId.ZERGLING and len(self.army) < 12:
            self.army.append(unit.tag)

    async def unitDestroyed(self, tag):
        print('UnitDestroyed:', tag)
        if self.enemynattag:
            if tag == self.enemynattag:
                print('AggroZB: killed enemy natural')
                if self.bot.townhalls:
                    hatch = self.bot.townhalls.closest_to(self.atktarget)
                    self.allin = False
                    for z in self.bot.units(UnitTypeId.ZERGLING):
                        await self.bot.do(MOVE, z.tag, hatch.position.towards(self.atktarget, 6))
        if tag in self.army:
            self.deadlings.append(tag)
            if len(self.deadlings) >= 12:
                self.allin = False
                print('AggroZB: too many ded lings; retreat')
                await self.bot.do(AbilityId.MOVE, [u.tag for u in self.bot.units(UnitTypeId.ZERGLING)], self.bot.townhalls.closest_to(self.atktarget).position.towards(self.atktarget, 6))