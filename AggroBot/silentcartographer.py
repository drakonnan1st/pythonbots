import time
from sc2.data import ActionResult
from sc2.constants import *
from sc2.position import Point2, Point3

class SilentCartographer:
    '''everything map-related and map awareness
    Responsibilities:
        [X] track expansions better than default
        [X] track enemy expansions
        [ ] do build order location json`s
        [ ] get building placements if no build order locations
        [X] deal with OV pillars
        [X] deal with OVs? '''

    def __init__(self, bot, jason=None, handle_OVs=True):
        self.bot = bot
        self.ready = False
        self.bo = jason
        self.expofree = [] # Point2
        self.exporanked = [] # Point2
        self.expomine = {} # tag: Point2
        self.expoenem = {} # tag: Point2
        self.heightmap = None
        self.shouldHandleOVs = handle_OVs
        self.OVs = [] # tags
        self.OVPillars = [] # Point2
        self.OVspots = [] # Point2
        self.OVspotsOccupied = {} # Point2: tag
        self.OVspotsRedundant = [] # Point2
        #self.enemtownhalltags = [] # tags #TODO: move to appropriate core
        #TODO: more

    async def step(self, iter):
        if self.ready == False:
            a = time.time()
            #setup own bases
            freebase = list(self.bot.expansion_locations.keys())
            ##sort expofree
            self.expofree = [self.bot.game_info.player_start_location]
            while freebase:
                viableExpos = {x.closest(freebase): x.distance_to_closest(freebase) for x in self.expofree}
                chosen = min(viableExpos, key=viableExpos.get)
                if chosen not in self.expofree:
                    self.expofree.append(chosen)
                freebase.remove(chosen)
            #self.exporanked = [i for i in self.expofree]
            ##seperate owned bases
            if len(self.bot.enemy_start_locations) == 1:
                loc = self.bot.enemy_start_locations[0]
                print('Cartographer: confirm len(enemy_start_locations) = 1')
            else:
                loc = self.bot.game_info.map_center
            for th in self.bot.townhalls.sorted_by_distance_to(loc, reverse=True):
                expo = th.position.closest(self.expofree)
                self.expomine[th.tag] = expo
                self.expofree.remove(expo)


            b = time.time()
            print('Cartographer: part1:', b-a)
            #setup enemy bases
            enems = self.bot.known_enemy_structures.of_type(self.bot.townhallIDs)
            if enems:
                for th in enems:
                    expo = th.position.closest(self.expofree)
                    self.expoenem[th.tag] = expo
                    self.expofree.remove(expo)
            else:
                print('Cartographer: no enemy bases; len(enemStartLoc):', len(self.bot.enemy_start_locations))
                if len(self.bot.enemy_start_locations) == 1:
                    loc2 = self.bot.enemy_start_locations[0].closest(self.expofree)
                    print('loc2:', loc2)
                    self.expoenem[0] = loc2
                    print('got expoenem[0]:', self.expoenem[0])
                    self.expofree.remove(loc2)


            c = time.time()
            print('Cartographer: part2:', c-b)
            # OV stuff
            if self.shouldHandleOVs:
                # setup OVs
                self.OVs = [x.tag for x in self.bot.units(UnitTypeId.OVERLORD)]
                # setup OV Pillars
                self.heightmap = self.bot.game_info.terrain_height
                pillarPoint2 = {Point2((x,y)): self.heightmap[(x,y)] >= 143
                               for x in range(self.heightmap.width)
                               for y in range(self.heightmap.height)}
                pillarPoint2 = self.bot.game_info._find_groups({p for p in pillarPoint2 if pillarPoint2[p]}, 2)
                for group in pillarPoint2:
                    pos = Point2.center(group)
                    self.OVPillars.append(Point2((int(pos.x)+1, int(pos.y))))
                # setup OV spots, remove nearby pillars
                if len(self.expomine) > 1:
                    loc = self.expomine[1]
                elif self.expomine and len(self.expofree) > 1:
                    loc = self.expofree[1-len(self.expomine)]
                else:
                    loc = self.bot.game_info.player_start_location
                self.OVspots = self.expofree + [i for i in self.OVPillars if i._distance_squared(loc) > 900]
                d = time.time()
                print('Cartographer: Part3:', d-c)


            self.ready = True
        else:
            #update enemy bases:
            for p in set(self.expoenem.values()):
                if self.bot.is_visible(p) and not self.bot.known_enemy_structures.closer_than(3, p):
                    self.expoenem = {t: p2 for t, p2 in self.expoenem.items() if p2 != p }
                    self.expofree.append(p)
                    print('Cartographer: enemy base missing, now free')
            unaccountedEnemBases = self.bot.known_enemy_structures.of_type(
                self.bot.townhallIDs).tags_not_in(self.expoenem.keys())
            for u in unaccountedEnemBases:
                closest = u.position.closest(self.expofree)
                if u.position._distance_squared(closest) < 1:
                    self.expoenem[u.tag] = closest
                    self.expofree.remove(closest)
                    print('Cartographer: found enemy base')

                else:
                    if u.is_snapshot:
                        self.expoenem[u.tag] = u.position
                    print('Cartographer: new base already accounted for?; unaccountedBases:', unaccountedEnemBases)
                    if 0 in self.expoenem.keys():
                        if closest == self.expoenem[0]:
                            del self.expoenem[0]
                            print('Cartographer: deleted expoenem[0]')
                    print('Cartographer: enemy base misplaced?')
                    self.bot._client.debug_sphere_out(u.position3d, 3, Point3((255,100,255)))


    async def buildingConstructionComplete(self, unit):
        if unit in self.bot.townhalls:
            closestspot = unit.position.closest(self.OVspots+list(self.OVspotsOccupied.keys()))
            if unit.position._distance_squared(closestspot) < 1:
                if closestspot in self.OVspotsOccupied.keys():
                    tag = self.OVspotsOccupied.pop(closestspot)
                    self.OVspotsRedundant.append(closestspot)
                    ov = self.bot.units.find_by_tag(tag)
                    if ov:
                        locs = unit.position.sort_by_distance(self.OVspots)
                        if len(locs) > 0:
                            await self.bot.do(SMART, tag, locs[0])
                            self.OVspots.remove(locs[0])
                            self.OVspotsOccupied[locs[0]] = tag


                else:
                    self.OVspots.remove(closestspot)
                    self.OVspotsRedundant.append(closestspot)
    async def unitCreated(self, unit):
        if not self.ready:
            return
        if unit in self.bot.townhalls:
            loc = unit.position.closest(self.expofree)
            if unit.position._distance_squared(loc) <= 225:
                self.expomine[unit.tag] = loc
                self.expofree.remove(loc)
            else:
                print('Cartographer: hatchery too damn far away')
        elif unit.type_id == UnitTypeId.OVERLORD and self.shouldHandleOVs:
            locs = unit.position.sort_by_distance(self.OVspots)
            # print(locs)
            if len(locs) > 0:
                await self.bot.do(SMART, unit.tag, locs[0])
                self.OVspots.remove(locs[0])
                self.OVspotsOccupied[locs[0]] = unit.tag



    async def unitDestroyed(self, tag):
        # did th die:
        if tag in self.expomine.keys():
            self.expofree = [self.expomine.pop(tag)] + self.expofree #needs to be first on list
        elif tag in self.OVspotsOccupied.values():
            point = list(self.OVspotsOccupied.keys())[list(self.OVspotsOccupied.values()).index(tag)]
            self.OVspotsOccupied.pop(point)
            self.OVspots.append(point)

