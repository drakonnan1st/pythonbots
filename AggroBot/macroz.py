import sc2, random
from sc2.data import ActionResult
from sc2.constants import *
from sc2.position import Point2, Point3
from offense import attacker



class MacroZ:
    #Only call after allin fails, to try and recover
    #should already be dead, but whatevs
    def __init__(self, bot):
        self.bot = bot
        self.max_workers = 72
        self.OVspots = []
        self.OVspots += self.bot.expansion_locations
        self.OVspots.remove(self.bot.townhalls.first.position)
        #self.injectors = [] #tags only
        self.max_injectors = 4
        self.creepers = [] #tags only
        self.allin = True #DONT TOUCH
        self.poolstatus = 0 #0: no pool; 1: in construction; 2: ready
        self.pooltag = 0 #tag
        self.upg = [AbilityId.RESEARCH_ZERGMISSILEWEAPONSLEVEL1, AbilityId.RESEARCH_ZERGGROUNDARMORLEVEL1,
                    AbilityId.RESEARCH_ZERGMISSILEWEAPONSLEVEL2, AbilityId.RESEARCH_ZERGGROUNDARMORLEVEL2,
                    AbilityId.RESEARCH_ZERGMISSILEWEAPONSLEVEL3, AbilityId.RESEARCH_ZERGGROUNDARMORLEVEL3]
        self.army = []
        self.mainarmy = None
        self.lingGroup = None
        self.lair = 0
        self.roachwarrenloc = None
        self.townhalls = None
        self.ready = False
        self.defendertags = []

    async def step(self, iter):
        if not self.ready:
            if self.bot.units(UnitTypeId.SPAWNINGPOOL):
                u = self.bot.units(UnitTypeId.SPAWNINGPOOL).first
                self.pooltag = u.tag
                if u.is_ready:
                    self.poolstatus = 2
                else:
                    self.poolstatus = 1
            self.ready = True
        #do shit once
        self.townhalls = self.bot.townhalls

        await self.defend()
        await self.macroBuildings()
        await self.macroUpgrades()
        await self.macroLarva()
        if self.poolstatus == 2:
            await self.macroQueenProduction()
        #await self.bot.inject(self.injectors)
        #await self.creep()
        if self.bot.units(UnitTypeId.ZERGLING) and (not self.lingGroup) and self.bot.units(UnitTypeId.ROACH).amount >= 8:
            self.lingGroup = attacker(self.bot.units(UnitTypeId.ZERGLING), 'LingGroup')
            self.bot.atkmanager.platoons.append(self.lingGroup)
            self.lingGroup.atk(goal=self.bot.enemy_start_locations[0].towards(self.bot.game_info.map_center, 4), retreat=None, should_ignore={UnitTypeId.LARVA, UnitTypeId.OVERLORD, UnitTypeId.OVERSEER, UnitTypeId.MUTALISK, UnitTypeId.OVERLORDTRANSPORT, UnitTypeId.TRANSPORTOVERLORDCOCOON, UnitTypeId.CORRUPTOR})
        if not self.mainarmy:
            if self.bot.units(UnitTypeId.ROACH).amount > 30:
                toadd = self.bot.units(UnitTypeId.ROACH).take(30) + [u for u in self.bot.units(UnitTypeId.RAVAGER)]
                self.mainarmy = attacker(toadd, 'MainArmy')
                self.bot.atkmanager.platoons.append(self.mainarmy)
                self.mainarmy.atk(goal=self.bot.enemy_start_locations[0].towards(self.bot.game_info.map_center, 3), retreat=self.townhalls.center, should_kite=False, should_ignore={UnitTypeId.LARVA, UnitTypeId.OVERLORD, UnitTypeId.OVERSEER, UnitTypeId.MUTALISK, UnitTypeId.OVERLORDTRANSPORT, UnitTypeId.TRANSPORTOVERLORDCOCOON, UnitTypeId.CORRUPTOR})



    async def macroBuildings(self):
        #TODO: MOVE AWAY
        if not self.townhalls:
            for u in self.bot.units.not_structure.idle:
                ts = self.bot.known_enemy_units
                if ts:
                    t = self.bot.known_enemy_units.closest_to(u)
                else:
                    t = self.bot.enemy_start_locations[0] #TODO: switch to legit enemy location
                await self.bot.do(AbilityId.ATTACK, u.tag, t, should_combine=True)
            return
        #pool first
        if not self.poolstatus and self.bot.can_afford(UnitTypeId.SPAWNINGPOOL):
            if len(self.bot.workers) > 12:
                w = self.bot.selectMineralDrone(self.townhalls.first)
                #poolloc TODO: when geysers are next to each other
                th = self.townhalls.first.position
                if self.bot.cartographer.expofree:
                    h = self.bot.cartographer.expofree[0]
                else:
                    print('MacroZ:MacroBuildings: NO MORE BASES')
                    return
                gas = self.bot.state.vespene_geyser.closer_than(10, th).closest_to(h).position
                poolloc = None
                if th.x <= gas.x and th.y > gas.y: #gas southwest
                    poolloc = Point2((gas.x-3, gas.y-1))
                elif th.x > gas.x and th.y <= gas.y: #gas northeast
                    poolloc = Point2((gas.x+3, gas.y+1))
                if poolloc:
                    await self.bot.do(UnitTypeId.SPAWNINGPOOL, w.tag, poolloc)
        #gas after pool
        if self.poolstatus and self.bot.units(UnitTypeId.EXTRACTOR).amount+self.bot.already_pending(UnitTypeId.EXTRACTOR) < 1:
            if self.bot.can_afford(UnitTypeId.EXTRACTOR) and len(self.bot.workers) > 12:
                print('MacroBuildings: Gas condition true')
                g = self.bot.state.vespene_geyser.closest_to(self.bot.units(UnitTypeId.SPAWNINGPOOL).first)
                w = self.bot.selectMineralDrone(self.townhalls.first)
                await self.bot.do(UnitTypeId.EXTRACTOR, w.tag, g)
        #natural
        if self.poolstatus and len(self.townhalls) < 2:
            if self.bot.can_afford(UnitTypeId.HATCHERY) and len(self.bot.workers) >= 14:
                w = self.bot.selectMineralDrone(self.townhalls.first)
                if self.bot.cartographer.expofree:
                    await self.bot.do(UnitTypeId.HATCHERY, w.tag, self.bot.cartographer.expofree[0])
        #3rd
        if len(self.townhalls) < 3 and self.bot.supply_workers >= 25 and self.bot.can_afford(UnitTypeId.HATCHERY):
            print('3rd hatch condition TRUE')
            loc = self.bot.cartographer.expofree[0]
            w = self.bot.selectMineralDrone(self.townhalls.random)
            await self.bot.do(UnitTypeId.HATCHERY, w.tag, loc)
        #lair
        if self.poolstatus and self.bot.supply_workers > 36 and not self.lair:
            if self.bot.can_afford(LAIR) and self.townhalls.ready.noqueue:
                self.lair = self.townhalls.ready.noqueue.first.tag
                print('Macroz: Got lair,', self.lair)
                await self.bot.do(AbilityId.UPGRADETOLAIR_LAIR, self.lair)
        #roach warren
        if not (self.bot.units(ROACHWARREN) or self.bot.already_pending(ROACHWARREN)) and len(self.bot.workers) > 29:
            if self.bot.can_afford(ROACHWARREN):
                th = self.townhalls.ready.closest_to(self.bot.game_info.map_center)
                mins = self.bot.state.mineral_field.closer_than(10, th).center
                self.bot._client.debug_sphere_out(Point3((mins.x, mins.y, 12)), 1, Point3((100,255,200)))
                loc = th.position.towards(mins, -8)
                self.bot._client.debug_sphere_out(Point3((loc.x, loc.y, 12)), 1, Point3((100, 200, 255)))
                #TODO: Fix spam
                w =  self.bot.selectMineralDrone(self.townhalls.closest_to(self.bot.game_info.map_center))
                loc = await self.bot.find_placement(ROACHWARREN, loc)
                if loc:
                    print('found loc for RoachWarren')
                    await self.bot.do(UnitTypeId.ROACHWARREN, w.tag, loc)
        #Evos
        if len(self.bot.units(EVOLUTIONCHAMBER))+self.bot.already_pending(EVOLUTIONCHAMBER) < 2 and len(self.bot.workers) > 34 and self.bot.minerals > 75:
            th = self.townhalls.ready.closest_to(self.bot.game_info.map_center)
            mins = self.bot.state.mineral_field.closer_than(10, th).center
            loc = th.position.towards(mins, -8)
            # TODO: Fix spam
            w = self.bot.selectMineralDrone(self.townhalls.closest_to(self.bot.game_info.map_center))
            loc = await self.bot.find_placement(UnitTypeId.EVOLUTIONCHAMBER, loc)
            if loc:
                print('found loc for Evo')
                await self.bot.do(UnitTypeId.EVOLUTIONCHAMBER, w.tag, loc)
        #more gases till 3
        if self.bot.units(UnitTypeId.ROACHWARREN) and len(self.bot.units(UnitTypeId.EXTRACTOR))+self.bot.already_pending(EXTRACTOR) < 3 and len(self.bot.workers) > 31:
            await self.bot.takeGas()
        #more gases till 6
        if len(self.bot.workers) >= 51 and len(self.bot.units(UnitTypeId.EXTRACTOR)) + self.bot.already_pending(EXTRACTOR) < 6:
            await self.bot.takeGas()
        #infes pit
        if not self.bot.units(UnitTypeId.INFESTATIONPIT) and self.bot.supply_workers >= 50 :
            if self.bot.units(UnitTypeId.LAIR).ready and self.bot.can_afford(UnitTypeId.INFESTATIONPIT):
                th = self.townhalls.ready.random
                mins = self.bot.state.mineral_field.closer_than(10, th).center
                self.bot._client.debug_sphere_out(Point3((mins.x, mins.y, 12)), 1, Point3((100, 255, 200)))
                loc = th.position.towards(mins, -8)
                self.bot._client.debug_sphere_out(Point3((loc.x, loc.y, 12)), 1, Point3((100, 200, 255)))
                # TODO: Fix spam
                w = self.bot.selectMineralDrone(self.townhalls.closest_to(self.bot.game_info.map_center))
                loc = await self.bot.find_placement(ROACHWARREN, loc)
                if loc:
                    print('found loc for Inf Pit')
                    await self.bot.do(UnitTypeId.INFESTATIONPIT, w.tag, loc)
        #hive
        if not self.bot.units(UnitTypeId.HIVE) and self.bot.units(UnitTypeId.INFESTATIONPIT).ready and self.bot.units(UnitTypeId.LAIR).ready.idle:
            if self.bot.can_afford(UnitTypeId.HIVE):
                lair = self.bot.units(UnitTypeId.LAIR).ready.idle.first
                await self.bot.do(AbilityId.UPGRADETOHIVE_HIVE, lair.tag)

        if self.bot.workers.amount >= 60 and len(self.townhalls) + self.bot.already_pending(UnitTypeId.HATCHERY) < 4:
            if self.bot.can_afford(UnitTypeId.HATCHERY):
                loc = self.bot.cartographer.expofree[0]
                w = self.bot.selectMineralDrone(self.townhalls.closest_to(loc))
                await self.bot.do(UnitTypeId.HATCHERY, w.tag, loc) #TODO: generalise expand_now and implement
        if self.townhalls.ready.amount >= 4 and len(self.bot.units(EXTRACTOR)) + self.bot.already_pending(EXTRACTOR) < 8:
            await self.bot.takeGas() #TODO: auto expo (5th base onwards)




    async def macroLarva(self):
        if self.bot.units(UnitTypeId.SPAWNINGPOOL).ready:
            poolready = True
        else:
            poolready = False
        if self.bot.units(UnitTypeId.ROACHWARREN).ready:
            roachready = True
        else:
            roachready = False
        lingcount = self.bot.units(UnitTypeId.ZERGLING).amount + 2*self.bot.already_pending(UnitTypeId.ZERGLING)
        roachcount = self.bot.units(UnitTypeId.ROACH).amount + self.bot.already_pending(UnitTypeId.ROACH)
        dronecount = self.bot.supply_workers + self.bot.already_pending(UnitTypeId.DRONE)
        lingsneeded = 0 if (roachready or not poolready) or lingcount >= 16 else 16-lingcount
        roachesneeded = 0 if (not roachready) or roachcount >= 8 else 8-roachcount
        dronesneeded = min(self.max_workers, 16*self.townhalls.amount + 3*len(self.bot.units(UnitTypeId.EXTRACTOR))) - dronecount #TODO: use ai numbers
        pendingOVs = self.bot.already_pending(UnitTypeId.OVERLORD)
        lps = self.townhalls.amount/11 + len(self.bot.creep.injectors)*3/30
        if (self.bot.supply_used > 12 and self.bot.supply_cap == 14) or (self.bot.supply_used > 18 and self.bot.supply_cap == 22):
            OVsneeded = 1 - pendingOVs
        elif self.bot.supply_cap + pendingOVs*8 >= 200:
            OVsneeded = 0
        else:
            if self.bot.units(UnitTypeId.ROACHWARREN): #producing at 2 supply per larva
                OVsneeded = -(self.bot.supply_left - 36*lps)//8 - pendingOVs #weird negatives to avoid need for ceil()
            else:
                OVsneeded = -(self.bot.supply_left - 18*lps)//8 - pendingOVs
            OVsneeded = 0 if OVsneeded < 0 else int(OVsneeded) #need int
        self.bot._client.debug_text_screen('dronesneeded: {} - {} = {},\n OVsneeded: {}, \n lingsNeeded: {} \n Roachesneeded: {}'.format(
                min(self.max_workers, 16*self.townhalls.amount + 3*self.bot.units(UnitTypeId.EXTRACTOR).amount),
                dronecount,
                dronesneeded,
                OVsneeded,
                lingsneeded,
                roachesneeded), pos=(0.05,0.05))

        if roachcount >= 8 and self.bot.units(RAVAGER).amount * 8 < roachcount and self.bot.units(RAVAGER).amount < 8:
            if self.bot.can_afford(RAVAGER) and self.bot.units(UnitTypeId.ROACH):
                if self.bot.known_enemy_units:
                    r = self.bot.units(ROACH).furthest_to(
                        self.bot.known_enemy_units.random)
                else:
                    r = self.bot.units(UnitTypeId.ROACH).furthest_to(self.bot.enemy_start_locations[0])
                await self.bot.do(AbilityId.MORPHTORAVAGER_RAVAGER, r.tag)

        larvatags = [x.tag for x in self.bot.units(UnitTypeId.LARVA)]
        #priority 1: OVs
        if OVsneeded:
            count = min(OVsneeded, len(larvatags), int(self.bot.minerals/100))
            print('OVs needed:', OVsneeded, '; supply:', self.bot.supply_used, '/', self.bot.supply_cap, '; lps:', lps)
            if count:
                await self.bot.do(UnitTypeId.OVERLORD, larvatags[:count])
                self.bot.minerals -= 100*count
                del larvatags[:count]
            else:
                return
        if lingsneeded:
            count = min(int(lingsneeded/2), len(larvatags), int(self.bot.minerals/50), self.bot.supply_left)
            if count:
                await self.bot.do(UnitTypeId.ZERGLING, larvatags[:count])
                self.bot.minerals -= 50*count
                self.bot.supply_left -= count
                del larvatags[:count]
            else:
                return
        if roachesneeded:
            count = min(roachesneeded, len(larvatags), int(self.bot.minerals/75), int(self.bot.vespene/25), int(self.bot.supply_left/2))
            if count:
                await self.bot.do(UnitTypeId.ROACH, larvatags[:count])
                self.bot.minerals -= 75 * count
                self.bot.vespene -= 25*count
                self.bot.supply_left -= 2*count
                del larvatags[:count]
            else:
                return
        if dronesneeded > 0:
            count = min(dronesneeded, len(larvatags), int(self.bot.minerals/50), self.bot.supply_left)
            print('dronesneeded:', dronesneeded,' count:', count, 'larva:', len(larvatags),
                  'supleft', self.bot.supply_left, 'intmin',int(self.bot.minerals/50))
            if count > 0:
                print('Making Drones; dronesneeded: {} - {} = {}; count=min({}, {}, {})={}'.format(
                    min(self.max_workers, 16 * self.townhalls.amount + 3 * self.bot.units(UnitTypeId.EXTRACTOR).amount),
                    dronecount, dronesneeded, len(larvatags), int(self.bot.minerals/50), self.bot.supply_left, count
                ))
                await self.bot.do(UnitTypeId.DRONE, larvatags[:count])
                self.bot.minerals -= 50*count
                self.bot.supply_left -= count
                del larvatags[:count]
            else:
                return
        #roaches:
        if roachready:
            count = min(len(larvatags), int(self.bot.minerals/75), int(self.bot.vespene/25), int(self.bot.supply_left/2))
            if count:
                await self.bot.do(UnitTypeId.ROACH, larvatags[:count])
                self.bot.minerals -= 75 * count
                self.bot.vespene -= 25*count
                self.bot.supply_left -= 2*count
                del larvatags[:count]
            else:
                return
        else:
            count = min(len(larvatags), int(self.bot.minerals/50), self.bot.supply_left)
            if count:
                await self.bot.do(UnitTypeId.ZERGLING, larvatags[:count])
                self.bot.minerals -= 50*count
                self.bot.supply_left -= count
                del larvatags[:count]


    async def macroUpgrades(self):
        upglist = await self.bot.get_available_abilities(self.bot.units.structure)
        if self.bot.units(UnitTypeId.SPAWNINGPOOL).ready.noqueue.exists:
            p = self.bot.units(UnitTypeId.SPAWNINGPOOL).ready.noqueue.first
            if any(AbilityId.RESEARCH_ZERGLINGMETABOLICBOOST in x for x in upglist):
                print('MacroZ: lingspeed in upglist')
                if self.bot.can_afford(AbilityId.RESEARCH_ZERGLINGMETABOLICBOOST):
                    await self.bot.do(AbilityId.RESEARCH_ZERGLINGMETABOLICBOOST, p.tag)
            if AbilityId.RESEARCH_ZERGLINGADRENALGLANDS in upglist:
                await self.bot.do(AbilityId.RESEARCH_ZERGLINGADRENALGLANDS, p.tag)
        if self.bot.units(UnitTypeId.ROACHWARREN).ready.noqueue.exists:
            w = self.bot.units(UnitTypeId.ROACHWARREN).ready.noqueue.first
            if any(AbilityId.RESEARCH_GLIALREGENERATION in x for x in upglist):
                if self.bot.can_afford(AbilityId.RESEARCH_GLIALREGENERATION):
                    await self.bot.do(AbilityId.RESEARCH_GLIALREGENERATION, w.tag)
        if self.bot.units(UnitTypeId.EVOLUTIONCHAMBER).ready.noqueue:
            if any(RESEARCH_ZERGMELEEWEAPONS in x for x in upglist):
                print('RESEARCH_ZERGMELEEWEAPONS in upglist')
            evos = self.bot.units(UnitTypeId.EVOLUTIONCHAMBER).ready.noqueue
            for e in evos:
                for u in self.upg:
                    if any(u in x for x in upglist):
                        if self.bot.can_afford(u) and e.is_idle:
                            await self.bot.do(u, e.tag)
                            break

    async def macroQueenProduction(self):
        if len(self.bot.units(QUEEN)) + self.bot.already_pending(QUEEN) < 5 and not (len(self.bot.workers) > 28 and not self.bot.units.of_type({LAIR, HIVE})):
            for h  in self.townhalls.idle:
                if self.bot.can_afford(QUEEN):
                    await self.bot.do(UnitTypeId.QUEEN, h.tag)
                    break

    async def defend(self):
        safe = True
        for th in self.townhalls:
            if self.bot.known_enemy_units.closer_than(20, th):
                safe = False
                self.bot._client.debug_sphere_out(th.position3d, 4, Point3((255, 0, 0)))
        if safe:
            if self.defendertags:
                await self.bot.do(AbilityId.MOVE, self.defendertags, self.townhalls.closest_to(self.bot.game_info.map_center).position.towards(self.bot.game_info.map_center, 6))
                self.defendertags = []
            return
        self.defendertags = []
        for th in self.townhalls:
            defense = self.bot.units.of_type({UnitTypeId.ROACH, UnitTypeId.RAVAGER, UnitTypeId.ZERGLING, UnitTypeId.QUEEN}).closer_than(15, th)
            viablequeens = []
            if defense:
                defense = defense.tags_not_in(self.defendertags+list(self.bot.creep.injectors.keys()))
            if not defense:
                #if self.bot.known_enemy_units.closer_than(20, th):
                    #defense = self.bot.workers.closer_than(15, th).tags_not_in(self.defendertags)
                viablequeens = [u.tag for u in self.bot.units(UnitTypeId.QUEEN) if u.tag in self.bot.creep.injectors.keys() and u.tag not in self.defendertags]
            self.defendertags += viablequeens + [u.tag for u in defense]
        for u in self.bot.units.tags_in(self.defendertags):
            if u.is_idle:
                tgt = self.bot.known_enemy_units.closest_to(u)
                if tgt:
                    self.bot._client.debug_line_out(u.position3d, tgt.position3d, Point3((25, 255, 255)))
                    await self.bot.do(AbilityId.ATTACK, u.tag, tgt)




        
    async def buildingConstructionComplete(self, unit):
        if unit.type_id == UnitTypeId.SPAWNINGPOOL:
            self.poolstatus = 2
        elif unit.type_id == EXTRACTOR: #TODO: Fix
            ths = self.townhalls.filter(lambda x: x.assigned_harvesters != x.ideal_harvesters)
            if ths:
                th = ths.closest_to(unit)
            else:
                th = self.townhalls.closest_to(unit)
            ww = [u.tag for u in self.bot.workers.idle.closer_than(8, unit)]
            if len(ww) > 3:
                ww = ww[:3]
                print('too many drones, picking first 3')
            elif len(ww) < 3:
                while len(ww) < 3:
                    w = self.bot.selectMineralDrone(th, notin=ww)
                    if not w:
                        print('NOT w')
                    if len(ww) < 3: #should be redundant
                        ww.append(w.tag)
                    print('appending to ww:', w)
            if len(ww) == 3:
                await self.bot.do(AbilityId.SMART, ww, unit)
        #TODO: Extractors, tech buildigs maybe, new hatches
    async def unitCreated(self, unit):
        if unit.type_id == UnitTypeId.SPAWNINGPOOL:
            self.pooltag = unit.tag
            if unit.is_ready:
                self.poolstatus = 2
            else:
                self.poolstatus = 1

    async def unitDestroyed(self, tag):
        if tag == self.pooltag:
            self.pooltag = 0
            self.poolstatus = 0
        elif tag == self.lair:
            self.lair = 0
