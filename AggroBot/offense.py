from sc2.unit import Unit
from sc2.units import Units
from sc2.constants import *
from sc2.position import Point2, Point3
from typing import Union, List





class attacker:
    def __init__(self, army = Union[int, "Unit", "Units", List["Unit"], List[int]], name='Untitled'):
        if isinstance(army, int):
            self.Single = True
            self.army = [army]
        elif isinstance(army, Unit):
            self.Single = True
            self.army = [army.tag]
        elif isinstance(army, Units):
            self.Single = False
            self.army = [x.tag for x in army]
        elif isinstance(army, List):
            self.Single = False
            if len(army) > 0:
                if isinstance(army[0], int):
                    self.army = army
                elif isinstance(army[0], Unit):
                    self.army = [u.tag for u in army]
        self.headcount = len(self.army)
        self.goal = None #Point2, will try to get there
        self.retreat = None #also Point2
        self.phase = 0 #0: idle, 1: in transit, 2: attacking, 3: retreating
        self.should_kite = True
        self.should_ignore = None
        self.name = name

    def atk(self, goal, retreat, should_kite=True, should_ignore={UnitTypeId.LARVA}):
        self.goal = goal
        self.retreat = retreat
        self.should_kite = should_kite
        self.should_ignore = should_ignore
        print('Attacker: issued atk to', self.name, ', ignorelist:', should_ignore)




class OffenseManager:
    def __init__(self, bot):
        self.bot = bot
        self.platoons = [] #list of attackers


    async def step(self, itera):
        for platoon in self.platoons:
            if platoon.goal != None:
                us = self.bot.units.tags_in(platoon.army)
                if not us:
                    print('AtkMgmt:', platoon.name, ' ERROR NO US')
                    continue
                c = us.center
                self.bot._client.debug_sphere_out(c, 7)
                if platoon.phase == 0:
                    await self.bot.do(AbilityId.MOVE, platoon.army, platoon.goal)
                    platoon.phase = 1
                    print('AtkMgmt: platoon', platoon.name, ' phase now 1, from 0')
                elif platoon.phase == 1:
                    if us.idle:
                        await self.bot.do(AbilityId.MOVE, platoon.army, platoon.goal)
                    if c._distance_squared(platoon.goal) <= 1600 or self.bot.known_enemy_units.not_structure.exclude_type(platoon.should_ignore).closer_than(30, c):
                        await self.bot.do(AbilityId.ATTACK, platoon.army, platoon.goal)
                        platoon.phase = 2
                        print('AtkMgmt: platoon', platoon.name, ' phase now 2, from 1')
                        continue
                elif platoon.phase == 2:
                    clear = 0
                    for u in us: #individual unit micro
                        #This is where the fun begins
                        if self.bot.known_enemy_units.exclude_type(platoon.should_ignore).closer_than(14, u):
                            target = self.bot.known_enemy_units.exclude_type(platoon.should_ignore).closest_to(u)
                            if platoon.should_kite == True:
                                if u.is_flying:
                                    tgtrnge = target.air_range
                                    rnge = u.air_range
                                else:
                                    tgtrnge = target.ground_range
                                    rnge = u.ground_range
                                if u.weapon_cooldown == 0:
                                    if not u.is_attacking:
                                        await self.bot.do(AbilityId.ATTACK, u.tag, target)
                                        continue
                                elif u.weapon_cooldown > 0:
                                    if rnge < tgtrnge:
                                        if not u.is_moving:
                                            print('u to move closer')
                                            await self.bot.do(AbilityId.MOVE, u.tag, target.position.towards(u, rnge/2))
                                            self.bot._client.debug_line_out(u, target, Point3((0,255,255)))
                                            continue
                                    else:
                                        d = u.distance_to(target)
                                        self.bot._client.debug_line_out(u, target, Point3((255,100,100)))
                                        p = (u.position3d + target.position3d)/2
                                        p = Point3((p.x, p.y, (u.position3d.z + target.position3d.z)/2))
                                        self.bot._client.debug_text_world('dist: {:.2f}'.format(d), p)
                                        if d <= min(tgtrnge+2, rnge):
                                            print('u too close')
                                            if not u.is_moving:
                                                await self.bot.do(AbilityId.MOVE, u.tag, u.position.towards(target, -10))
                                                continue
                                        elif d > rnge+3:
                                            print('u too far')
                                            if not u.is_attacking:
                                                await self.bot.do(AbilityId.ATTACK, u.tag, target)
                                                continue
                            else:
                                if not u.is_attacking:
                                    await self.bot.do(AbilityId.ATTACK, u.tag, target)
                                else:
                                    if u.order_target != target.tag:
                                        await self.bot.do(AbilityId.ATTACK, u.tag, target)
                        else: #no enemies in range of some rando
                            clear += 1
                            if u.distance_to(c) > 7: #try to regroup
                                if not u.is_moving:
                                    await self.bot.do(AbilityId.MOVE, u.tag, c, should_combine=True)
                            else:
                                if not u.is_attacking:
                                    await self.bot.do(AbilityId.ATTACK, u.tag, platoon.goal, should_combine=True)
                    #code for advance, retreat and complete
                    if clear == platoon.headcount:
                        d = c._distance_squared(platoon.goal)
                        if d > 1600 and us.idle:
                            await self.bot.do(AbilityId.MOVE, platoon.army, platoon.goal)
                            platoon.phase = 1
                            print('AtkMgmt: platoon', platoon.name, '.phase now 1, from 2')
                            continue
                        if 49 < d <= 1600 and any([not x.is_attacking for x in us]):
                            await self.bot.do(AbilityId.ATTACK, platoon.army, platoon.goal)
                        if d <= 25:
                            platoon.goal = None
                            print("AtkMgmt:", platoon.name, " platoon now done")
                            platoon.phase = 0
                            continue
                    #retreat: #TODO: will cry to lings
                    if platoon.retreat:#sometimes we wanna die
                        if len(self.bot.known_enemy_units.not_structure.exclude_type(platoon.should_ignore)) > platoon.headcount*1.25:
                            await self.bot.do(AbilityId.MOVE, platoon.army, platoon.retreat)
                            platoon.phase = 3
                            print('AtkMgmt: platoon', platoon.name, ' now retreating from attacking')
                elif platoon.phase == 3:
                    for u in us:
                        if platoon.should_kite:
                            rnge = u.air_range if u.is_flying else u.ground_range
                            if u.weapon_cooldown != 0:
                                if not u.is_moving:
                                    await self.bot.do(AbilityId.MOVE, u.tag, platoon.goal, should_combine=True)
                                    continue
                            else:
                                targets = self.bot.known_enemy_units.exclude_type(platoon.should_ignore).closer_than(rnge, u)
                                if targets:
                                    await self.bot.do(AbilityId.ATTACK, u.tag, targets.closest_to(u), should_combine=True)
                    if c._distance_squared(platoon.retreat) < 49:
                        platoon.goal = None
                        platoon.phase = 0
                        continue



    async def buildingConstructionComplete(self, unit):
        pass
    async def unitCreated(self, unit):
        pass
    async def unitDestroyed(self, tag):
        for p in self.platoons:
            if tag in p.army:
                p.army.remove(tag)
            if not p.army:
                print('AtkMgmt: platoon', p.name, 'is dead')
                self.platoons.remove(p)
                del p