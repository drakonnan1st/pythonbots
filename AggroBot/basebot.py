import sc2, asyncio
from sc2.data import ActionResult, race_worker
from sc2 import run_game, maps, Race, Difficulty
from sc2.constants import *
from sc2.game_data import AbilityData
from sc2.position import Point2, Point3
from sc2.unit import Unit
from sc2.units import Units
from s2clientprotocol import sc2api_pb2, raw_pb2, common_pb2
from typing import Union, List
from itertools import groupby

#BALANCE ASSUMPTIONS:
#-Assume SCVs, Probes & Drones cost 50min only

class BaseBot(sc2.BotAI):
    actlist = []
    listActionRaw = [] #list of raw_pb2.ActionRaw
    listMerger = [] #[[tags, abvalue, target, queue], ...]
    townhallIDs = {UnitTypeId.HATCHERY, UnitTypeId.LAIR, UnitTypeId.HIVE, UnitTypeId.COMMANDCENTER,
                 UnitTypeId.ORBITALCOMMAND, UnitTypeId.PLANETARYFORTRESS, UnitTypeId.NEXUS}
    def __init__(self):
        self.max_workers = 70
        super().__init__()

    async def do(self, ability, tags, target=None, queue=False, should_combine=False):
        print('BaseBot:do:', ability, ', target:', target)
        if should_combine:
            actl = self.listMerger
        else:
            actl = self.listActionRaw
        await self.doActionRaw(actl, ability, tags, target, queue, should_combine)
    async def dostuff(self):
        #await self.do_actions(self.actlist)
        #self.actlist = []
        await self.doRequestActions(self.listActionRaw, self.listMerger)
        self.listActionRaw = []
        self.listMerger = []

    async def doActionRaw(self, alist, ability, tags, target=None, queue=False, should_combine=False):
        if isinstance(tags, int):
            tags = [tags]
        if isinstance(ability, UnitTypeId):
            abvalue = self._game_data.units[ability.value].creation_ability.id.value
        elif isinstance(ability, AbilityId):
            abvalue = ability.value
        else:
            print('doActionRaw: not an AbilityId', ability)
            return None
        tt = target
        if not target:
            command = raw_pb2.ActionRawUnitCommand(ability_id=abvalue, unit_tags = tags, queue_command=queue)
        elif isinstance(target, Unit):
            tt = target.tag
            command = raw_pb2.ActionRawUnitCommand(
                ability_id=abvalue, unit_tags = tags, queue_command=queue, target_unit_tag=tt)
        elif isinstance(target, Point2):
            command = raw_pb2.ActionRawUnitCommand(
                ability_id=abvalue, unit_tags=tags, queue_command=queue,
                target_world_space_pos=common_pb2.Point2D(x=target.x, y=target.y))
        else:
            print('doActionRaw: Error, target neither Unit, Point2 nor None;', target)
            return None
        actraw = raw_pb2.ActionRaw(unit_command=command)
        if should_combine:
            alist.append((tags, abvalue, tt, queue))
        else:
            alist.append(actraw)

    async def doRequestActions(self, fulllist, mergelist=[]):
        acts = []
        if mergelist:
            length = len(mergelist)
            mergelist.sort(key= lambda x: x[1:])
            for key, group in groupby(mergelist, lambda x: x[1:]):
                abvalue, target, queue = key
                tags = [tag for command in group for tag in command[0]]
                if isinstance(target, int):  #Note: target is unit's tag
                    cmd = raw_pb2.ActionRawUnitCommand(
                        ability_id=abvalue, unit_tags=tags, queue_command=queue, target_unit_tag=target)
                elif isinstance(target, Point2):
                    cmd = raw_pb2.ActionRawUnitCommand(
                        ability_id=abvalue, unit_tags=tags, queue_command=queue,
                        target_world_space_pos=common_pb2.Point2D(x=target.x, y=target.y))
                elif not target:
                    cmd = raw_pb2.ActionRawUnitCommand(ability_id=abvalue, unit_tags=tags, queue_command=queue)
                else:
                    print('BaseBot:doRequestActions: Error in mergelist, target neither Unit tag, Point2 nor None', target)
                    continue
                acts.append(raw_pb2.ActionRaw(unit_command=cmd))
            print('BaseBot:doRequestActions: Merging; pre:', length, ', post:', len(acts))
        acts += fulllist
        res = await self._client._execute(action=sc2api_pb2.RequestAction(actions=[sc2api_pb2.Action(action_raw=a) for a in acts]))
        res = [ActionResult(r) for r in res.action.result]
        return res

    def _prepare_step(self, state):
        super()._prepare_step(state)
        self.supply_workers = state.common.food_workers
        self.supply_army = state.common.food_army
        self.larva_count = state.common.larva_count

    async def concede(self):
        await self.chat_send('(gg)')
        await asyncio.sleep(1)
        await self._client.leave()
    
    async def drone(self):
        if self.supply_workers < self.max_workers: #TODO: implement max workers into count's
            if self.race == Race.Zerg:
                count = min(int(self.minerals/50), self.supply_left, self.units(UnitTypeId.LARVA).amount)
                if count:
                    tags = [x.tag for x in self.units(UnitTypeId.LARVA)[:count]]
                    await self.do(UnitTypeId.DRONE, tags)
            elif self.race == Race.Protoss:
                count = min(int(self.minerals/50), self.supply_left, self.townhalls.idle.amount)
                if count:
                    tags = [x.tag for x in self.townhalls.prefer_idle[:count]]
                    await self.do(UnitTypeId.PROBE, tags)
            elif self.race == Race.Terran:
                if self.units(UnitTypeId.BARRACKS).ready:
                    tags = [x.tag for x in self.townhalls.of_type({UnitTypeId.ORBITALCOMMAND, UnitTypeId.PLANETARYFORTRESS}).noqueue]
                else:
                    tags = [x.tag for x in self.townhalls]
                count = min(int(self.minerals)/50, self.supply_left, len(tags))
                if count:
                    await self.do(UnitTypeId.SCV, tags)
    def getGameTime(self):
        return self.state.game_loop*0.725/16

    async def workersplit(self, th):
        wPool = self.workers.closer_than(10, th)
        for w in wPool:
            self.actlist.append(w.gather(self.state.mineral_field.closest_to(w)))

    async def chrono(self, user, t):
        if not t.has_buff(BuffId.CHRONOBOOSTENERGYCOST):
            a = await self.get_available_abilities(user)
            if AbilityId.EFFECT_CHRONOBOOSTENERGYCOST in a:
                await self.do(AbilityId.EFFECT_CHRONOBOOSTENERGYCOST, user.tag, t)

    def selectMineralDrone(self, th, notin: Union["Unit", "Units", int, list]=[]): #TODO: Fix, may require experimentation
        rm = []
        if isinstance(notin, Unit):
            rm = [notin.tag]
        elif isinstance(notin, int):
            rm = [notin]
        elif isinstance(notin, Units):
            rm = [x.tag for x in notin]
        elif isinstance(notin, list):
            rm = notin
        mineralTags = [x.tag for x in self.state.units.mineral_field]
        ws = self.units.of_type([SCV, DRONE, PROBE]).closer_than(10, th).filter(lambda w: w.is_gathering and (not w.is_carrying_minerals) and (w.tag not in rm) and w.orders[0].target in mineralTags)
        if not ws:
            ws = self.units.of_type([SCV, DRONE, PROBE]).closer_than(10, th).filter(lambda w: (w.is_gathering) and (w.tag not in rm) and w.orders[0].target in [mineralTags])
            if len(ws) == 0:
                print('selectMineralDrone: had to pick closest worker')
                return self.units.of_type([SCV, DRONE, PROBE]).filter(lambda w: w not in rm).closest_to(self.state.mineral_field.closer_than(10,th).center)
            else:
                ww = ws[0]
        else:
            ww = ws[0]
        return ww

    # async def doAbility(self, unit, ability, queue=False, skipcheck=False): #idk why this is needed, do() is superior
    #     if skipcheck == True:
    #         if self.can_afford(ability):
    #             await self.do(unit(ability, queue=queue))
    #     else:
    #         a = await self.get_available_abilities(unit)
    #         if ability in a and self.can_afford(ability):
    #             await self.do(unit(ability, queue=queue))

    async def inject(self, injects):
        for t in injects:
            q = self.units.find_by_tag(t)
            th = self.townhalls.ready
            if th and q:
                h = th.closest_to(q)
            else:
                continue
            if q.energy >= 25 and q.is_idle and not h.has_buff(BuffId.QUEENSPAWNLARVATIMER):
                await self.do(AbilityId.EFFECT_INJECTLARVA, t, h)

    async def takeGas(self): #TODO: Optimise
        if self.can_afford(EXTRACTOR):
            gsrs = []
            for t in self.townhalls.ready:
                gsrs += self.state.vespene_geyser.closer_than(10, t)
            print('basebot:takegas:', self.townhalls.amount, 'bases;', len(gsrs), 'geysers')
            for g in gsrs:

                if g.type_id not in {UnitTypeId.EXTRACTOR, UnitTypeId.REFINERY, UnitTypeId.ASSIMILATOR}:
                    w = self.selectMineralDrone(self.townhalls.closest_to(g))
                    print('tryna make extractors: nearbygeysers:', gsrs, 'g:', g)
                    await self.do(UnitTypeId.EXTRACTOR, w.tag, g)

    async def can_place_list(self, building: Union[AbilityData, AbilityId, UnitTypeId], positions: List["Point2"]) -> List["Point2"]:
        """AbilityData, AbilityId, UnitTypeId only"""
        if isinstance(building, UnitTypeId):
            building = self._game_data.units[building.value].creation_ability
        elif isinstance(building, AbilityId):
            building = self._game_data.abilities[building.value]
        mask = await self._client.query_building_placement(building, positions)
        r = [l for i, l in enumerate(positions) if mask[i] == ActionResult.Success]
        return r
