import sc2, random
from sc2.data import ActionResult
from sc2.constants import *
from sc2.position import Point2, Point3



class AggroZA:
    def __init__(self, bot):
        self.phase = 0
        self.bot = bot
        self.hatch = None
        self.army = []
        self.max_workers = 66 #idk why
        self.allin = True
        

    async def step(self, iter):
        if self.phase == 0: #get 16 drones
            if self.hatch == None:
                self.hatch = self.bot.units(HATCHERY).first
            if self.bot.units(UnitTypeId.OVERLORD).noqueue and self.bot.cartographer.ready:
                self.enemmain = self.bot.enemy_start_locations[0]
                enemnat = self.enemmain.closest(self.bot.cartographer.expofree)
                pillarlocs = enemnat.sort_by_distance(self.bot.cartographer.OVspots)
                if len(pillarlocs) > 1:
                    if self.enemmain._distance_squared(pillarlocs[0]) > self.enemmain._distance_squared(pillarlocs[1]):
                        loc = pillarlocs[0]
                    else:
                        loc = pillarlocs[1]
                    await self.bot.do(AbilityId.SMART, self.bot.units(UnitTypeId.OVERLORD).noqueue.first.tag, loc)
            if self.bot.supply_used < 14:
                await self.bot.drone() #TODO make drone in basebot of all races
            else:
                if len(self.bot.units(EXTRACTOR)) < 2 and self.bot.supply_used <= 14:
                    if self.bot.minerals >= 75:
                        gs = self.bot.state.vespene_geyser.closer_than(10, self.hatch)
                        for g in gs:
                            if not self.bot.units(EXTRACTOR).closer_than(2,g).exists:
                                w = self.bot.units(DRONE).filter(lambda w: w.is_carrying_minerals == False).closest_to(g)
                                await self.bot.do(EXTRACTOR, w.tag, g)
                                print('AggroZA::step::phase0: Built extractor', g)
                if len(self.bot.units(EXTRACTOR)) == 2 and self.bot.supply_used == 14:
                    await self.bot.do(CANCEL, [x.tag for x in self.bot.units(EXTRACTOR)])
                if len(self.bot.workers) > 14:
                    tags = [w.tag for w in self.bot.workers]
                    await self.bot.do(SMART, tags, self.hatch)
                    await self.bot.do(SMART, tags, self.bot.state.mineral_field.closer_than(10,self.enemmain).furthest_to(self.enemmain), queue=True)
                    self.phase = 1
        if self.phase == 1: #in transit
            if len(self.bot.workers.closer_than(6, self.bot.state.mineral_field.closer_than(10,self.enemmain).furthest_to(self.enemmain))) > 12:
                for w in self.bot.workers.closer_than(20, self.enemmain):  # TODO: needs mergelist
                    targets = self.bot.known_enemy_units.not_structure
                    if targets:
                        #await self.bot.do(w(ATTACK, targets.closest_to(w)))
                        await self.bot.do(ATTACK, w.tag, targets.closest_to(w), should_combine=True)
                    else:
                        #await self.bot.do(w(ATTACK, ))
                        await self.bot.do(ATTACK, w.tag, self.enemmain.towards(self.bot.state.mineral_field.closer_than(10, self.enemmain).furthest_to(self.enemmain).position, 3), should_combine=True)
                        print('warning, no drone targets')
                    self.army.append(w.tag)
                self.phase = 2
                self.groupA = []
                self.groupB = []
                print('phase now 2')
        elif self.phase == 2:
            await self.bot.drone()
            for t in self.army+self.groupA+self.groupB: #TODO: needs mergelist
                if not self.bot.units.find_by_tag(t):
                    if t in self.army:
                        self.army.remove(t)
                    if t in self.groupA:
                        self.groupA.remove(t)
                    if t in self.groupB:
                        self.groupB.remove(t)
                    continue
                w = self.bot.units.by_tag(t)
                if w.is_attacking and w.orders[0].target in [x.tag for x in self.bot.known_enemy_structures]:
                    targets = self.bot.known_enemy_units.not_structure.exclude_type({LARVA})
                    if targets:
                        #await self.bot.do(w(ATTACK, targets.closest_to(w)))
                        await self.bot.do(ATTACK, t, targets.closest_to(w), should_combine=True)
                if w.is_attacking and w.health < 20 and t in self.army:
                    #await self.bot.do(w(SMART, self.bot.state.mineral_field.closest_to(self.hatch)))
                    await self.bot.do(SMART, t, self.bot.state.mineral_field.closest_to(self.hatch), should_combine=True)
                    self.army.remove(t)
                    self.groupA.append(t)
                    print('pulling back', w)
                if t in self.groupA and self.bot.known_enemy_units.not_structure:
                    if w.distance_to(self.bot.known_enemy_units.not_structure.closest_to(w)) < 1:
                        targets = self.bot.known_enemy_units.not_structure.exclude_type({LARVA})
                        if not targets:
                            targets = self.bot.known_enemy_units.exclude_type({LARVA})
                            if not targets:
                                #await self.bot.do(w(SMART, self.bot.state.mineral_field.closest_to(self.hatch)))
                                await self.bot.do(SMART, t, self.bot.state.mineral_field.closest_to(self.hatch), should_combine=True)
                                self.groupA.remove(t)
                                continue
                        #await self.bot.do(w.attack(targets.closest_to(w)))
                        await self.bot.do(ATTACK, t, targets.closest_to(w), should_combine=True)
                        print('redeployed', w)
                        self.groupA.remove(t)
                        self.groupB.append(t)
                elif t in self.groupA and not self.bot.known_enemy_units.not_structure:
                    #await self.bot.do(w(SMART, self.bot.state.mineral_field.closest_to(self.hatch)))
                    await self.bot.do(SMART, t, self.bot.state.mineral_field.closest_to(self.hatch), should_combine=True)
                    self.groupA.remove(t)
                if w.health < 5 and t in self.groupB:
                    #await self.bot.do(w(SMART, self.bot.state.mineral_field.closest_to(self.hatch)))
                    await self.bot.do(SMART, t, self.bot.state.mineral_field.closest_to(self.hatch), should_combine=True)
                    self.groupB.remove(t)
                    print('bringing back', w)
                if w.is_idle:
                    print('idle', w)
                    targets = self.bot.known_enemy_units.exclude_type({LARVA})
                    if targets:
                       # await self.bot.do(w(ATTACK, targets.closest_to(w)))
                        await self.bot.do(ATTACK, t, targets.closest_to(w), should_combine=True)
                        continue
                    else:
                        print('lol cant find targets')
                        #await self.bot.do(w(SMART, self.bot.state.mineral_field.closest_to(self.hatch)))
                        await self.bot.do(SMART, t, self.bot.state.mineral_field.closest_to(self.hatch), should_combine=True)
                        if t in self.army:
                            self.army.remove(t)
                        if t in self.groupA:
                            self.groupA.remove(t)
                        if t in self.groupB:
                            self.groupB.remove(t)
                        break
            if len(self.bot.workers.closer_than(20, self.enemmain)) == 0:
                print('lol attack failed')
                #self.phase = 4
                self.allin = False


    
    #needed for other bots
    async def buildingConstructionComplete(self, unit):
        pass
    async def unitCreated(self, unit):
        pass
    async def unitDestroyed(self, tag):
        pass
