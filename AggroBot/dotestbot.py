import sc2
from sc2.data import ActionResult
from sc2 import run_game, maps, Race, Difficulty
from sc2.player import Bot, Computer, Human
from sc2.constants import *
from sc2.position import Point2, Point3

from basebot import BaseBot

class DoTestBot(BaseBot):
    def __init__(self):
        super().__init__()
        self.ready = False

    async def on_step(self, iteration: int):
        if self.ready == False:
            print('started self.ready')
            #l = self.units(UnitTypeId.LARVA).first.tag
            #await self.do(UnitTypeId.DRONE, l)
            await self.drone()
            tt = [x.tag for x in self.workers]
            await self.do(AbilityId.ATTACK, tt, self.enemy_start_locations[0])
            self.ready = True


        await self.dostuff()

run_game(maps.get("BlueshiftLE"), [
    Bot(Race.Zerg, DoTestBot()),
    Computer(Race.Zerg, Difficulty.Easy)],
    #Human(Race.Zerg),
    #Bot(Race.Zerg, AggroBot())],
    realtime=False)
