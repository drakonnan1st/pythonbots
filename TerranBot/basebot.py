import sc2, random
from sc2.data import ActionResult
from sc2 import run_game, maps, Race, Difficulty
from sc2.player import Bot, Computer
from sc2.constants import *
from sc2.position import Point2, Point3
from sc2.unit import Unit
from sc2.units import Units
from typing import Union #neede for selectMineralWorker, and any bigboi functions

Class Basebot(sc2.BotAI):
    def __init__(self):
        self.max_workers = 66 #I dont think Terran needs more than 66
        self.micro = []
        self.macro = []
        self.DBB = [] #list of dedicated depot builders
        self.autodepots = False
        self.autoupgrades = False

#Has selectMineralDrone, dropmules, setCCrally,
    #setProdrally, workersplit, doAbility, depotMicro, getGameTime


    def selectMineralDrone(self, th, notin: Union["Unit", "Units", int, list]=[]):
        rm = []
        if isinstance(notin, Unit):
            rm = [notin.tag]
        elif isinstance(notin, int):
            rm = [notin]
        elif isinstance(notin, Units):
            rm = [x.tag for x in notin]
        elif isinstance(notin, list):
            rm = notin
        mineralTags = [x.tag for x in self.state.units.mineral_field]
        w = self.units(SCV).closer_than(10, th).filter(lambda w: w.is_gathering and (not w.is_carrying_minerals) and (w.tag not in rm) and w.orders[0].target in mineralTags)
        if len(w) == 0:
            w = self.units(SCV).closer_than(10, th).filter(lambda w: (w.is_gathering) and (w.tag not in rm) and w.orders[0].target in [mineralTags])
            if len(w) == 0:
                ww = self.units(SCV).filter(lambda w: w not in rm).closest_to(th)
                print('selectMineralDrone: had to pick closest worker')
        else:
            ww = w[0]
        return ww

    async def dropmules(self):
        if self.units(ORBITALCOMMAND).exists:
            for x in self.units(ORBITALCOMMAND).ready.filter(lambda x: x.energy >= 50):
                if self.scansneeded == 0:
                    a = await self.get_available_abilities(x)
                    if AbilityId.CALLDOWNMULE_CALLDOWNMULE in a:
                        m = []
                        for y in self.townhalls.ready:
                            m += self.state.mineral_field.closer_than(10,y)
                        if len(m) > 0:
                            m2 = max(m, key=lambda x: x.mineral_contents)
                            self.micro.append(x(CALLDOWNMULE_CALLDOWNMULE, m2))

    async def setCCrally(self, target, exclude=[]):
        for x in self.townhalls:
            if x not in exclude:
                self.macro.append(x(RALLY_WORKERS, target))

    async def setProdrally(self, target, exclude=[]):
        for x in self.units.of_type([BARRACKS, STARPORT, FACTORY]):
            if x not in exclude:
                self.macro.append(x(RALLY_UNITS, target))

    async def workersplit(self, th):
        wPool = self.workers.closer_than(10, th)
        for w in wPool:
            self.micro.append(w.gather(self.state.mineral_field.closest_to(w)))


    async def doAbility(self, unit, ability, queue=False, mode='micro'):
        a = await self.get_available_abilities(unit)
        if ability in a and self.can_afford(ability):
            if mode == 'micro':
                self.micro.append(unit(ability, queue=queue))
            if mode == 'macro':
                self.macro.append(unit(ability, queue=queue))
            if mode == 'do':
                await self.do(unit(ability, queue=queue))

    async def depotMicro(self):
        for x in self.units.of_type([SUPPLYDEPOT, SUPPLYDEPOTDROP]).ready:
            if not self.known_enemy_units.closer_than(4, x).exists:
                self.micro.append(x(MORPH_SUPPLYDEPOT_LOWER))
        for x in self.units.of_type(SUPPLYDEPOTLOWERED).ready:
            if self.known_enemy_units.closer_than(3,x).exists:
                self.micro.append(x(MORPH_SUPPLYDEPOT_RAISE))

    def getGameTime(self):
        return self.state.game_loop*0.725/16

    
