import sc2, random
from sc2.data import ActionResult
from sc2 import run_game, maps, Race, Difficulty
from sc2.player import Bot, Computer
from sc2.constants import *
from sc2.position import Point2, Point3
from sc2.unit import Unit
from sc2.units import Units
from typing import Union #neede for selectMineralWorker, and any bigboi functions

from basebot import Basebot

Class SKT1INnoVation(Basebot):
    #class feels unecessary, just a middleman
    def on_start(self):
        print('SKT1INno: on_start')
        if self.enemy_race == ZERG
