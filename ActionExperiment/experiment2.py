import sc2, random
from sc2.data import ActionResult
from sc2 import run_game, maps, Race, Difficulty
from sc2.player import Bot, Computer, Human
from sc2.constants import *
from sc2.position import Point2, Point3

from basebot import BaseBot
#from offense import OffenseManager, attacker
from microtest02 import MicroTest02

#results (Realtime):
#Time::init: 0.001998424530029297
#Time::testA 15.925155878067017
#Time::testB 0.010993003845214844
#Time::testC 0.00600433349609375


class Experiment(BaseBot):
    def __init__(self):
        super().__init__()
        self.ais = []


    def on_start(self):
        self.ai = MicroTest02(self)
        self.ais.append(self.ai)
        #self.warmonger = OffenseManager(self)
        #self.ais.append(self.warmonger)


    async def on_step(self, iteration):
        if iteration == 0:
            await self.chat_send('bot started')
        for ai in self.ais:
            await ai.step(iteration)


        for e in self.known_enemy_units:
            self._client.debug_text_world(str(e.ground_range), e.position3d)
        await self._client.send_debug()
        await self.dostuff()

    async def on_building_construction_complete(self, unit):
        for ai in self.ais:
            await ai.buildingConstructionComplete(unit)
    async def on_unit_created(self, unit):
        for ai in self.ais:
            await ai.unitCreated(unit)
    async def on_unit_destroyed(self, unit_tag):
        for ai in self.ais:
            await ai.unitDestroyed(unit_tag)



def main():
    sc2.run_game(maps.get("Testing"), [
    Bot(Race.Protoss, Experiment()),],
    realtime=True)

if __name__ == '__main__':
    main()
