import sc2, random
from sc2.data import ActionResult
from sc2 import run_game, maps, Race, Difficulty
from sc2.player import Bot, Computer, Human
from sc2.constants import *
from sc2.position import Point2, Point3
import time

from basebot import BaseBot


class MicroTest02:
    def __init__(self, bot):
        self.bot = bot
        self.stalkers = None
        self.waypoint = Point2((80,76))
        self.waypoint2 = Point2((80, 25))
        self.retreat = Point2((40, 76))
        self.doingtestA = False
        self.doingtestB = False
        self.doingtestC = False
        self.initinit = False
        self.marines = None
        self.marinetags = []
        self.testAtag = None
        self.testBtag = None
        self.testCtag = None

    async def step(self, itera):
        if self.initinit == False:
            start = time.time()
            self.marines = self.bot.units(UnitTypeId.MARINE)
            self.marinetags = [x.tag for x in self.marines]
            self.testAtag = self.bot.units(UnitTypeId.DRONE).first.tag
            self.testBtag = self.bot.units(UnitTypeId.ZERGLING).first.tag
            self.testCtag = self.bot.units(UnitTypeId.PROBE).first.tag
            end = time.time()
            print('Time::init:', end-start)
            await self.bot.chat_send("Time::init: {}".format(end-start))
            self.initinit = True


    async def testA(self):
        start = time.time()
        for m in self.marines:
            await self.bot.do(m(ATTACK, self.waypoint))
        end = time.time()
        print('Time::testA', end-start)
        await self.bot.chat_send("Time::testA: {}".format(end-start))

    async def testB(self):
        start = time.time()
        actions = []
        for m in self.marines:
            actions.append(m(ATTACK, self.waypoint2))
        await self.bot.do_actions(actions)
        end = time.time()
        print('Time::testB', end - start)
        await self.bot.chat_send("Time::testB: {}".format(end - start))

    async def testC(self):
        start = time.time()
        await self.bot.doGroup(AbilityId.ATTACK, self.marinetags, self.retreat)
        end = time.time()
        print('Time::testC', end - start)
        await self.bot.chat_send("Time::testC: {}".format(end - start))
        await self.testD()

    async def testD(self):
        start = time.time()
        htlist = [x.tag for x in self.bot.units(UnitTypeId.HIGHTEMPLAR)]
        await self.bot.doGroup(AbilityId.MORPH_ARCHON, htlist)
        end = time.time()
        print('Time::testD', end - start)
        await self.bot.chat_send("Time::testD: {}".format(end - start))

    async def testE(self):
        start = time.time()
        actlist = []
        for ht in self.bot.units(UnitTypeId.HIGHTEMPLAR):
            actlist.append(ht(AbilityId.MORPH_ARCHON))
        await self.bot.do_actions(actlist)
        end = time.time()
        print('Time::testE', end - start)
        await self.bot.chat_send("Time::testE: {}".format(end - start))




    async def buildingConstructionComplete(self, unit):
        pass
    async def unitCreated(self, unit):
        pass
    async def unitDestroyed(self, tag):
        if tag == self.testAtag:
            await self.testE()
        elif tag == self.testBtag:
            await self.testB()
        elif tag == self.testCtag:
            await self.testC()


    async def morph_archon(self, ht1, ht2):
        from s2clientprotocol import raw_pb2 as raw_pb
        from s2clientprotocol import sc2api_pb2 as sc_pb
        command = raw_pb.ActionRawUnitCommand(
                ability_id=AbilityId.MORPH_ARCHON.value,
                unit_tags=[ht1.tag, ht2.tag],
                queue_command=False
            )
        action = raw_pb.ActionRaw(unit_command=command)
        await self._client._execute(action=sc_pb.RequestAction(
                actions=[sc_pb.Action(action_raw=action)]
            ))