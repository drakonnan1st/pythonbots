import sc2, random
from sc2.data import ActionResult
from sc2 import run_game, maps, Race, Difficulty
from sc2.player import Bot, Computer
from sc2.constants import *
from sc2.position import Point2, Point3
from sc2.unit import Unit
from sc2.units import Units
from typing import Union #neede for selectMineralWorker, and any bigboi functions

class BioA(sc2.BotAI):
    def __init__(self):
        print('init')
        self.max_workers = 70
        self.byun = None
        self.rax1 = None
        self.scansneeded = 0
        self.startstim = False
        self.rallyworker = None
        self.rallyarmy = None
        self.micro = []
        self.macro = []
        self.fact1 = None
        self.cc1 = None
        self.depotmen = []
        self.autodepots = False
        self.autoupgrades = False
        self.drop1 = []
    def on_start(self):
        print('start')
        self.bo = 0
        self.derailed = False
        if self.enemy_start_locations[0].position[0] > self.game_info.map_center[0]:
            self.loc = {
                'ENEMYnat': Point2((143.5, 109.5)),
                'ENEMY3rd': Point2((144.5,82.5)),
                'depot1': Point2((35, 46)),
                'depot2': Point2((32,43)),
                'depot3': Point2((32,63)),
                'depot4': Point2((34,63)),
                'nat': Point2((24.5,54.5)),
                '3rd': Point2((23.5,81.5)),
                'rax1': Point2((34.5,43.5)),
                'rax2': Point2((24.5, 38.5)),
                'fact1': Point2((34.5,40.5)),
                'star1': Point2((34.5,37.5)),
                'engi1': Point2((30.5,67.5)),
                'engi2': Point2((34.5,60.5))
            }
            print('leftwards spawn')
            self.ENEMYlocnat = Point2((143.5, 109.5))
            self.locdepot1 = Point2((35,46))
            self.locdepot2 = Point2((32,43))
            self.locnat = Point2((24.5,54.5))
            self.locrax1 = Point2((34.5,43.5))
            self.locfact1 = Point2((34.5,40.5))
            self.locstar1 = Point2((34.5,37.5))
            self.locrax2 = Point2((24.5, 38.5))
        elif self.enemy_start_locations[0].position[0] < self.game_info.map_center[0]:
            self.loc = {
                'ENEMYnat': Point2((24.5, 54.5)),
                'ENEMY3rd': Point2((23.5,81.5)),
                'depot1': Point2((133,118)),
                'depot2': Point2((136,121)),
                'depot3': Point2((136,101)),
                'depot4': Point2((134,101)),
                'nat': Point2((143.5,109.5)),
                '3rd': Point2((144.5,82.5)),
                'rax1': Point2((131.5,120.5)),
                'rax2': Point2((143.5,125.5)),
                'fact1': Point2((131.5,123.5)),
                'star1': Point2((131.5,126.5)),
                'engi1': Point2((137.5,98.5)),
                'engi2': Point2((133.5,103.5))
            }
            print('righwards spawn')
            self.ENEMYlocnat = Point2((24.5, 54.5))
            self.locnat = Point2((143.5,109.5))
            self.locdepot1 = Point2((133,118))
            self.locdepot2 = Point2((136,121))
            self.locrax1 = Point2((131.5,120.5))
            self.locfact1 = Point2((131.5,123.5))
            self.locstar1 = Point2((131.5,126.5))
            self.locrax2 = Point2((143.5,125.5))

    async def on_step(self, iteration):
        #debug "bo: {}, lenworkers: {}".format(self.bo, len(self.workers))
        self._client.debug_text_screen("bo: {}, lenworkers: {}".format(self.bo, len(self.workers)), pos=(0.1,0.1))
        self._client.debug_text_screen("pending: {}".format(self.units.not_ready), pos=(0.1,0.2))
        #self._client.debug_text_simple("bo: {}, lenworkers: {}".format(self.bo, len(self.workers)))
        await self._client.send_debug()
        
        if not self.derailed:
            await self.buildOrder()
        await self.depotMicro()
        await self.dropmules()
        if self.autoupgrades == True:
            await self.macroUpgrades()

        if len(self.townhalls) > 1:
            t = self.townhalls.closest_to(self.rallyworker)
            if t.assigned_harvesters >= t.ideal_harvesters:
                for x in self.townhalls.prefer_close_to(t):
                    if x != t and x.assigned_harvesters < x.ideal_harvesters:
                        self.rallyworker = self.state.mineral_field.closest_to(x)
                        await self.setCCrally(self.rallyworker)

        if self.byun:
            await self.reaperMicro()

        if self.micro:
            await self.do_actions(self.micro)
            self.micro = []
        if self.macro:
            await self.do_actions(self.macro)
            self.macro = []
            

    async def on_unit_created(self, unit):
        if unit.type_id == UnitTypeId.COMMANDCENTER:
            unit.morph = None
            if self.rallyworker == None or len(self.townhalls) <= 2:
                if self.state.mineral_field.exists:
                    self.macro.append(unit(RALLY_WORKERS, self.state.mineral_field.closest_to(unit)))
                    print('rallied', unit, 'to nearby minerals')
            else:
                if len(self.townhalls) > 2:
                    self.macro.append(unit(RALLY_WORKERS, self.rallyworker))
        if unit.type_id in [BARRACKS, FACTORY, STARPORT]:
            if self.rallyarmy != None:
                self.macro.append(unit(RALLY_UNITS, self.rallyarmy))
            else:
                print('idk no army rally, for', unit)

    async def on_building_construction_complete(self, unit):
        if unit.type_id == REFINERY:
            g = self.geysers.closest_to(unit)
            mineralTags = [x.tag for x in self.state.units.mineral_field]
            print('refinery done on', g, g.position, '; assigned harvesters on_complete:', g.assigned_harvesters)
            scvring = self.units(SCV).closer_than(2.5, unit).prefer_close_to(unit)
            if len(scvring) > 3:
                scvs = scvring[:3]
                print('too many scvring; take first 3:', scvs)
            if len(scvring) < 3:
                print('too few scvring, only have', len(scvring))
                miners = self.units(SCV).filter(lambda x: (x.is_gathering and (not x.is_carrying_minerals) and x.orders[0].target in mineralTags) or x.is_idle and x not in scvring).prefer_close_to(unit)
                scvs = scvring + miners[:3-len(scvring)]
                print('should append ', 3-len(scvring), '; scvs:', scvs)
            if len(scvring) ==  3:
                scvs = scvring
            if len(scvs) == 3:
                for x in scvs:
                    print('SCV ordered to mine:', x)
                    self.micro.append(x.gather(g))
            else:
                print('ERROR scvs NOT 3 AAAAARRRGH')
        if unit in self.townhalls:
            if unit.position.distance_to_point2(self.loc['nat']) < 1:
                self.cc2 = unit
        else:
            print('on_construction: ', unit)


    async def buildOrder(self):
        if self.bo == 0:  # init orders
            self.cc1 = self.units(COMMANDCENTER).first
            await self.workersplit(self.cc1)
            await self.do(self.cc1.train(SCV))
            await self.do(self.cc1(RALLY_WORKERS, self.locdepot1)) #TODO locdepot1
            self.rallyworker = self.state.mineral_field.closest_to(self.cc1)
            self.cc1.rally = 'depot1'
            self.bo = 1
            print('bo now 1')
        if self.bo == 1:  # 14 depot
            await self.drone() #TODO self.drone
            if self.can_afford(SUPPLYDEPOT) and self.supply_used == 14:
                self.builderA = self.units(SCV).closest_to(self.loc['depot1'])
                await self.do(self.builderA.build(SUPPLYDEPOT, self.loc['depot1']))
                await self.chat_send('(glhf)')
                self.gas1 = self.state.vespene_geyser.closer_than(10, self.cc1).closest_to(self.locnat)
                await self.do(self.cc1(RALLY_WORKERS, self.state.mineral_field.closer_than(10,self.cc1).closest_to(self.gas1)))
                self.cc1.rally = 'min'
                self.builderB = None
                print('bo now 2')
                self.bo = 2
        if self.bo == 2:  # 15rax1, [2]scv queued till nat, 16gas
            await self.drone()
            if self.units.of_type([SUPPLYDEPOT, SUPPLYDEPOTLOWERED, SUPPLYDEPOTDROP]).ready.exists:
                if self.can_afford(BARRACKS):
                    self.macro.append(self.builderA.build(BARRACKS, self.locrax1))
                    print('rax1 started')
                    self.macro.append(self.builderA.gather(self.state.mineral_field.closer_than(10, self.locnat).closest_to(self.builderA.position), queue=True))#TODO locnat
                    self.macro.append(self.builderA.move(self.locnat, queue=True))
                if self.minerals >= 45:
                    if self.builderB == None:
                        mineralTags = [x.tag for x in self.state.units.mineral_field]
                        w = self.workers.filter(lambda w: w.is_gathering and w.is_carrying_minerals == False).closest_to(self.gas1.position.towards(self.cc1.position, 3))
                        self.builderB = w
                        #await self.do(w(HARVEST_RETURN)) #TODO Confirm return_resource
                        await self.do(w.move(self.gas1, queue=True))
                    else:
                        if self.can_afford(REFINERY):
                            v = self.state.vespene_geyser.closest_to(self.gas1.position)
                            await self.do(self.builderB.build(REFINERY, v))
                            self.builderB = None
                            print('build ref cleared builderB')
                            self.bo = 3
        if self.bo == 3:
            if len(self.workers) < 19:
                await self.drone()
            if self.supply_used == 17:
                if self.cc1.rally == 'min':
                    await self.do(self.cc1(RALLY_WORKERS, self.gas1.position))
                    self.cc1.rally = 'gas'
                    self.ref1 = self.units(REFINERY).first
                    print("gas1 loc: ", self.ref1.position)
            if self.supply_used == 18:
                if self.units(REFINERY).ready.exists:
                    g = self.geysers.closest_to(self.gas1.position)
                    if g.assigned_harvesters == 3:
                        if self.cc1.rally == 'gas':
                            await self.do(self.cc1(RALLY_WORKERS, self.state.mineral_field.closest_to(self.cc1)))
                            self.cc1.rally = 'min'
                        self.bo = 4
                        print('bo now 4')
        if self.bo == 4:
            if (self.supply_used < 19 and len(self.units(REAPER)) == 0):
                await self.drone()
            if self.units(BARRACKS).ready.exists:
                self.rax1 = self.units(BARRACKS).closest_to(self.locrax1)
                if self.can_afford(REAPER):
                    self.macro.append(self.rax1.train(REAPER))
                    self.macro.append(self.rax1(RALLY_UNITS, self.loc['ENEMYnat']))
                    print('reaper started, mins:', self.minerals)
            if self.supply_used == 20:
                print('19 workers exist')
                if self.cc1.is_idle:
                    print('mins when getting OC:', self.minerals)
                    await self.do(self.cc1(UPGRADETOORBITAL_ORBITALCOMMAND))
                    print(self.townhalls.first.orders, self.townhalls.first.build_progress, self.townhalls.first.type_id)
                if not AbilityId.COMMANDCENTERTRAIN_SCV in await self.get_available_abilities(self.cc1):
                    print('bo: 5')
                    self.bo = 5
        if self.bo == 5:
            if self.can_afford(NEXUS):
                self.macro.append(self.builderA.build(COMMANDCENTER, self.locnat))
                self.macro.append(self.builderA(SMART, self.state.mineral_field.closest_to(self.locnat), queue=True))
            if len(self.townhalls) == 2:
                self.bo = 6
                print('bo now 6')
        if self.bo == 6:
            await self.drone()
            if self.minerals >= 90 and len(self.units(BARRACKS)) < 2:
                mineralTags = [x.tag for x in self.state.units.mineral_field]
                if self.builderB == None:
                    self.builderB = self.workers.closer_than(10, self.cc1).filter(lambda w: len(w.orders) == 1 and w.orders[0].ability.id in [AbilityId.HARVEST_GATHER] and w.orders[0].target in mineralTags).first
                    await self.do(self.builderB.move(self.loc['rax2']))
                    self.cc2 = self.units(COMMANDCENTER).closest_to(self.loc['nat'])
                if self.can_afford(BARRACKS):
                    await self.do(self.builderB.build(BARRACKS, self.loc['rax2']))
                    await self.do(self.builderB.gather(self.state.mineral_field.closest_to(self.cc1), queue=True))
                    self.builderB = None
                    print('lost builder B')

            if self.units(BARRACKS).ready.noqueue.exists and len(self.units(BARRACKS)) == 2 and not self.units(BARRACKSREACTOR).exists:
                self.rax2 = self.units(BARRACKS).closest_to(self.loc['rax2'])
                print('rax noqueue, and ', self.rax1)
                if self.can_afford(BUILD_REACTOR):
                    print('can afford reactor')
                    await self.do(self.rax1.build(BARRACKSREACTOR))
            if self.units(REAPER).exists and self.byun == None:
                self.byun = self.units(REAPER).first
                self.byun.phase = 0
                print('got byun')
                mineralTags = [x.tag for x in self.state.units.mineral_field]
                self.builderB = self.units(SCV).closer_than(10, self.cc1).filter(lambda w: len(w.orders) == 1 and w.orders[0].ability.id in [AbilityId.HARVEST_GATHER] and w.orders[0].target in mineralTags).first
                print('got B:', self.builderB)
                await self.do(self.builderB.move(self.loc['depot2']))
            if self.units(BARRACKSREACTOR).exists and len(self.units.of_type([SUPPLYDEPOT, SUPPLYDEPOTLOWERED])) < 2:
                if self.can_afford(SUPPLYDEPOT) and self.builderB != None:
                    await self.do(self.builderB.build(SUPPLYDEPOT, self.loc['depot2']))
                    print('depot2started')
                    await self.do(self.builderB(SMART, self.state.mineral_field.closest_to(self.cc1), queue=True))
                    self.builderB = None
                    for g in self.state.vespene_geyser.closer_than(10, self.cc1):
                        if g.position != self.gas1.position:
                            self.gas2 = g
                            print('g is ', self.gas2, 'at loc', g.position)
                    
            if len(self.units.of_type([SUPPLYDEPOT, SUPPLYDEPOTLOWERED])) == 2:
                self.armyrally = self.units.of_type([SUPPLYDEPOT, SUPPLYDEPOTLOWERED]).closest_to(self.loc['depot2'])
                self.macro.append(self.rax2(RALLY_UNITS, self.armyrally))
                mineralTags = [x.tag for x in self.state.units.mineral_field]
                w = self.units(SCV).closer_than(10, self.cc1).filter(lambda w: len(w.orders) == 1 and w.orders[0].ability.id in [AbilityId.HARVEST_GATHER] and w.orders[0].target in mineralTags).first
                await self.do(w.move(self.gas2))
                await self.do(self.rax1(RALLY_UNITS, self.units.of_type([SUPPLYDEPOT, SUPPLYDEPOTLOWERED]).closest_to(self.loc['depot2'])))
                self.bo = 7
                print('bo now 7')
        if self.bo == 7:
            await self.drone()
            if self.units(BARRACKSREACTOR).ready.exists:
                print('barracksreactor exists')
                if len(self.rax1.orders) < 2 and self.can_afford(MARINE):
                    self.macro.append(self.rax1.train(MARINE))
            if len(self.units(REFINERY)) < 2:
                if self.can_afford(REFINERY):
                    w = self.units(SCV).closer_than(2, self.gas2)
                    if len(w) > 0:
                        await self.do(w[0].build(REFINERY, self.gas2))
                        print('gas2 started')
                        mineralTags = [x.tag for x in self.state.units.mineral_field]
                        self.builderB = self.units(SCV).closer_than(10, self.cc1).filter(lambda w: len(w.orders) == 1 and w.orders[0].ability.id in [AbilityId.HARVEST_GATHER] and w.orders[0].target in mineralTags).first
                        await self.do(self.builderB.move(self.loc['fact1']))
            if len(self.units(REFINERY)) == 2:
                if not self.units(FACTORY).exists and self.can_afford(FACTORY):
                    await self.do(self.builderB.build(FACTORY, self.loc['fact1']))
                    await self.do(self.builderB.move(self.loc['star1'], queue=True))
                if self.supply_used == 23:
                    if self.cc1.rally == 'min':
                        await self.do(self.cc1(RALLY_WORKERS, self.gas2.position))
                        self.cc1.rally = 'gas2'
                if len(self.units(REFINERY).ready) == 2:
                    g = self.geysers.closest_to(self.gas2)
                    if self.cc1.rally == 'gas2':
                        await self.do(self.cc1(RALLY_WORKERS, self.state.mineral_field.closest_to(self.cc1)))
                        self.cc1.rally = self.cc1.tag
##                    if g.assigned_harvesters < 2:
##                        w = self.units.idle.closest_to(self.gas2)
##                        if w != None:
##                            await self.do(w.gather(self.state.vespene_geyser.closest_to(self.gas2)))
##                    if g.assigned_harvesters < 3:
##                        x = self.units(SCV).closest_to(
##                            self.cc1.position.towards(self.state.mineral_field.closest_to(self.gas2).position,
##                                                      distance=2))
##                        await self.do(x.gather(self.state.vespene_geyser.closest_to(self.gas2)))
                    if g.assigned_harvesters == 3 and self.units(FACTORY).exists:
                        self.fact1 = self.units(FACTORY).first
                        print('got factory', self.fact1)
                        self.bo = 8
                        print('bo now 8')
                        await self.chat_send('bo now 8')
                        self.rax1 = self.units(BARRACKS).closest_to(self.loc['rax1'])
                        print('conrifm rax1: ', self.rax1)
                        self.rax2 = self.units(BARRACKS).closest_to(self.loc['rax2'])
                        print('confirm rax2: ', self.rax2)
        if self.bo == 8:
            orbitalpriority = False
            if len(self.townhalls.ready) == 2 and self.cc2.is_idle and len(self.units(ORBITALCOMMAND)) < 2:
                a = await self.get_available_abilities(self.cc2)
                if AbilityId.UPGRADETOORBITAL_ORBITALCOMMAND in a:
                    if self.can_afford(BARRACKS):
                        await self.do(self.cc2(UPGRADETOORBITAL_ORBITALCOMMAND))
                    orbitalpriority = True
                else:
                    orbitalpriority = False
            techlabpriority = (self.rax2.is_ready and not self.units(BARRACKSTECHLAB).exists) #if true, dont drone
            #factpriority = self.units(BARRACKSTECHLAB).exists and self.vespene > 92 and not self.units(FACTORY).exists
            stimpriority = (self.units(BARRACKSTECHLAB).ready.exists and self.units(BARRACKSTECHLAB).noqueue.exists and self.startstim == False)
            starportpriority = (self.units(FACTORY).ready.exists and not self.units(STARPORT).exists)
            reactorpriority = (self.units(FACTORY).ready.noqueue.exists and not self.units(FACTORYREACTOR).exists)
            if not orbitalpriority and not techlabpriority and not stimpriority and not starportpriority and not reactorpriority: #FIX
                await self.Production(['MARINE'])
                await self.drone()
            #else:
                #print('stopped marines, priority: oc', orbitalpriority, '; techlab', techlabpriority, '; stim', stimpriority, '; star', starportpriority, '; react', reactorpriority)
            
            if self.units(BARRACKSTECHLAB).ready.noqueue.exists:
                if self.can_afford(AbilityId.BARRACKSTECHLABRESEARCH_STIMPACK) and self.startstim == False:
                    await self.do(self.units(BARRACKSTECHLAB).first(AbilityId.BARRACKSTECHLABRESEARCH_STIMPACK))
                    print('started stim')
                    self.startstim = True
            if not self.units(BARRACKSTECHLAB).exists:
                        if self.units(BARRACKS).closest_to(self.loc['rax2']).is_ready:
                            print('rax2 idle')
                            if (self.minerals >= 50  and self.vespene >= 25): #self.can_afford(BUILD_TECHLAB) or 
                                await self.do(self.rax2.build(BARRACKSTECHLAB))
            if self.units(FACTORY).ready.exists:
                if not self.units(STARPORT).exists:
                    if self.can_afford(STARPORT):
                        await self.do(self.builderB.build(STARPORT, self.loc['star1']))
                else:
                    if self.can_afford(BUILD_REACTOR) and self.units(FACTORY).noqueue.exists:
                        self.fact1 = self.units(FACTORY).first
                        self.macro.append(self.fact1.build(FACTORYREACTOR))
                    if self.units(FACTORYREACTOR).exists:
                        self.bo = 9
                        self.star1 = self.units(STARPORT).first.tag
                        self.fact1 = self.units(FACTORY).first #should be redundant, but just incase
                        print('bo: 9, supply_used = ', self.supply_used)
        if self.bo == 9:
            if (self.units(REAPER).exists and self.supply_used >= 39) or (self.supply_used >= 38 and not self.units(REAPER).exists):
                if len(self.depotmen) == 0:
                    a = self.selectMineralDrone(self.cc2)
                    self.micro.append(a.move(self.loc['depot3']))
                    self.depotmen.append(a)
                    self.builderA = self.selectMineralDrone(self.cc2, a)
                    self.micro.append(self.builderA(MOVE, self.loc['depot4']))
                    print('bo9: Depotman1:', self.depotmen[0], 'builderA', self.builderA)
            if len(self.depotmen) > 0 and len(self.units.of_type([SUPPLYDEPOT, SUPPLYDEPOTLOWERED])) < 4:
                print('bo9: Depotman exists, <4 depots')
                if self.can_afford(PYLON):
                    if len(self.units.of_type([SUPPLYDEPOT, SUPPLYDEPOTLOWERED])) < 3:
                        self.cc2 = self.townhalls.closest_to(self.loc['nat'])
                        self.macro.append(self.builderA.build(SUPPLYDEPOT, self.loc['depot4']))
                        self.macro.append(self.builderA(SMART, self.state.mineral_field.closest_to(self.cc2), queue=True))
                        print('built depot 3')
                    if len(self.units.of_type([SUPPLYDEPOT, SUPPLYDEPOTLOWERED])) == 3:
                        self.macro.append(self.depotmen[0].build(SUPPLYDEPOT, self.loc['depot3']))
                        self.autodepots = True
                        print('built depot 4')
                        
            await self.Production(['MARINE'])
            await self.drone()
            if self.units(STARPORT).ready.exists and self.units(STARPORT).first.position.distance_to_point2(self.loc['fact1']) > 1:
                if not self.units(STARPORT).first.is_flying:
                    print('bo9: starport exists, not flying, distance to locfact:', self.units.by_tag(self.star1).position.distance_to_point2(self.loc['fact1']), '; await liftoff+move')
                    self.micro.append(self.units.by_tag(self.star1)(LIFT_STARPORT))
                    #self.micro.append(self.star1(MOVE, self.loc['fact1'], queue=True)) #doesnt work
            if self.units(STARPORTFLYING).exists: #Entire block doesnt work
                if self.units(STARPORTFLYING).first.is_idle:
                    print('bo9: starport exists, flying, distance to locfact:', self.units.by_tag(self.star1).position.distance_to_point2(self.loc['fact1']), '; await move')
                    self.micro.append(self.units.by_tag(self.star1)(MOVE, self.loc['fact1']))
            if self.units(FACTORYREACTOR).ready.exists:
                print('bo9: reactor done, await liftoff factory, queue land factory')
                self.micro.append(self.fact1(LIFT_FACTORY))
            if self.units(STARPORTFLYING).exists and self.units(FACTORYFLYING).exists:
                print('bo9: both in air; land starport, star1:', self.units.by_tag(self.star1))
                self.micro.append(self.units.by_tag(star1)(LAND_STARPORT, self.loc['fact1']))
                self.micro.append(self.fact1(LAND_FACTORY, self.loc['star1']))

            
##            if self.units(STARPORT).ready.exists and not self.star1.has_add_on:
##                if (not self.star1.is_flying):
##                    await self.do(self.star1(LIFT_STARPORT))
##                    await self.do(self.star1(MOVE, self.loc['fact1'], queue=True))
####                else:
####                    if self.star1.is_idle:
####                        await self.do(self.star1(MOVE, self.loc['fact1']))
##            if self.units(FACTORYREACTOR).ready.exists:
##                if not self.fact1.is_flying:
##                    await self.do(self.fact1(LIFT_FACTORY))
##            if self.units(REACTOR).exists:
##                if self.fact1.is_idle:
##                    await self.do(self.star1(LAND_STARPORT, self.loc['fact1']))
##                    await self.do(self.fact1(LAND_FACTORY, self.loc['star1']))

                
            if self.units(STARPORTREACTOR).exists:
                await self.Production(['MEDIVAC'])
                self.star1 = self.units(STARPORT).first
                if len(self.star1.orders) == 2:
                    self.autoupgrades = True
                    self.bo = 10
                    if self.already_pending(MEDIVAC):
                        print('bo: 9.9, medivac pending')
                    print('bo: 10')
        if self.bo == 10:
            await self.Production(['MARINE'])
            if len(self.units(REFINERY)) < 4 and self.can_afford(REFINERY):
                print('lenREF < 4 and can afford refinery')
                for g in self.state.vespene_geyser.closer_than(10, self.loc['nat']):
                    print('bo10: got g:', g)
                    if g.position.distance_to_point2(self.units(REFINERY).closest_to(g).position) > 1:
                        print('bo10: g pos:', g.position, '.dist to nearest ref:', g.position.distance_to_point2(self.units(REFINERY).closest_to(g).position))
                        w = self.selectMineralDrone(self.cc2)
                        self.macro.append(w.build(REFINERY, g))
            if len(self.units(REFINERY)) == 4 and len(self.units(ENGINEERINGBAY)) < 2:
                if self.minerals < 250:
                    w = self.selectMineralDrone(self.cc2)
                    w2 = self.selectMineralDrone(self.cc2, w)
                    if w and w2:
                        self.macro.append(w.build(ENGINEERINGBAY, self.loc['engi1']))
                        self.macro.append(w(SMART, self.state.mineral_field.closest_to(self.cc2), queue=True))
                        self.macro.append(w2.build(ENGINEERINGBAY, self.loc['engi2']))
                        self.macro.append(w2(SMART, self.state.mineral_field.closest_to(self.cc2), queue=True))
            if len(self.drop1)  == 0 and len(self.units(MARINE)) == 16:
                self.drop1 = [x for x in self.units(MARINE)]
            if len(self.drop1) == 16 and len(self.units(MEDIVAC)) == 2:
                self.drop1.append(self.units(MEDIVAC)[0])
                self.drop1.append(self.units(MEDIVAC)[1])
                print('drop1 done')
            
                    
    def selectMineralDrone(self, th, notin: Union["Unit", "Units", int, list]=[]):
        rm = []
        if isinstance(notin, Unit):
            rm = [notin.tag]
        elif isinstance(notin, int):
            rm = [notin]
        elif isinstance(notin, Units):
            rm = [x.tag for x in notin]
        elif isinstance(notin, list):
            rm = notin
        mineralTags = [x.tag for x in self.state.units.mineral_field]
        w = self.units(SCV).closer_than(10, th).filter(lambda w: w.is_gathering and (not w.is_carrying_minerals) and (w.tag not in rm) and w.orders[0].target in mineralTags)
        if len(w) == 0:
            w = self.units(SCV).closer_than(10, th).filter(lambda w: (w.is_gathering) and (w.tag not in rm) and w.orders[0].target in [mineralTags])
            if len(w) == 0:
                ww = self.units(SCV).filter(lambda w: w not in rm).closest_to(th)
                print('selectMineralDrone: had to pick closest worker')
        else:
            ww = w[0]
        return ww


    async def dropmules(self):
        if self.units(ORBITALCOMMAND).exists:
            for x in self.units(ORBITALCOMMAND).ready.filter(lambda x: x.energy >= 50):
                if self.scansneeded == 0:
                    a = await self.get_available_abilities(x)
                    if AbilityId.CALLDOWNMULE_CALLDOWNMULE in a:
                        m = []
                        for y in self.townhalls.ready:
                            m += self.state.mineral_field.closer_than(10,y)
                        if len(m) > 0:
                            m2 = max(m, key=lambda x: x.mineral_contents)
                            self.micro.append(x(CALLDOWNMULE_CALLDOWNMULE, m2))

    async def setCCrally(self, target, exclude=[]):
        for x in self.townhalls:
            if x not in exclude:
                self.macro.append(x(RALLY_WORKERS, target))

    async def setProdrally(self, target, exclude=[]):
        for x in self.units.of_type([BARRACKS, STARPORT, FACTORY]):
            if x not in exclude:
                self.macro.append(x(RALLY_UNITS, target))

    async def macroUpgrades(self):
        if self.units.of_type([BARRACKSTECHLAB, ENGINEERINGBAY, ARMORY]).ready.noqueue.exists:
            upgs = await self.get_available_abilities(self.units.of_type([BARRACKSTECHLAB, ENGINEERINGBAY, ARMORY]).ready.noqueue)
            print('macroupgrades: upgs:', upgs)
            for x in self.units.of_type([BARRACKSTECHLAB, ENGINEERINGBAY, ARMORY]).ready.noqueue: #only get bio upgrades, and maybe +1tank
                if AbilityId.RESEARCH_COMBATSHIELD in upgs and self.can_afford(AbilityId.RESEARCH_COMBATSHIELD): #TODO: check if can_afford redundant
                    self.macro.append(self.units(BARRACKSTECHLAB).noqueue.first(AbilityId.RESEARCH_COMBATSHIELD))
                elif AbilityId.RESEARCH_CONCUSSIVESHELLS in upgs and self.units(MARAUDER).exists: #Note: if no marauders alive, wont get it
                    self.macro.append(self.units(BARRACKSTECHLAB).noqueue.first(AbilityId.RESEARCH_CONCUSSIVESHELLS))
                for bioupg in [AbilityId.ENGINEERINGBAYRESEARCH_TERRANINFANTRYWEAPONSLEVEL1, AbilityId.ENGINEERINGBAYRESEARCH_TERRANINFANTRYWEAPONSLEVEL2, AbilityId.ENGINEERINGBAYRESEARCH_TERRANINFANTRYWEAPONSLEVEL3,
                               AbilityId.ENGINEERINGBAYRESEARCH_TERRANINFANTRYARMORLEVEL1, AbilityId.ENGINEERINGBAYRESEARCH_TERRANINFANTRYARMORLEVEL2, AbilityId.ENGINEERINGBAYRESEARCH_TERRANINFANTRYARMORLEVEL3]:
                    if bioupg in upgs:
                        if self.can_afford(bioupg):
                            self.macro.append(self.units(ENGINEERINGBAY).noqueue.first(bioupg))
                            break
                #TODO: Armory research

    async def workersplit(self, th):
        wPool = self.workers.closer_than(10, th)
        for w in wPool:
            self.micro.append(w.gather(self.state.mineral_field.closest_to(w)))

    async def drone(self):
        if len(self.workers) < self.max_workers and self.supply_left > 0:
            if self.units(BARRACKS).ready.exists:
                ths = self.units.of_type([ORBITALCOMMAND, PLANETARYFORTRESS]).ready.noqueue
            else:
                ths = self.townhalls.ready.noqueue
            if len(ths) > 0:
                for th in ths:
                    if self.can_afford(SCV):
                        self.macro.append(th.train(SCV))

    async def doAbility(self, unit, ability, queue=False, mode='micro'):
        a = await self.get_available_abilities(unit)
        if ability in a and self.can_afford(ability):
            if mode == 'micro':
                self.micro.append(unit(ability, queue=queue))
            if mode == 'macro':
                self.macro.append(unit(ability, queue=queue))
            if mode == 'do':
                await self.do(unit(ability, queue=queue))

    async def depotMicro(self):
        for x in self.units.of_type([SUPPLYDEPOT, SUPPLYDEPOTDROP]).ready:
            if not self.known_enemy_units.closer_than(4, x).exists:
                self.micro.append(x(MORPH_SUPPLYDEPOT_LOWER))
        for x in self.units.of_type(SUPPLYDEPOTLOWERED).ready:
            if self.known_enemy_units.closer_than(3,x).exists:
                self.micro.append(x(MORPH_SUPPLYDEPOT_RAISE))

    async def Production(self, goals):
        for scv in self.depotmen:
            if scv.is_idle:
                if self.can_afford(PYLON):
                    loc = await self.find_placement(PYLON, self.loc['depot3'].towards(self.game_info.map_center, distance=4), placement_step=2)
                    if loc:
                        self.macro.append(scv.build(SUPPLYDEPOT, loc))
                        print('building another one')
        reactorrax = self.units(BARRACKS).filter(lambda x: x.add_on_tag in [y.tag for y in self.units(BARRACKSREACTOR)])
        techrax = self.units(BARRACKS).filter(lambda x: x.add_on_tag in [y.tag for y in self.units(BARRACKSTECHLAB)])
        techfact = self.units(FACTORY).filter(lambda x: x.add_on_tag in [y.tag for y in self.units(FACTORYTECHLAB)])
        reactorport = self.units(STARPORT).filter(lambda x: x.add_on_tag in [y.tag for y in self.units(STARPORTREACTOR)])
        if 'MARAUDER' in goals:
            for rax in techrax:
                if rax.is_idle and self.can_afford(MARAUDER):
                    self.macro.append(rax.train(MARAUDER))
        if 'MARINE' in goals:
            for rax in reactorrax + techrax:
                if (rax in reactorrax and len(rax.orders) < 2) or (rax in techrax and rax.is_idle): #self.already_pending(MARINE) + self.already_pending(MARAUDER) < 2*len(reactorrax) + len(techrax) and self.can_afford(MARINE)
                    if self.can_afford(MARINE):
                        self.macro.append(rax.train(MARINE))
                        #print('Prod: rax order length', len(rax.orders))
        if 'TANK' in goals:
            for fact in techfact:
                if fact.is_idle and self.can_afford(SIEGETANK):
                    self.macro.append(fact.train(SIEGETANK))
        if 'MEDIVAC' in goals:
            for port in reactorport:
                if len(port.orders) <  2 and self.can_afford(MEDIVAC):
                    self.macro.append(port.train(MEDIVAC))

	async def reaperMicro(self):
        if self.byun.phase == 0: #just built, kill anything on sight, else move on
            if self.known_enemy_units.closer_than(50, self.cc1):
                e = self.known_enemy_units.closest_to(self.byun)
                if not self.byun.is_attacking:
                    await self.micro.append(self.byun(ATTACK, e))
                if self.byun.ground_range > e.ground_range and self.byun.position.distance_to(e) < self.byun.ground_range:
                    if self.byun.weapon_cooldown > 0:
                        if self.byun.is_attacking:
                            loc = self.byun.position.towards(e.position, -1)
                            if
                            await self.micro.append(self.byun(MOVE))
            else:a
    
            
        
            
        




run_game(maps.get("(2)LostandFoundLE"), [
    Bot(Race.Terran, BioA()),
    Computer(Race.Zerg, Difficulty.Easy)],
    realtime=False)
