import sc2, random
from sc2.data import ActionResult
from sc2 import run_game, maps, Race, Difficulty
from sc2.player import Bot, Computer
from sc2.constants import *
from sc2.position import Point2, Point3
from sc2.unit import Unit #random, Unit, ActionResult, Point3 all from BuRny's find_placement

class Toss1(sc2.BotAI):
    def __init__(self):
        print('init')
        self.gas1probes = []
        self.gas2probes = []
        self.derailed = False
        self.last_iteration = -1
    def on_start(self):
        self.lowSupplyLeftCondition = lambda other: \
            all([ #8*pylons_pending + 8*pylons not_ready + supply cap < 200 (i.e. will making another pylon go over 200 max supply?)
			8 * sum(other.already_pending(x) for x in {PYLON}) + 8 * other.units.of_type({PYLON}).not_ready.amount + other.supply_cap < 200,
		other.townhalls.exists,
		other.supply_used >= 14,
		(other.units.of_type({GATEWAY, WARPGATE}).filter(lambda u: u.build_progress > 0.6).amount
		- 8 * other.units.of_type(PYLON).not_ready.amount
		- 15 * other.units(NEXUS).not_ready.filter(lambda u: u.build_progress > 0.8).amount)
		* 20/18 > other.supply_left,
		sum(other.already_pending(x) for x in {PYLON}) < 2 ]) #pending includes under construction, not just 'ordered'
                
        self.bo = 0
        print('start')
        self.gas1 = None
        self.gas2 = None
        self.nexiA = None
        self.pylon1probe = None
        self.scooter = None
        self.antiOV = None
        self.gandalf = None
        if self.enemy_start_locations[0].position[0] > self.game_info.map_center[0]:
            print('bot left')
            self.spawn = 7
            self.locenemnat = Point2((143.5, 109.5))
            self.locpylon1 = Point2((28,60))
            self.locpylon2 = Point2((32,23))
            self.locpylon3 = Point2((30,25))
            self.locgate1 = Point2((30.5,65.5))
            self.locnat = Point2((24.5, 54.5))
            self.loccore = Point2((31.5, 61.5))
            self.loctwil = Point2((26.5,20.5))
            self.loc3rd = Point2((23.5,81.5))
            self.locrobo = Point2((26.5,23.5))
            self.locwall = Point2((30.5,63.5))
            self.locOVA = Point2((38,45))
            self.locOVB = Point2((48,35))
            self.locwarp = Point2((124,127))
            self.locgate2 = Point2((32.5,58.5))
            self.locgate3 = Point2((29.5,22.5))
            self.locgate4 = Point2((29.5,19.5))
            self.locgate5 = Point2((32.5,20.5))
            self.locgate6 = Point2((32.5,28.5))
            self.locgate7 = Point2((35.5,26.5))
            self.locgate8 = Point2((35.5,23.5))
        if self.enemy_start_locations[0].position[0] < self.game_info.map_center[0]:
            print('top right')
            self.spawn = 1
            self.locenemnat = Point2((24.5, 54.5))
            self.locpylon1 = Point2((140,104))
            self.locpylon2 = Point2((136,141))
            self.locpylon3 = Point2((138,139))
            self.locgate1 = Point2((135.5,105.5))
            self.locnat = Point2((143.5, 109.5))
            self.loccore = Point2((136.5, 102.5))
            self.loctwil = Point2((141.5,143.5))
            self.loc3rd = Point2((144.5,82.5))
            self.locrobo = Point2((141.5,140.5))
            self.locwall = Point2((137.5,100.5))
            self.locOVA = Point2((130,119))
            self.locOVB = Point2((120,129))
            self.locwarp = Point2((44,37))
            self.locgate2 = Point2((137.5,98.5))
            self.locgate3 = Point2((138.5,141.5))
            self.locgate4 = Point2((138.5,144.5))
            self.locgate5 = Point2((135.5,143.5))
            self.locgate6 = Point2((135.5,135.5))
            self.locgate7 = Point2((132.5,137.5))
            self.locgate8 = Point2((132.5,140.5))
            
        
    async def on_step(self, iteration):
        if self.last_iteration < iteration:
            self.last_iteration = iteration
        else:
            return
        if self.bo == 0:
            self.nexi = self.units(NEXUS).first
            self.nexi.rally = 'min'
            await self.build_workers()
            await self.chat_send('(glhf)')
            await self.workersplit(self.nexi)
            self.bo = 1
        if self.derailed == False:
            await self.buildOrder()
        await self.microGandalf()
        if self.bo >= 7 and len(self.units(PYLON)) >= 3:
            if self.lowSupplyLeftCondition(self):
                if self.can_afford(PYLON):
                    if self.scooter:
                        loc = await self.find_placement(PYLON, self.locOVB, placement_step=1)
                        if loc:
                            await self.do(self.scooter.build(PYLON, loc))
                            if len(self.units(WARPGATE)) > 4:
                                await self.do(self.scooter.build(PYLON, loc, queue=True))
        if self.bo == 99: #allin starts
            await self.microPrism()
            if self.allin == 1:
                if (self.prism.phase == 1 or self.prism.phase == 5) and self.minerals >= 800:
                    for x in self.units(WARPGATE).ready:
                        a = await self.get_available_abilities(x)
                        if WARPGATETRAIN_ZEALOT in a:
                            if self.units(WARPPRISMPHASING).exists:
                                await self.warpin(ZEALOT, self.units(WARPPRISMPHASING).first.position.to2, x)
                                print('warped from phasing')
                if self.units(ZEALOT).exists:
                    if self.prism.phase == 1:
                        print('prism phase now 2')
                        self.prism.phase = 2


                for x in self.units(ZEALOT).ready.idle.filter(lambda y: len(y.orders) == 0 and y not in self.units(ZEALOT).closer_than(1, self.locenemnat)):
                    await self.do(x.attack(self.enemy_start_locations[0]))
                    await self.do(x.attack(self.locenemnat, queue=True))
                    print('gave order to x')
                
                
                        
                        
                
                
                    

                    
            
        if self.bo == 19:    
            await self.build_workers()
            await self.build_pylons()
            await self.expand()
            await self.build_assimilator()
            await self.build_assimilator()
            await self.build_production()
            await self.build_stalker()

    async def microPrism(self):
        if self.units(WARPPRISM).exists or self.units(WARPPRISMPHASING).exists:
            if self.allin == 1:
                if self.prism.phase == 0: #in transit
                    if self.units(WARPPRISM).idle.exists:
                        self.locprism = self.prism.position
                        await self.do(self.prism(MORPH_WARPPRISMPHASINGMODE))
                        self.prism.phase = 1
                if self.prism.phase == 2:
                    print('len of close zlots ', len(self.units(ZEALOT).ready.closer_than(4, self.locprism)))
                    print('len of ready zlots ', len(self.units(ZEALOT).ready))
                    if self.units(ZEALOT).exists and len(self.units(ZEALOT).ready) >= 4:
                            print('no nearby zealots')
                            await self.do(self.units(WARPPRISMPHASING).first(MORPH_WARPPRISMTRANSPORTMODE))

                            self.prism.phase = 3
                if self.prism.phase == 3:
                    if self.units(WARPPRISM).idle.exists:
                        await self.do(self.units(WARPPRISM).first.move(self.locwarp))
                        self.prism.phase = 4
                if self.prism.phase == 4:
                    if self.units(WARPPRISM).closer_than(1, self.locwarp).idle.exists:
                        self.locprism = self.prism.position
                        await self.do(self.prism(MORPH_WARPPRISMPHASINGMODE))
                        self.prism.phase = 5


    async def chrono(self, user, t):
        if not t.has_buff(BuffId.CHRONOBOOSTENERGYCOST):
            a = await self.get_available_abilities(user)
            if AbilityId.EFFECT_CHRONOBOOSTENERGYCOST in a:
                await self.do(user(AbilityId.EFFECT_CHRONOBOOSTENERGYCOST, t))
                
    async def workersplit(self, th):
        wPool = self.workers.closer_than(10, th)
        for w in wPool:
            await self.do(w.gather(self.state.mineral_field.closest_to(w)))

    async def build_production(self):
        if self.units(PYLON).ready.exists:
            pylon = self.units(PYLON).ready.random
            if self.units(GATEWAY).ready.exists:
                if not self.units(CYBERNETICSCORE):
                    if self.can_afford(CYBERNETICSCORE) and not self.already_pending(CYBERNETICSCORE):
                        await self.build(CYBERNETICSCORE, near=pylon)
            else:
                if self.can_afford(GATEWAY) and not self.already_pending(GATEWAY):
                    await self.build(GATEWAY, near=pylon)

    async def build_stalker(self):
        for gate in self.units(GATEWAY).ready.noqueue:
            if self.can_afford(STALKER) and self.supply_left > 1:
                await self.do(gate.train(STALKER))
        
        
    async def build_workers(self):
        for nexus in self.units(NEXUS).ready.noqueue:
            if len(self.workers) < 31:
                if self.can_afford(PROBE):
                    await self.do(nexus.train(PROBE))
                
    async def build_pylons(self):
        if self.supply_left < 5 and not self.already_pending(PYLON):
            nexuses = self.units(NEXUS).ready
            if nexuses.exists:
                if self.can_afford(PYLON):
                    await self.build(PYLON, near=nexuses.first)

    async def expand(self):
        if self.units(NEXUS).amount < 2 and self.can_afford(NEXUS):
            await self.expand_now()
            
    async def build_assimilator(self):
        if self.units(GATEWAY).exists:
            for nexus in self.units(NEXUS).ready:
                vespenes = self.state.vespene_geyser.closer_than(10.0, nexus)
                for vespene in vespenes:
                    if not self.can_afford(ASSIMILATOR):
                        break
                    worker = self.select_build_worker(vespene.position)
                    if worker is None:
                        break
                    if not self.units(ASSIMILATOR).closer_than(1, vespene).exists:
                        await self.do(worker.build(ASSIMILATOR, vespene))

    async def microAntiOV(self):
        if self.antiOV != None:
            if self.antiOV.is_idle:
                await self.do(self.antiOV.move(self.locOVA))
                await self.do(self.antiOV(PATROL, self.locOVB, queue=True))
    async def microGandalf(self):
        if self.gandalf != None:
            if self.units(ADEPT).exists:
                #init
                if self.gandalf.state == 0: #0: not initialised
                    await self.do(self.gandalf.move(self.locwall))
                    self.gandalf.state = 1 #1: at ease
                else:
                    if self.known_enemy_units.closer_than(6, self.gandalf).exists:
                        await self.do(self.gandalf.move(self.locwall))
                        await self.do(self.gandalf(HOLDPOSITION, queue=True))
                        self.gandalf.state = 2 #2: on Hold
                    else:
                        if self.gandalf.state == 2:
                            await self.do(self.gandalf.move(self.locwall))
                            await self.do(self.gandalf(STOP, queue=True))
                            self.gandalf.state = 1

    async def warpin(self, unit, location, warpgate):
        if isinstance(location, sc2.unit.Unit):
            location = location.position.to2
        elif location is not None:
            location = location.to2
        abilities = await self.get_available_abilities(warpgate)
        if AbilityId.WARPGATETRAIN_ZEALOT in abilities:
            pos = location.random_on_distance(1.5)
            placement = await self.find_placement(AbilityId.WARPGATETRAIN_ZEALOT, pos, placement_step=1)
            if placement is None:
                # return ActionResult.CantFindPlacementLocation
                print("can't place")
                return
            await self.do(warpgate.warp_in(unit, placement))
            print('warped in ', unit, 'at ', pos)
                    
                
            

    #The Build Order, 8-gate Chargelot Allin from Classic vs Serral IEM KATOWICE 2018
    #Follows this so long as opponent does nothing
    async def buildOrder(self):
        if self.bo == 1:
            if self.minerals >= 40:
                if self.pylon1probe == None:
                    self.pylon1probe = self.select_build_worker(self.locpylon1, force=True)
                    await self.do(self.pylon1probe.move(self.locpylon1))
                    print('self.bo now 2')
                    self.bo = 2
        if self.bo == 2:
            await self.build_workers()
            if not self.units(PYLON).exists:
                if self.can_afford(PYLON):
                    await self.do(self.pylon1probe.build(PYLON, self.locpylon1))
            else:
                self.bo = 3
                print('self.bo now 3')
                await self.do(self.pylon1probe.gather(self.state.mineral_field.closest_to(self.nexi), queue=True))
                self.pylon1probe = None
                
        if self.bo == 3:
            if self.supply_used < 21:
                await self.build_workers()
            if self.supply_used == 15 and len(self.workers)==14:
                if self.gas1 == None:
                    self.gas1 = self.state.vespene_geyser.closer_than(8, self.nexi).prefer_close_to(self.locpylon1)[0]
                    print('gas1 loc: ', self.gas1.position)
                    await self.do(self.nexi(RALLY_WORKERS, self.gas1.position))
                    self.nexi.rally = 'gas'
            if len(self.workers) == 15 and not self.units(ASSIMILATOR).exists:
                if self.supply_used == 16:
                    await self.chrono(self.nexi, self.nexi)
                for p in self.workers.idle:
                    if p is None:
                        break
                    else:
                        self.scooter = p
                        if self.can_afford(ASSIMILATOR):
                            await self.do(p.build(ASSIMILATOR, self.gas1))
                            
                            await self.do(self.scooter.move(self.locgate1, queue=True))
                            mins = self.state.mineral_field.closer_than(10, self.nexi)
                            minn = mins.closest_to(mins.center)
                            await self.do(self.nexi(RALLY_WORKERS, minn))
                            self.nexi.rally = 'min'
            if self.units(ASSIMILATOR).exists:
                self.ass1 = self.units(ASSIMILATOR).first
                if not self.units(GATEWAY).exists:
                    
                    if self.supply_used == 17:
                        if self.can_afford(GATEWAY):
                            await self.do(self.scooter.build(GATEWAY, self.locgate1))
                            await self.do(self.scooter.move(self.gas1.position.towards(self.nexi.position), queue=True))
                            await self.do(self.scooter.move(self.gas1.position, queue=True))
                else:
                    if self.units(ASSIMILATOR).ready.exists:
                        if self.scooter != None and self.supply_used < 20:
                            self.ass1 = self.units(ASSIMILATOR).first
                            await self.do(self.scooter.gather(self.ass1))
                            self.gas1probes.append(self.scooter.tag)
                            print('added scooter to gas1:', self.gas1probes)
                            self.scooter = None
                    if self.supply_used == 18:
                        if self.nexi.rally == 'min':
                            await self.do(self.nexi(RALLY_WORKERS, self.ass1))
                            self.nexi.rally = 'gas'
                    if self.supply_used == 19:
                        if len(self.gas1probes) == 1:
                            self.gas1probes.append(self.units(PROBE).closest_to(self.nexi.position.towards(self.gas1.position, distance=4)).tag)
                            print('added to gas1:', self.gas1probes)
                        if self.nexi.rally == 'gas':
                            self.nexi.rally = 'nat'
                            await self.do(self.nexi(RALLY_WORKERS, self.locnat))
                    if self.supply_used == 20:
                        if self.nexi.rally == 'nat':
                            await self.do(self.nexi(RALLY_WORKERS, self.ass1))
                            self.nexi.rally = 'gas'
                        if self.nexi.noqueue:
                            if self.scooter == None:
                                self.scooter = self.units(PROBE).closest_to(self.locnat)
                                print('got p after 19 probe')
                        if self.can_afford(NEXUS):
                            await self.do(self.scooter.build(NEXUS, self.locnat))
                            self.nexiA = self.units(NEXUS)
                            await self.do(self.scooter.gather(self.state.mineral_field.closest_to(self.locnat), queue=True))
                            await self.do(self.scooter.move(self.loccore, queue=True))
                    if self.supply_used == 21:
                        if len(self.gas1probes) == 2:
                            self.gas1probes.append(self.units(PROBE).filter(lambda w: w.tag not in self.gas1probes).closest_to(self.nexi.position.towards(self.gas1.position, distance=3)).tag)
                            print('added to gas1:', self.gas1probes)
                        if self.can_afford(CYBERNETICSCORE) and self.units(GATEWAY).ready.exists:
                            self.nexi2 = self.units(NEXUS).closest_to(self.locnat)
                            await self.do(self.scooter.build(CYBERNETICSCORE, self.loccore))
                            await self.do(self.scooter.return_resource(queue=True))
                            print('cyber built')
                            await self.do(self.scooter.move(self.locpylon2, queue=True))
                            if self.nexi.rally == 'gas':
                                for geyser in self.state.vespene_geyser.closer_than(10, self.nexi):
                                    if geyser.position != self.gas1.position:
                                        self.gas2 = geyser
                                self.nexi.rally = 'gas2'
                                print('gas 2 loc: ', self.gas2.position, 'gas1 loc: ', self.gas1.position)
                                await self.do(self.nexi(RALLY_WORKERS, self.gas2.position))
                                self.bo = 4
        if self.bo == 4:
            await self.build_workers()
            if self.supply_used == 22:
                if len(self.units(ASSIMILATOR)) == 1:
                    for p in self.workers.idle:
                        if p is None:
                            break
                        else:
                            self.scooterB = p
                            if self.can_afford(ASSIMILATOR):
                                await self.do(p.build(ASSIMILATOR, self.gas2))
                                await self.do(self.scooterB.move(self.gas2.position.towards(self.nexi.position), queue=True))
                                print('got p2 before 22 gas')
                if len(self.units(ASSIMILATOR)) == 2:
                    self.ass2 = self.units(ASSIMILATOR).closest_to(self.gas2)
                    if self.can_afford(PYLON):
                        await self.do(self.scooter.build(PYLON, self.locpylon2))
                        await self.do(self.scooter.move(self.gas2.position.towards(self.nexi.position), queue=True))
                        await self.do(self.scooter.move(self.gas2.position, queue=True))
            if self.supply_used == 23:
                if self.nexi.rally == 'gas2':
                    self.nexi.rally = 'min2'
                    await self.do(self.nexi(RALLY_WORKERS, self.state.mineral_field.closest_to(self.locnat)))
                    await self.do(self.units(NEXUS).closest_to(self.locnat)(RALLY_WORKERS, self.state.mineral_field.closest_to(self.locnat)))
            if len(self.units(ASSIMILATOR).ready) == 2:
                if self.scooter != None and not self.units(CYBERNETICSCORE).ready.exists:
                    p = self.workers.filter(lambda w: w.is_idle == True)
                    for w in p:
                        self.gas2probes.append(w.tag)
                        await self.do(w.gather(self.ass2))
                    self.scooter = None
                    self.scooterB = None
            if self.units(CYBERNETICSCORE).ready.exists:
                if self.units(GATEWAY).noqueue.exists:
                    g = self.units(GATEWAY).noqueue.first
                    await self.do(g.train(ADEPT))
                    await self.do(self.nexi(AbilityId.EFFECT_CHRONOBOOSTENERGYCOST, self.units(GATEWAY).first))
                    mineralTags = [x.tag for x in self.state.units.mineral_field]
                    self.scooter = self.workers.closer_than(10, self.nexi).filter(lambda w: len(w.orders) == 1 and w.orders[0].ability.id in [AbilityId.HARVEST_GATHER] and w.orders[0].target in mineralTags).first
                    await self.do(self.scooter.move(self.loctwil))
                if self.can_afford(TWILIGHTCOUNCIL):
                    await self.do(self.scooter.build(TWILIGHTCOUNCIL, self.loctwil))
                    await self.do(self.scooter.gather(self.state.mineral_field.closest_to(self.nexi), queue=True))
                    self.scooter = None
                    self.bo = 5
        if self.bo == 5:
            if self.supply_used < 36:
                await self.build_workers()
            if self.supply_used == 27:
                if self.can_afford(AbilityId.RESEARCH_WARPGATE):
                    if self.units(CYBERNETICSCORE).first.is_idle:
                        await self.do(self.units(CYBERNETICSCORE).first(AbilityId.RESEARCH_WARPGATE))
                if self.units(ADEPT).exists:
                    if self.units(GATEWAY).first.is_idle:
                        if self.can_afford(STALKER):
                            await self.do(self.units(GATEWAY).first.train(STALKER))
                        self.gandalf = self.units(ADEPT).first
                        self.gandalf.state = 0
            if len(self.units(NEXUS).ready) == 2 and not self.units(TWILIGHTCOUNCIL).ready.exists:
                for n in self.units(NEXUS):
                    if not n.has_buff(BuffId.CHRONOBOOSTENERGYCOST):
                        await self.do(self.nexi2(RALLY_WORKERS, self.state.mineral_field.closest_to(self.locnat)))
                        await self.chrono(n, n)
            if self.units(TWILIGHTCOUNCIL).filter(lambda x: x.build_progress > 0.9):
                if not self.units(ROBOTICSFACILITY).exists:
                    if self.scooter == None:
                        mineralTags = [x.tag for x in self.state.units.mineral_field]
                        self.scooter = self.workers.closer_than(10, self.nexi).filter(lambda w: len(w.orders) == 1 and w.orders[0].ability.id in [AbilityId.HARVEST_GATHER] and w.orders[0].target in mineralTags).first
                        await self.do(self.scooter.move(self.locrobo))
            if self.units(TWILIGHTCOUNCIL).ready.exists:
                    if self.can_afford(ROBOTICSFACILITY) and not self.units(ROBOTICSFACILITY).exists:
                        await self.do(self.scooter.build(ROBOTICSFACILITY, self.locrobo))
                    if self.units(ROBOTICSFACILITY).exists:
                        if len(self.gas1probes) > 0:
                            for p in self.workers.filter(lambda w:w.tag in self.gas1probes and len(w.orders) == 1 and w.orders[0].ability.id in [AbilityId.HARVEST_RETURN]):
                                await self.do(p(AbilityId.HARVEST_RETURN))
                                await self.do(p.gather(self.state.mineral_field.closest_to(self.locnat), queue=True))
                                self.gas1probes.remove(p.tag)
                                print('gas1', self.gas1probes)
                        if len(self.gas2probes) > 0:
                            for p in self.workers.filter(lambda w:w.tag in self.gas2probes and len(w.orders) == 1 and w.orders[0].ability.id in [AbilityId.HARVEST_RETURN]):
                                await self.do(p(AbilityId.HARVEST_RETURN))
                                await self.do(p.gather(self.state.mineral_field.closest_to(self.locnat), queue=True))
                                self.gas2probes.remove(p.tag)
                                print('gas2', self.gas2probes)
                        if self.units(STALKER).exists and self.antiOV == None:
                            self.antiOV = self.units(STALKER).first
                            await self.microAntiOV()
                        if self.units(TWILIGHTCOUNCIL).first.is_idle:
                            a = await self.get_available_abilities(self.units(TWILIGHTCOUNCIL).first)
                            if self.can_afford(AbilityId.RESEARCH_CHARGE) and AbilityId.RESEARCH_CHARGE in a:
                                await self.do(self.units(TWILIGHTCOUNCIL).first(AbilityId.RESEARCH_CHARGE))
                                await self.do(self.scooter.move(self.locgate3, queue = True))
                                self.scooterB = self.workers.closer_than(10, self.nexi2).filter(lambda w: len(w.orders) == 1 and w.orders[0].ability.id in [AbilityId.HARVEST_GATHER]).random
                                await self.do(self.scooterB.move(self.locgate2))
                                self.bo = 6
                                print('self bo now 6')
        if self.bo == 6:
            if len(self.workers) < 32:
                await self.build_workers()
            if len(self.units(GATEWAY)) < 8:
                if self.can_afford(GATEWAY):
                    if len(self.units(GATEWAY)) == 1:
                        print('pre gate2')
                        await self.do(self.scooterB.build(GATEWAY, self.locgate2))
                        await self.do(self.scooterB.gather(self.state.mineral_field.closest_to(self.nexi2), queue=True))
                    if len(self.units(GATEWAY)) == 2:
                        await self.do(self.scooter.build(GATEWAY, self.locgate3))
                        await self.do(self.scooter.move(self.locgate5, queue=True))
                    if len(self.units(GATEWAY)) == 3:
                        await self.do(self.scooter.build(GATEWAY, self.locgate4))
                        await self.do(self.scooter.move(self.locgate5, queue=True))
                    if len(self.units(GATEWAY)) == 4:
                        await self.do(self.scooter.build(GATEWAY, self.locgate5))
                        await self.do(self.scooter.move(self.locgate6, queue=True))
                    if len(self.units(GATEWAY)) == 5:
                        await self.do(self.scooter.build(GATEWAY, self.locgate6))
                        await self.do(self.scooter.move(self.locgate7, queue=True))
                    if len(self.units(GATEWAY)) == 6:
                        await self.do(self.scooter.build(GATEWAY, self.locgate7))
                        await self.do(self.scooter.move(self.locgate8, queue=True))
                    if len(self.units(GATEWAY)) == 7:
                        await self.do(self.scooter.build(GATEWAY, self.locgate8))
                        await self.do(self.scooter.gather(self.state.mineral_field.closest_to(self.nexi), queue=True))
            if self.units(ROBOTICSFACILITY).ready.exists:
                if self.units(ROBOTICSFACILITY).noqueue.ready.exists:
                    if not self.units(WARPPRISM).exists:
                        if self.can_afford(WARPPRISM):
                            await self.do(self.units(ROBOTICSFACILITY).first.train(WARPPRISM))
                            await self.do(self.units(ROBOTICSFACILITY).first(RALLY_UNITS, self.locwarp.towards(self.loccore, 14)))
                            await self.chrono(self.nexi, self.units(ROBOTICSFACILITY).first)                    
                if len(self.units(ROBOTICSFACILITY).ready.first.orders) == 1:
                    await self.chrono(self.nexi2, self.units(ROBOTICSFACILITY).first)
                    if self.can_afford(PYLON):
                        await self.do(self.scooter.build(PYLON, self.locpylon3))
                        await self.do(self.scooter.move(self.locgate7.towards(self.game_info.map_center), queue=True))
                        print('built pylon3')
                        self.bo = 7
        if self.bo == 7:
            if self.units(WARPPRISM).exists:
                self.prism = self.units(WARPPRISM).first
                self.prism.phase = 0
                if (len(self.units(GATEWAY).ready)+len(self.units(WARPGATE))) == 8 and self.units(CYBERNETICSCORE).noqueue.exists:
                    for g in self.units(GATEWAY).noqueue:
                        await self.do(g(MORPH_WARPGATE))
                    self.bo = 99
                    print('allin has now started')
                    self.allin = 1 #allin has started
                        
            



run_game(maps.get("(2)LostandFoundLE"), [
    Bot(Race.Protoss, Toss1()),
    Computer(Race.Zerg, Difficulty.Easy)],
    realtime=False)


