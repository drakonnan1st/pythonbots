import sc2, random
from sc2.data import ActionResult
from sc2 import run_game, maps, Race, Difficulty
from sc2.player import Bot, Computer, Human
from sc2.constants import *
from sc2.position import Point2, Point3

class KRHatch(sc2.BotAI):
    def __init__(self):
        print("init'ed")
        self.hatch1 = None
        self.hatch2 = None
        self.hatchAll = []
        self.pool = None
        self.gas1 = None
        self.OV1 = None
        self.OV2 = None
        self.OV3 = None
        self.Injects = []
    def on_start(self):
        print('now on_start')
        self.bo = 0
        if self.enemy_start_locations[0].position[0] > self.game_info.map_center[0]:
            self.loc = {
                'ENEMYlocnat': Point2((143.5, 109.5)),
                'nat': Point2((24.5,54.5)),
                'main': Point2((28.5,32.5)),
                'gas1': Point2((32.5,25.5)),
                'pool': Point2((32.5,28.5)),
                '3rd': Point2((23.5,81.5)),
                'OVA': Point2((140, 96)),
                'fact1': Point2((34.5,40.5)),
                'star1': Point2((34.5,37.5)),
            }
            print('leftwards spawn')
        elif self.enemy_start_locations[0].position[0] < self.game_info.map_center[0]:
            self.loc = {
                'ENEMYlocnat': Point2((24.5, 54.5)),
                'depot1': Point2((133,118)),
                'depot2': Point2((136,121)),
                'nat': Point2((143.5,109.5)),
                'main': Point2((139.5,131.5)),
                'gas1': Point2((146.5,128.5)),
                'pool': Point2((146.5,125.5)),
                '3rd': Point2((144.5,82.5)),
                'OVA': Point2((28,68)),
                'fact1': Point2((131.5,123.5)),
                'star1': Point2((131.5,126.5)),
            }
            print('righwards spawn')
    async def on_step(self, iteration):
        if ZERGLINGMOVEMENTSPEED in self.state.upgrades:
            print(self.state.upgrades)
            await self.chat_send(str(self.state.upgrades))
        await self.buildOrder()
        await self.MicroOV()
        await self.inject()

    async def buildOrder(self):
        if self.bo == 0:
            self.hatch1 = self.units(HATCHERY).first
            self.hatch1.rally = "min"
            await self.workersplit(self.hatch1)
            await self.drone()
            self.OV1 = self.units(OVERLORD).first
            #await self.do(self.OV1(MOVE, self.loc['OVA']))
            if self.supply_used > 12:
                self.bo = 1
        if self.bo == 1:
            if self.can_afford(PYLON):
                await self.do(self.units(LARVA).first.train(OVERLORD))
                self.mineralTags = [x.tag for x in self.state.units.mineral_field]
                self.bo = 2
        if self.bo == 2:
            if self.supply_used < 16 and self.supply_left > 0:
                await self.drone()
            elif self.supply_used == 16:
                if self.hatch2 == None:
                    self.hatch2 = self.units(EGG).closest_to(self.loc['nat'])
                    await self.do(self.hatch2(SMART, self.loc['nat']))
                await self.drone()
            elif self.supply_used == 17:
                if self.can_afford(HATCHERY):
                    self.hatch2 = self.units(DRONE).closest_to(self.loc['nat'])
                    await self.do(self.hatch2.build(HATCHERY, self.loc['nat']))
            if len(self.units(HATCHERY)) == 2:
                print(self.hatch2)
                self.hatch2 = self.units(HATCHERY).closest_to(self.loc['nat'])
                self.bo = 3
        if self.bo == 3:
            if not self.units(EXTRACTOR).exists:
                if self.supply_used == 16:
                    await self.drone()
                elif self.supply_used == 17:
                    if self.gas1 == None:
                        self.gas1 = self.units(EGG).first
                        await self.do(self.gas1(SMART, self.loc['gas1']))
                    await self.drone()
                elif self.supply_used == 18:
                    if self.pool == None:
                        for e in self.units(EGG):
                            if e.position != self.gas1.position:
                                self.pool = e
                                await self.do(e(SMART, self.loc['pool']))
                    if len(self.workers) >= 17:
                        #self.gas1 = self.units(DRONE).closest_to(self.gas1.position)
                        if len(self.workers.closer_than(2,self.loc['gas1'])) > 0:
                            #if self.gas1 == None:
                            self.gas1 = self.workers.closest_to(self.loc['gas1'])
                            #else:
                            if self.can_afford(EXTRACTOR):
                                await self.do(self.gas1.build(EXTRACTOR, self.state.vespene_geyser.closest_to(self.loc['gas1'])))
                                self.pool = None
            else:
                if len(self.workers) == 17:
                    if self.can_afford(SPAWNINGPOOL):
                        await self.do(self.units(DRONE).closest_to(self.loc['pool']).build(SPAWNINGPOOL, self.loc['pool']))
                        self.bo = 4
                        self.gas1 = self.geysers.closest_to(self.loc['gas1'])
                        self.flagA = 0
                        print('self.bo now 4')
        if self.bo == 4:
            if self.supply_used < 18:
                await self.drone()
            if self.supply_used == 18:
                if self.flagA < 2:
                    for e in self.units(EGG):
                        await self.do(e(SMART, self.gas1))
                        print(e)
                        self.flagA += 1
                        print(self.flagA)
                else:
                    await self.drone()
            if self.supply_used == 19:
                if self.units(EXTRACTOR).ready.exists:
                    g = self.geysers.closest_to(self.loc['gas1'])
                    self.gas1 = g
                    print(g.assigned_harvesters)
                    if self.workers.idle.exists:
                        await self.do(self.workers.idle.first(SMART, self.gas1))
                    if g.assigned_harvesters == 2:
                        await self.do(self.workers.filter(lambda w: len(w.orders) == 1 and w.orders[0].ability.id in [AbilityId.HARVEST_GATHER] and w.orders[0].target in self.mineralTags).closest_to(self.gas1)(SMART, self.gas1))
                        print('got 3rd drone')
                    if g.assigned_harvesters == 3:
                        if self.can_afford(OVERLORD) and self.units(LARVA).exists:
                            await self.do(self.units(LARVA).first.train(OVERLORD))
                            
                            self.pool = self.units(SPAWNINGPOOL).first
                            self.bo = 5
                            print("self.bo now 5")
        if self.bo == 5:
            if self.supply_used == 19:
                await self.drone()
            if self.supply_used == 20:
                if self.hatch1.rally == 'min':
                    for h in self.units(HATCHERY):
                                await self.do(h(RALLY_WORKERS, self.state.mineral_field.closest_to(self.loc['nat'])))
                    self.hatch1.rally = 'min2'
                if self.units(SPAWNINGPOOL).ready.exists:
                    if self.can_afford(QUEEN):
                        await self.do(self.hatch1.train(QUEEN))
            if self.supply_used == 22:
                if self.can_afford(DRONE) and self.units(LARVA).exists:
                    await self.do(self.units(LARVA).first.train(ZERGLING))
            if len(self.units(HATCHERY).ready) == 2:
                if self.supply_used < 26:
                    self.hatch2 = self.units(HATCHERY).closest_to(self.loc['nat'])
                    if self.can_afford(SPAWNINGPOOL):
                        await self.do(self.hatch2.train(QUEEN))
                        await self.do(self.units(LARVA).first.train(ZERGLING))
                        print('gas1 drones', self.gas1.assigned_harvesters)
            if self.vespene > 92 and self.gas1.assigned_harvesters > 1:
                g = self.geysers.closest_to(self.gas1)
                w = self.workers.filter(lambda w: len(w.orders) == 1 and w.orders[0].ability.id in [AbilityId.HARVEST_GATHER] and w.orders[0].target == self.gas1.tag)
                if len(w) >  0 and g.assigned_harvesters == 3:
                    #if self.gas1.assigned_harvesters == 3:
                    print('assigned harveseters 3')
                    await self.do(w[0](SMART, self.state.mineral_field.closest_to(self.hatch2)))
                if len(w) > 0 and g.assigned_harvesters == 2:
                    print('assigned harvesters 2')
                    await self.do(w[0](MOVE, self.loc['3rd']))
            if self.can_afford(AbilityId.RESEARCH_ZERGLINGMETABOLICBOOST):
                if self.units(SPAWNINGPOOL).first.is_idle:
                    self.pool = self.units(SPAWNINGPOOL).first
                    await self.do(self.pool(AbilityId.RESEARCH_ZERGLINGMETABOLICBOOST))
                    self.bo = 6
                    print('bo now 6')
        if self.bo == 6:
            if self.supply_used < 30 and len(self.units(HATCHERY)) == 2:
                await self.drone()
            if self.supply_used == 30:
                if self.can_afford(HATCHERY):
                    self.hatchAll = self.units(HATCHERY)
                    await self.do(self.units(DRONE).closest_to(self.loc['3rd']).build(HATCHERY, self.loc['3rd']))
            if len(self.units(HATCHERY)) == 3:
                if len(self.hatchAll) == 2:
                    self.hatchAll = self.units(HATCHERY)
                    print('3 hatches in hatchall')
                if len(self.Injects) < 2:
                    if self.units(QUEEN).exists:
                        for q in self.units(QUEEN):
                            if q not in self.Injects:
                                if q.energy >= 25:
                                    await self.do(q(EFFECT_INJECTLARVA, self.townhalls.closest_to(q)))
                                    self.Injects.append(q)
                                    print('appeneded ', q, 'into Injects')
                if len(self.Injects) == 2:
                    if self.can_afford(QUEEN):
                        await self.do(self.hatch2.train(QUEEN))
                        self.bo = 7
                        print('bo now 7')
        if self.bo == 7:
            if self.supply_used < 33:
                await self.drone()
            if self.supply_used == 33:
                if self.can_afford(OVERLORD) and self.units(LARVA).exists:
                    await self.do(self.units(LARVA).closest_to(self.hatch1).train(OVERLORD))
                    self.bo = 8
        if self.bo == 8:
            if self.supply_used < 35:
                await self.drone()
            if self.supply_used == 35:
                if self.can_afford(OVERLORD) and self.units(LARVA).exists:
                    await self.do(self.units(LARVA).closest_to(self.hatch2).train(OVERLORD))
                    self.bo = 9
        if self.bo == 9:
            if self.supply_used < 36:
                await self.drone()
            if self.supply_used >= 36 and self.supply_used < 38 and self.supply_left > 0:
                lA = self.units(LARVA).closest_to(self.hatch1)
                if self.can_afford(DRONE):
                    await self.do(lA.train(DRONE))
                    await self.do(lA(SMART, self.gas1))
            if self.supply_used >= 38 and self.supply_used < 42 and len(self.units(QUEEN)) < 3:
                await self.drone()
                    
                
                
            
                                
                    

    async def workersplit(self, th):
        wPool = self.workers.closer_than(10, th)
        for w in wPool:
            await self.do(w.gather(self.state.mineral_field.closest_to(w)))

    async def drone(self):
        for l in self.units(LARVA):
            if self.can_afford(DRONE):
                await self.do(l.train(DRONE))
    async def inject(self):
        for q in self.Injects:
            h = self.townhalls.ready.closest_to(q)
            if q.energy >= 25 and q.is_idle and not h.has_buff(QUEENSPAWNLARVATIMER):
                await self.do(q(EFFECT_INJECTLARVA, h))

    async def MicroOV(self):
        if self.OV1 != None:
            if len(self.OV1.orders) == 0:
                await self.do(self.OV1(MOVE, self.loc['OVA']))
                await self.do(self.OV1(HOLDPOSITION, queue=True))
        if len(self.units(OVERLORD)) == 2:
            self.OV2 = self.units(OVERLORD).closest_to(self.hatch1)
            if len(self.OV2.orders) == 0:
                await self.do(self.OV2(MOVE, self.loc['nat'].towards(self.loc['3rd'], 2)))
                await self.do(self.OV2(PATROL, self.loc['nat'].towards(self.hatch1.position, 2), queue=True))
            




def main1():
    run_game(maps.get("(2)LostandFoundLE"), [
        Human(Race.Zerg),
        Bot(Race.Zerg, KRHatch())],
        realtime=True)

def main2():
    run_game(maps.get("(2)LostandFoundLE"), [
        Bot(Race.Zerg, KRHatch()),
        Computer(Race.Zerg, Difficulty.Easy)],
        realtime=False)


if __name__ == '__main__':
    main1()