import time
import random
import sc2
from s2clientprotocol import raw_pb2, sc2api_pb2, common_pb2
from sc2 import run_game, maps, Race, Difficulty
from sc2.constants import *
from sc2.data import ActionResult
from sc2.game_data import AbilityData
from sc2.player import Bot, Computer, Human
from sc2.position import Point2, Point3
from sc2.unit import Unit
from sc2.units import Units


class Experiment2(sc2.BotAI):
    def __init__(self):
        super().__init__()
        self.stalkers = None
        self.waypoint = Point2((80,76))
        self.waypoint2 = Point2((80, 25))
        self.retreat = Point2((40, 76))
        self.testAtrigger = None #unit tag
        self.testBtrigger = None
        self.testCtrigger = None
        self.testDtrigger = None
        self.testEtrigger = None
        self.testApoint = None
        self.testBpoint = None
        self.testCpoint = None
        self.testDpoint = None
        self.ttestEpoint = None
        self.legitInit = False
        self.marines = None
        self.marinetags = []

        #importantly named; dont break
        self.ActionRawList = [] #TODO: Find an alternative
        self.ActionRaw_toMerge = []


    async def on_step(self, iteration):
        if self.legitInit == False:
            start = time.time()
            self.marines = self.units(UnitTypeId.MARINE)
            self.marinetags = [x.tag for x in self.marines]
            self.testAtrigger = self.units(UnitTypeId.DRONE).first.tag
            self.testApoint = self.units(UnitTypeId.DRONE).first.position
            self.testBtrigger = self.units(UnitTypeId.ZERGLING).first.tag
            self.testBpoint = self.units(UnitTypeId.ZERGLING).first.position
            self.testCtrigger = self.units(UnitTypeId.PROBE).first.tag
            self.testCpoint = self.units(UnitTypeId.PROBE).first.position
            self.testDtrigger = self.units(UnitTypeId.SCV).first.tag
            self.testDpoint = self.units(UnitTypeId.SCV).first.position
            self.testEtrigger = self.units(UnitTypeId.WIDOWMINE).first.tag
            self.testEpoint = self.units(UnitTypeId.WIDOWMINE).first.position
            end = time.time()
            print('Time::init:', end-start)
            await self.chat_send("Time::init: {}".format(end-start))
            self.legitInit = True



    async def testA(self): #timing for old-school self.do() on 150 marines
        start = time.time()
        for m in self.marines:
            await self.do(m(ATTACK, self.waypoint))
        end = time.time()
        print('Time::testA', end-start)
        await self.chat_send("Time::testA: {}".format(end-start))

    async def testB(self): #timing for self.do_actions() on 150 marines; compare with testC()
        start = time.time()
        actions = []
        for m in self.marines:
            actions.append(m(ATTACK, self.waypoint2))
            actions.append(m(ATTACK, self.waypoint, queue=True))
        await self.do_actions(actions)
        end = time.time()
        print('Time::testB', end - start)
        await self.chat_send("Time::testB: {}".format(end - start))

    async def testC(self): #timing for bran new self.doGroup() on 150 marines; compare with testB()
        start = time.time()
        await self.doGroup(AbilityId.ATTACK, self.marinetags, self.waypoint2)
        await self.doGroup(AbilityId.ATTACK, self.marinetags, self.waypoint, queue=True)
        end = time.time()
        print('Time::testC', end - start)
        await self.chat_send("Time::testC: {}".format(end - start))

    async def testD(self): #morph archon using Burny's confirmed method
        start = time.time()
        htlist = [x.tag for x in self.units(UnitTypeId.DARKTEMPLAR)]
        await self.doGroup(AbilityId.MORPH_ARCHON, htlist)
        end = time.time()
        print('Time::testD', end - start)
        await self.chat_send("Time::testD: {}".format(end - start))

    async def testE(self): #morph archon using normal do_actions() method
        start = time.time()
        actlist = []
        for ht in self.units(UnitTypeId.HIGHTEMPLAR):
            actlist.append(ht(AbilityId.MORPH_ARCHON))
        await self.do_actions(actlist)
        end = time.time()
        print('Time::testE', end - start)
        await self.chat_send("Time::testE: {}".format(end - start))

    async def testF(self): #try to morph all 6 HT into archons in one step
        start = time.time()
        #ASSUME number of HT is even
        httags = [x.tag for x in self.units(UnitTypeId.HIGHTEMPLAR)]
        while len(httags) > 0:
            ht1 = httags.pop()
            ht2 = httags.pop()
            await self.doGroup(AbilityId.MORPH_ARCHON, [ht1, ht2])
        end = time.time()
        print('Time::testF', end - start)
        await self.chat_send("Time::testF: {}".format(end - start))

    async def testG(self):
        start = time.time()
        x = []
        await self.doActionRaw(x, AbilityId.ATTACK, self.marinetags, self.waypoint)
        await self.doActionRaw(x, AbilityId.ATTACK, self.marinetags, self.waypoint2, queue=True)
        await self.doRequestActions(x)
        end = time.time()
        print('Time::testG', end - start)
        await self.chat_send("Time::testG: {}".format(end - start))


    async def doGroup(self, ability, tags, target=None, queue=False):
        if isinstance(ability, AbilityId):
            abvalue = ability.value
        else:
            print('doGroup: not an action', ability)
            return None
        if not target:
            command = raw_pb2.ActionRawUnitCommand(ability_id=abvalue, unit_tags = tags, queue_command=queue)
        elif isinstance(target, Unit):
            command = raw_pb2.ActionRawUnitCommand(
                ability_id=abvalue, unit_tags = tags, queue_command=queue, target_unit_tag=target.tag)
        elif isinstance(target, Point2):
            command = raw_pb2.ActionRawUnitCommand(
                ability_id=abvalue, unit_tags=tags, queue_command=queue,
                target_world_space_pos=common_pb2.Point2D(x=target.x, y=target.y))
        else:
            print('doGroup: Error, target neither Unit, Point2 nor None;', target)
            return None
        act = raw_pb2.ActionRaw(unit_command=command)
        await self._client._execute(action=sc2api_pb2.RequestAction(actions=[sc2api_pb2.Action(action_raw=act)]))
        

    async def doActionRaw(self, list, ability, tags, target=None, queue=False, should_combine=False):
        if isinstance(ability, AbilityId):
            abvalue = ability.value
        else:
            print('doActionRaw: not an AbilityId', ability)
            return None
        if not target:
            command = raw_pb2.ActionRawUnitCommand(ability_id=abvalue, unit_tags = tags, queue_command=queue)
        elif isinstance(target, Unit):
            command = raw_pb2.ActionRawUnitCommand(
                ability_id=abvalue, unit_tags = tags, queue_command=queue, target_unit_tag=target.tag)
        elif isinstance(target, Point2):
            command = raw_pb2.ActionRawUnitCommand(
                ability_id=abvalue, unit_tags=tags, queue_command=queue,
                target_world_space_pos=common_pb2.Point2D(x=target.x, y=target.y))
        else:
            print('doActionRaw: Error, target neither Unit, Point2 nor None;', target)
            return None
        actraw = raw_pb2.ActionRaw(unit_command=command)
        if should_combine:
            list.append([tags, abvalue, target, queue])
        else:
            list.append(actraw)

    async def doRequestActions(self, fulllist, mergelist=[]):
        acts = []
        if mergelist:
            pass #TODO: do this lol
        acts += fulllist
        res = await self._client._execute(action=sc2api_pb2.RequestAction(actions=[sc2api_pb2.Action(action_raw=a) for a in acts]))
        res = [ActionResult(r) for r in res.action.result]
        return res



    async def on_building_construction_complete(self, unit):
        pass
    async def on_unit_created(self, unit):
        if self.legitInit:
            if unit.type_id == UnitTypeId.DRONE:
                self.testAtrigger = unit.tag
            elif unit.type_id == UnitTypeId.ZERGLING:
                self.testBtrigger = unit.tag
            elif unit.type_id == UnitTypeId.PROBE:
                self.testCtrigger = unit.tag
            elif unit.type_id == UnitTypeId.SCV:
                self.testDtrigger = unit.tag
            elif unit.type_id == UnitTypeId.WIDOWMINE:
                self.testEtrigger = unit.tag
    async def on_unit_destroyed(self, unit_tag):
        if unit_tag == self.testAtrigger:
            await self.testG()
            await self._client.debug_create_unit([[UnitTypeId.DRONE, 1, self.testApoint, 1]])
        elif unit_tag == self.testBtrigger:
            await self.testB()
            await self._client.debug_create_unit([[UnitTypeId.ZERGLING, 1, self.testBpoint, 1]])
        elif unit_tag == self.testCtrigger:
            await self.testC()
            await self._client.debug_create_unit([[UnitTypeId.PROBE, 1, self.testCpoint, 1]])
        elif unit_tag == self.testDtrigger:
            await self.testD()
            await self._client.debug_create_unit([[UnitTypeId.SCV, 1, self.testDpoint, 1]])
        elif unit_tag == self.testEtrigger:
            await self.testE()
            await self._client.debug_create_unit([[UnitTypeId.WIDOWMINE, 1, self.testEpoint, 1]])



def main():
    sc2.run_game(maps.get("Testing"), [
    Bot(Race.Protoss, Experiment2()),],
    realtime=True)

if __name__ == '__main__':
    main()
