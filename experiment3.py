import time
from typing import Union, List
import random
import sc2
from s2clientprotocol import raw_pb2, sc2api_pb2, common_pb2
from sc2 import run_game, maps, Race, Difficulty
from sc2.constants import *
from sc2.data import ActionResult
from sc2.game_data import AbilityData
from sc2.player import Bot, Computer, Human
from sc2.position import Point2, Point3
from sc2.unit import Unit
from sc2.units import Units


class Experiment2(sc2.BotAI):
    def __init__(self):
        super().__init__()
        self.placements3by3 = []
        self.placements2by2 = []
        self.placements5by3 = []
        self.occupied3by3 = []
        self.mateus = []
        self.mateus2 = []
        self.ready = False


    async def on_step(self, iteration):
        if self.ready == False:
            start = time.time()
            for th in self.townhalls:
                # generate zerg closewall 3by3s
                zclose = self.generate_3by3_zergclose(th)
                if zclose:
                    self.placements3by3 += zclose
                m = await self.matuiss2(th)
                if m:
                    self.mateus += m

            end = time.time()
            print('Time::init:', end-start)
            await self.chat_send("Time::init: {}".format(end-start))
            print(self.placements3by3)
            print('mateus:', len(self.mateus), self.mateus)
            self.ready = True
        else:
            for p in self.placements3by3:
                self._client.debug_box_out(Point3((p.x-1.5, p.y-1.5, 8)), Point3((p.x+1.5, p.y+1.5, 12)), Point3((255,100,100)))

            for p in self.occupied3by3:
                self._client.debug_box_out(Point3((p.x - 1.5, p.y - 1.5, 8)), Point3((p.x + 1.5, p.y + 1.5, 12)),
                                           Point3((100, 255, 100)))
            for p in self.mateus + self.mateus2:
                self._client.debug_box_out(Point3((p.x - 1.5, p.y - 1.5, 8)), Point3((p.x + 1.5, p.y + 1.5, 10)),
                                           Point3((100, 100, 200)))
            await self._client.send_debug()

            if self.placements3by3:
                if self.can_place(UnitTypeId.GATEWAY, self.placements3by3[0]):
                    await self._client.debug_create_unit([[UnitTypeId.GATEWAY, 1, self.placements3by3[0], 1]])
                    self.occupied3by3.append(self.placements3by3.pop(0))
            #if self.mateus:
                #if self.can_place(UnitTypeId.GATEWAY, self.mateus[0]):
                    #await self._client.debug_create_unit([[UnitTypeId.ENGINEERINGBAY, 1, self.mateus[0], 1]])
                    #self.mateus2.append(self.mateus.pop(0))
            if self.state.chat:
                for chat in self.state.chat:
                    if chat.message == 'kys':
                        await self._client.leave()

    def generate_3by3_zergclose(self, hatch):
        if isinstance(hatch, Unit):
            pos = hatch.position
        else:
            pos = hatch

        # 8 viable spots: () jk only for distant geysers
        geysers = self.state.vespene_geyser.closer_than(10, hatch)
        if len(geysers) == 1:  #TODO:
            pass
        elif len(geysers) == 2: # hell yeah
            g1p = geysers[0].position
            g2p = geysers[1].position
            if g1p._distance_squared(g2p) > 64: #hell yeah; only 8 viable spots now
                if g1p.y == g2p.y:
                    if g1p.y > pos.y: #minline above
                        print('minline above')
                        return [Point2((pos.x-5, pos.y-2)), Point2((pos.x+5, pos.y-2))]
                    elif g1p.y < pos.y:
                        print('minline below')
                        return [Point2((pos.x-5, pos.y+2)), Point2((pos.x+5, pos.y+2))]
                    else:
                        print('ERROR: 2 gases far apart; same y coord as nexus')
                elif g1p.x == g2p.x:
                    if g1p.x > pos.x: # minline on right
                        print('minline right')
                        return [Point2((pos.x-1, pos.y-5)), Point2((pos.x-1, pos.y+5))]
                    elif g1p.x < pos.x:
                        print('minline left')
                        return [Point2((pos.x+1, pos.y-5)), Point2((pos.x+1, pos.y+5))]
                else:
                    #diagonals:
                    viable = [Point2((pos.x-4, pos.y-4)), Point2((pos.x-4, pos.y-4)),
                              Point2((pos.x+4, pos.y+4)), Point2((pos.x+4, pos.y+4)),
                              Point2((pos.x-4, pos.y+4)), Point2((pos.x-4, pos.y+4)),
                              Point2((pos.x+4, pos.y-4)), Point2((pos.x+4, pos.y-4))]
                    viable.remove(g1p.closest(viable)) # to get 2nd closest
                    viable.remove(g2p.closest(viable))
                    return [g1p.closest(viable), g2p.closest(viable)]
            else:
                # geysers close by
                mins = self.state.mineral_field.closer_than(8, pos)
                mincenter = mins.center
                if g1p.x == g2p.x:
                    # straight above or below
                    pass
                elif g1p.y == g2p.y:
                    # straight left or right
                    pass
                else:
                    min = mins.closest_to(pos.towards(geysers.center, -6)).position
                    # viableA = [(pos.x, pos.y-5), (pos.x, pos.y+5)]
                    if g1p.x < pos.x:
                        if g1p.y < pos.y: # gas bot left
                            return [Point2((pos.x, pos.y-5)), Point2((min.x+2.5, min.y-1))]
                        elif g1p.y > pos.y: # gas top left
                            return [Point2((pos.x, pos.y+5)), Point2((min.x+2.5, min.y+1))]
                    elif g1p.x > pos.x:
                        if g1p.y < pos.y: # gas bot right
                            return [Point2((pos.x, pos.y-5)), Point2((min.x-2.5, min.y-1))]
                        elif g1p.y > pos.y: # gas top left
                            return [Point2((pos.x, pos.y+5)), Point2((min.x-2.5, min.y+1))]

    async def matuiss2(self, hatch):
        a = time.time()
        mins = self.state.mineral_field
        rng = range(-11, 11)
        hatchpos = hatch.position
        if not mins.closer_than(10, hatchpos):
            return
        spots = []
        phat = ()
        b = time.time()
        l = await self.can_place_list(UnitTypeId.ENGINEERINGBAY,
                                      [pn for pn in (
                                          p for p in (Point2((x + hatchpos.x, y + hatchpos.y))
                for x in rng for y in rng if 121 >= x * x + y * y >= 81)
                                      ) if abs(pn.distance_to(mins.closer_than(10, hatchpos).closest_to(pn))-3) <= 0.5])

        for s in hatchpos.sort_by_distance(l):
            if not spots:
                spots.append(s)
            else:
                if all(
                        abs(already_found.x - s.x) >= 3 or abs(already_found.y - s.y) >= 3
                        for already_found in spots
                ):
                    spots.append(s)
        print('Mateus: part1:', b-a, '; Part2:', time.time()-b)
        return spots

    async def can_place_list(self, building: Union[AbilityData, AbilityId, UnitTypeId], positions: List["Point2"]) -> List["Point2"]:
        """AbilityData, AbilityId, UnitTypeId only"""
        if isinstance(building, UnitTypeId):
            building = self._game_data.units[building.value].creation_ability
        elif isinstance(building, AbilityId):
            building = self._game_data.abilities[building.value]
        mask = await self._client.query_building_placement(building, positions)
        r = [l for i, l in enumerate(positions) if mask[i] == ActionResult.Success]
        return r










    async def doGroup(self, ability, tags, target=None, queue=False):
        if isinstance(ability, AbilityId):
            abvalue = ability.value
        else:
            print('doGroup: not an action', ability)
            return None
        if not target:
            command = raw_pb2.ActionRawUnitCommand(ability_id=abvalue, unit_tags = tags, queue_command=queue)
        elif isinstance(target, Unit):
            command = raw_pb2.ActionRawUnitCommand(
                ability_id=abvalue, unit_tags = tags, queue_command=queue, target_unit_tag=target.tag)
        elif isinstance(target, Point2):
            command = raw_pb2.ActionRawUnitCommand(
                ability_id=abvalue, unit_tags=tags, queue_command=queue,
                target_world_space_pos=common_pb2.Point2D(x=target.x, y=target.y))
        else:
            print('doGroup: Error, target neither Unit, Point2 nor None;', target)
            return None
        act = raw_pb2.ActionRaw(unit_command=command)
        await self._client._execute(action=sc2api_pb2.RequestAction(actions=[sc2api_pb2.Action(action_raw=act)]))
        

    async def doActionRaw(self, list, ability, tags, target=None, queue=False, should_combine=False):
        if isinstance(ability, AbilityId):
            abvalue = ability.value
        else:
            print('doActionRaw: not an AbilityId', ability)
            return None
        if not target:
            command = raw_pb2.ActionRawUnitCommand(ability_id=abvalue, unit_tags = tags, queue_command=queue)
        elif isinstance(target, Unit):
            command = raw_pb2.ActionRawUnitCommand(
                ability_id=abvalue, unit_tags = tags, queue_command=queue, target_unit_tag=target.tag)
        elif isinstance(target, Point2):
            command = raw_pb2.ActionRawUnitCommand(
                ability_id=abvalue, unit_tags=tags, queue_command=queue,
                target_world_space_pos=common_pb2.Point2D(x=target.x, y=target.y))
        else:
            print('doActionRaw: Error, target neither Unit, Point2 nor None;', target)
            return None
        actraw = raw_pb2.ActionRaw(unit_command=command)
        if should_combine:
            list.append([tags, abvalue, target, queue])
        else:
            list.append(actraw)

    async def doRequestActions(self, fulllist, mergelist=[]):
        acts = []
        if mergelist:
            pass #TODO: do this lol
        acts += fulllist
        res = await self._client._execute(action=sc2api_pb2.RequestAction(actions=[sc2api_pb2.Action(action_raw=a) for a in acts]))
        res = [ActionResult(r) for r in res.action.result]
        return res




def main():
    sc2.run_game(maps.get("CartographyTest01"), [
    Bot(Race.Protoss, Experiment2()),],
    realtime=True)

if __name__ == '__main__':
    main()
