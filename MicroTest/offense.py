import sc2, random
from sc2.data import ActionResult
from sc2.unit import Unit
from sc2.units import Units
from sc2 import run_game, maps, Race, Difficulty
from sc2.player import Bot, Computer, Human
from sc2.constants import *
from sc2.position import Point2, Point3
from typing import Union, List


class attacker:
    def __init__(self, army = Union[int, "Unit", "Units", List[int]]):
        if isinstance(army, int):
            self.Single = True
            self.army = army
        elif isinstance(army, Unit):
            self.Single = True
            self.army = army.tag
        elif isinstance(army, Units):
            self.Single = False
            self.army = [x.tag for x in army]
        elif isinstance(army, List):
            self.Single = False
            self.army = army
        self.goal = None #Point2, will try to get there
        self.retreat = None #also Point2
        self.phase = 0 #0: idle, 1: in transit, 2: attacking, 3: retreating

    def atk(self, goal, retreat):
        self.goal = goal
        self.retreat = retreat




class OffenseManager:
    def __init__(self, bot):
        self.bot = bot
        self.platoons = [] #list of attackers


    async def step(self, iter):
        for platoon in self.platoons:
            if platoon.goal != None:
                if platoon.phase == 0:
                    if platoon.Single == True:
                        u = self.bot.units.find_by_tag(platoon.army)
                        if u:
                            await self.bot.do(u.attack(platoon.goal))
                            platoon.phase = 1
                    else:
                        us = self.bot.units.tags_in(platoon.army)
                        for u in us:
                            await self.bot.do(u.attack(platoon.goal))
                        platoon.phase = 3
                elif platoon.phase == 1: #remove?
                    u = None
                    if platoon.Single:
                        u = self.bot.units.find_by_tag(platoon.army)
                    else:
                        us = self.bot.tags_in(platoon.army)
                        if us:
                            u = self.bot.units.tags_in(platoon.army).closest_to(platoon.goal)
                    if u:
                        if u.position.distance_to(platoon.goal) <= 15:
                            if platoon.Single:
                                platoon.phase = 2
                                await self.bot.do(u.attack(platoon.goal))
                            else:
                                for u in self.bot.tags_in(platoon.army):
                                    await self.bot.do(u.attack(platoon.goal))
                                platoon.phase = 2
                elif platoon.phase == 2:
                    tags = []
                    if platoon.Single:
                        tags.append(platoon.army)
                    else:
                        tags = platoon.army
                    for u in self.bot.units.tags_in(tags):
                        #This is where the fun begins
                        target = self.bot.known_enemy_units


    async def buildingConstructionComplete(self, unit):
        pass
    async def unitCreated(self, unit):
        pass
    async def unitDestroyed(self, tag):
        for p in self.platoons:
            if p.Single:
                if p.army == tag:
                    self.platoons.remove(p)
                    del p
            else:
                if tag in p.army:
                    p.remove(tag)