import sc2, random
from sc2.data import ActionResult
from sc2 import run_game, maps, Race, Difficulty
from sc2.player import Bot, Computer, Human
from sc2.constants import *
from sc2.position import Point2, Point3

from basebot import BaseBot
from microtest import MicroTest
from workermgmt import WorkerMgmt

class TossB(BaseBot):
    def __init__(self):
        super().__init__()
        self.ais = []

    def on_start(self):
        self.ai = MicroTest(self)
        self.ais.append(self.ai)
        #self.workermanager = WorkerMgmt(self)
        #self.ais.append(self.workermanager)

    async def on_step(self, iteration):
        if iteration == 0:
            await self.workersplit(self.townhalls.random)
            await self.chat_send('(glhf)')
        for ai in self.ais:
            await ai.step(iteration)



        #End of on_step()
        await self.dostuff()

    async def on_building_construction_complete(self, unit):
        for ai in self.ais:
            await ai.buildingConstructionComplete(unit)
    async def on_unit_created(self, unit):
        for ai in self.ais:
            await ai.unitCreated(unit)
    async def on_unit_destroyed(self, unit_tag):
        for ai in self.ais:
            await ai.unitDestroyed(unit_tag)




run_game(maps.get("(2)LostandFoundLE"), [
    Bot(Race.Protoss, TossB()),
    Computer(Race.Protoss, Difficulty.Easy)],
    realtime=False)
