import sc2, random
from sc2.data import ActionResult
from sc2 import run_game, maps, Race, Difficulty
from sc2.player import Bot, Computer, Human
from sc2.constants import *
from sc2.position import Point2, Point3

from basebot import BaseBot
from workermgmt import WorkerMgmt

class MicroTest:
    def __init__(self, bot):
        self.phase = 0
        self.bot = bot
        self.nexus1 = None
        self.workerA = None
        if self.bot.enemy_start_locations[0].position[0] > self.bot.game_info.map_center[0]:
            self.loc = {
                'pylon1': Point2((32,23)),
                'pylon2': Point2((30,25)),
                'pylon3': Point2((28,60)),
                'gate1': Point2((35.5,26.5)),
                'gate2': Point2((35.5,23.5)),
                'core': Point2((26.5,20.5))
                }
        if self.bot.enemy_start_locations[0].position[0] < self.bot.game_info.map_center[0]:
            self.loc = {
                'pylon1': Point2((136,141)),
                'pylon2': Point2((138,139)),
                'pylon3': Point2((140,104)),
                'gate1': Point2((132.5,137.5)),
                'gate2': Point2((132.5,140.5)),
                'core': Point2((141.5,143.5))
                }
        

    async def step(self, iter):
        await self.MacroBuildings()



    async def MacroBuildings(self):
        if self.bot.supply_used < 14:
            await self.bot.drone()
        if self.bot.supply_used == 14 and self.bot.units(UnitTypeId.PYLON).amount == 0:
            if self.workerA == None:
                self.nexus1 = self.bot.units(UnitTypeId.NEXUS).first
                self.workerA = self.bot.selectMineralDrone(self.nexus1)
            if self.bot.minerals >= 80 and self.workerA:
                await self.bot.do(self.workerA.move(self.loc['pylon1']))
            if self.bot.minerals >= 100:
                await self.bot.do(self.workerA.build(UnitTypeId.PYLON, self.loc['pylon1']))
                await self.bot.do(self.workerA(SMART, self.bot.state.mineral_field.closest_to(self.nexus1), queue=True))
        if self.bot.units(UnitTypeId.PYLON).amount == 1 and self.bot.supply_used < 17:
            await self.bot.drone()
            if self.bot.supply_cap > 20 and self.nexus1.energy > 25:
                await self.bot.chrono(self.nexus1, self.nexus1)
            if self.bot.minerals >= 150 and self.bot.units(UnitTypeId.GATEWAY).amount == 0:
                await self.bot.do(self.workerA.build(UnitTypeId.GATEWAY, self.loc['gate1']))
                g = self.bot.state.vespene_geyser.closer_than(10, self.nexus1).closest_to(self.workerA)
                await self.bot.do(self.workerA.move(g, queue=True))
        if self.bot.supply_used == 17:
            await self.bot.drone()
            if self.bot.units(UnitTypeId.ASSIMILATOR).amount == 0:
                if self.bot.minerals >= 75:
                    g = self.bot.state.vespene_geyser.closer_than(10, self.nexus1).closest_to(self.workerA)
                    await self.bot.do(self.workerA.build(ASSIMILATOR, g))
                    await self.bot.do(self.workerA(SMART, self.bot.state.mineral_field.closest_to(self.nexus1), queue=True))
        if self.bot.supply_used >= 18:
            await self.bot.drone()
            if self.bot.units(UnitTypeId.ASSIMILATOR).amount == 1:
                if self.bot.minerals >= 75:
                    g = self.bot.state.vespene_geyser.closer_than(10, self.nexus1).furthest_to(self.bot.units(UnitTypeId.ASSIMILATOR).first)
                    w = self.bot.selectMineralDrone(self.nexus1)
                    await self.bot.do(w.build(UnitTypeId.ASSIMILATOR, g))
                    await self.bot.do(w(SMART, self.bot.state.mineral_field.closest_to(w), queue=True))
        if self.bot.units(UnitTypeId.ASSIMILATOR).amount == 2 and not self.bot.units(UnitTypeId.CYBERNETICSCORE):
            await self.bot.drone()
            if self.bot.units(UnitTypeId.GATEWAY).amount < 2:
                if self.bot.minerals >= 120:
                    await self.bot.do(self.workerA.move(self.loc['gate2']))
                if self.bot.minerals >= 150:
                    await self.bot.do(self.workerA.build(UnitTypeId.GATEWAY, self.loc['gate2']))
                    await self.bot.do(self.workerA(SMART, self.bot.state.mineral_field.closer_than(10, self.nexus1).closest_to(self.workerA), queue=True))
            if self.bot.units(UnitTypeId.GATEWAY).amount == 2:
                if self.bot.minerals >= 120:
                    await self.bot.do(self.workerA.move(self.loc['core']))
                if self.bot.units(UnitTypeId.GATEWAY).ready and self.bot.can_afford(UnitTypeId.CYBERNETICSCORE):
                    await self.bot.do(self.workerA.build(UnitTypeId.CYBERNETICSCORE, self.loc['core']))
                    await self.bot.do(self.workerA(SMART, self.bot.state.mineral_field.closest_to(self.nexus1), queue=True))
        if self.bot.units(UnitTypeId.CYBERNETICSCORE) and self.bot.units(UnitTypeId.PYLON).amount == 1:
            await self.bot.drone()
            
            if self.bot.supply_used == 22:
                if self.bot.minerals >= 100:
                    for g in self.bot.units(UnitTypeId.ASSIMILATOR):
                        w = self.bot.selectMineralDrone(self.nexus1)
                        a = self.bot.state.vespene_geyser.closest_to(g)
                        await self.bot.do(w(SMART, a))
                    await self.bot.do(self.workerA.build(UnitTypeId.PYLON, self.loc['pylon2']))
                    await self.bot.do(self.workerA(SMART, self.bot.state.mineral_field.closest_to(self.nexus1), queue=True))
        if self.bot.units(UnitTypeId.PYLON).amount == 2:
            await self.bot.drone()
            if self.bot.units(UnitTypeId.CYBERNETICSCORE).ready:
                if self.bot.already_pending(UnitTypeId.STALKER) < 2:
                    for g in self.bot.units(UnitTypeId.GATEWAY):
                        if self.bot.can_afford(UnitTypeId.STALKER):
                            await self.bot.do(g.train(UnitTypeId.STALKER))
                if self.bot.can_afford(AbilityId.RESEARCH_WARPGATE):
                    if self.bot.units(UnitTypeId.CYBERNETICSCORE).idle:
                        await self.bot.do(self.bot.units(UnitTypeId.CYBERNETICSCORE).first(AbilityId.RESEARCH_WARPGATE))
                if self.bot.already_pending(UnitTypeId.STALKER) + self.bot.units(UnitTypeId.STALKER).amount < 2 and self.bot.units(UnitTypeId.PYLON).amount < 3:
                    await self.bot.do(self.workerA.move(self.loc['pylon3']))
                    if self.bot.minerals >= 100:
                        await self.bot.do(self.workerA.build(UnitTypeId.PYLON, self.loc['pylon3']))
                        await self.bot.do(self.workerA(SMART, self.bot.state.mineral_field.closest_to(self.workerA), queue=True))
                        a = await self.bot.get_next_expansion()
                        await self.bot.do(self.workerA.move(a, queue=True))
        if self.bot.units(UnitTypeId.PYLON).amount == 3 and self.bot.townhalls.amount < 2:
            if self.bot.already_pending(UnitTypeId.STALKER) + self.bot.units(UnitTypeId.STALKER).amount < 4:
                for g in self.bot.units(UnitTypeId.GATEWAY):
                    if self.bot.can_afford(UnitTypeId.STALKER):
                        await self.bot.do(g.train(UnitTypeId.STALKER))
                        break
            if self.bot.minerals >= 400:
                loc = await self.bot.get_next_expansion()
                await self.bot.do(self.workerA.build(UnitTypeId.NEXUS, loc))


    async def StalkerMicro(self):
        for s in self.bot.units(UnitTypeId.STALKER):
            pass




    async def buildingConstructionComplete(self, unit):
        if unit.type_id == UnitTypeId.ASSIMILATOR:
            if self.bot.units(UnitTypeId.ASSIMILATOR).amount <= 2:
                ws = self.bot.workers.closer_than(10, self.nexus1).filter(lambda x: not x.is_carrying_minerals)
                if len(ws) >= 2:
                    wss = ws.prefer_close_to(unit)[:2]
                else:
                    wss = self.bot.workers.closer_than(10, self.nexus1).prefer_close_to(unit)[:2]
                if len(wss) == 2:
                    for w in wss:
                        await self.bot.do(w(SMART, unit))
                else:
                    print('AAAARGH')
    async def unitCreated(self, unit):
        pass
    async def unitDestroyed(self, tag):
        pass
